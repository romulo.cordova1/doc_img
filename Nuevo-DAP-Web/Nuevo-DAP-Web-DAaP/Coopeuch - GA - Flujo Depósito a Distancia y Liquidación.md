<img align="left" width="19%" height="19%" src="https://gitlab.com/d2b_chile/formato-documento-markdown/-/raw/master/Imagenes/D2B-GRIS-POSITIVO-CON BAJADA-01.png">

# **Documento de Implementación** de Nuevo DAP Web - DAaP - Renovable y Liquidación (Google Analytics)

Junio 2021





## Sobre el Documento

El siguiente documento describe las variables a ser utilizadas en cada marca, se debe copiar el script tal cual como viene indicado en cada paso. Cuando aparezca un  **`%variable%`**, corresponde a una variable que se debe reemplazar según el dato que corresponda de acuerdo al paso o sección en que se encuentre.

______

































## Simulador Depósito a Plazo
### Landing

![](https://gitlab.com/romulo.cordova1/doc_img/-/raw/main/Nuevo-DAP-Web/Nuevo-DAP-Web-DAaP/img/daap_landing.PNG)

Cuando el socio ingrese a esta sección de **Mis Productos**, llamada **Depósito a Plazo**, se debe ejecutar el siguiente script:

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/mis-productos/deposito-a-plazo',
});
</script>
```

##### Simular
![](https://gitlab.com/romulo.cordova1/doc_img/-/raw/main/Nuevo-DAP-Web/Nuevo-DAP-Web-DAaP/img/daap_landing_simular.PNG)

Cuando el socio haga click en el botón o enlace "**Simular**" (bordeado en color naranja), se debe ejecutar el siguiente script.

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/mis-productos/deposito-a-plazo',
            'eve-cat' : 'Click',
			'eve-lab' : 'Simular'
		}
	});
</script>
```
### Simulación
#### Paso 1 (Simulación)
![](https://gitlab.com/romulo.cordova1/doc_img/-/raw/main/Nuevo-DAP-Web/Nuevo-DAP-Web-DAaP/img/daap_paso1.png)

Cuando el socio ingrese a esta página, se debe ejecutar el siguiente script:

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/mis-productos/deposito-a-plazo/paso-1',
});
</script>
```

##### Simular

Cuando el socio haga click en **Simular** (bordeado en naranja), se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/mis-productos/deposito-a-plazo/paso-1',
            'eve-cat' : 'Click',
			'eve-lab' : 'Simular'
		}
	});
</script>
```
##### Volver
Cuando el socio haga click en **Volver** (bordeado en azul), se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/mis-productos/deposito-a-plazo/paso-1',
            'eve-cat' : 'Click',
			'eve-lab' : 'Volver'
		}
	});
</script>
```

#### Paso 2 (Resultado)

![](https://gitlab.com/romulo.cordova1/doc_img/-/raw/main/Nuevo-DAP-Web/Nuevo-DAP-Web-DAaP/img/daap_paso2.PNG)

Cuando el socio ingrese a esta página, se debe ejecutar el siguiente script:

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/mis-productos/deposito-a-plazo/paso-2',
});
</script>
```

##### Continuar
Cuando el socio haga click en **Continuar** (bordeado en naranja), se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/mis-productos/deposito-a-plazo/paso-2',
            'eve-cat' : 'Click',
			'eve-lab' : 'Continuar'
		}
	});
</script>
```
##### Volver
Cuando el socio haga click en **Volver** (bordeado en azul), se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/mis-productos/deposito-a-plazo/paso-2',
            'eve-cat' : 'Click',
			'eve-lab' : 'Volver'
		}
	});
</script>
```


#### Paso 3 (Detalle simulación)
![](https://gitlab.com/romulo.cordova1/doc_img/-/raw/main/Nuevo-DAP-Web/Nuevo-DAP-Web-DAaP/img/daap_paso3_invertir.PNG)

Cuando el socio ingrese a esta página, se debe ejecutar el siguiente script:

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/mis-productos/deposito-a-plazo/paso-3',
});
</script>
```

##### Invertir
Cuando el socio haga click en **Invertir** (bordeado en naranja), se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/mis-productos/deposito-a-plazo/paso-3',
            'eve-cat' : 'Click',
			'eve-lab' : 'Invertir'
		}
	});
</script>
```
###### [Caso borde] Solicitar Cuenta Vista
![](https://gitlab.com/romulo.cordova1/doc_img/-/raw/main/Nuevo-DAP-Web/Nuevo-DAP-Web-DAaP/img/daap_paso3_solicitud_cuenta_vista.png)

Habrá casos donde el socio podría hacer click en **Invertir**, sin poseer una *Cuenta Vista*, si esto ocurre, se mostrará el botón **Solicitar Cuenta Vista**, necesaria para la contratación del *Depósito a Plazo*. 

Cuando el socio haga click en **Solicitar Cuenta Vista** (bordeado en violeta), se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/mis-productos/deposito-a-plazo/paso-3',
            'eve-cat' : 'Click',
			'eve-lab' : 'Solicitar Cuenta Vista'
		}
	});
</script>
```

##### Volver
Cuando el socio haga click en **Volver** (bordeado en azul), se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/mis-productos/deposito-a-plazo/paso-3',
            'eve-cat' : 'Click',
			'eve-lab' : 'Volver'
		}
	});
</script>
```
##### Cambiar
Cuando el socio haga click en **Cambiar** (bordeado en verde), se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/mis-productos/deposito-a-plazo/paso-3',
            'eve-cat' : 'Click',
			'eve-lab' : 'Cambiar simulacion'
		}
	});
</script>
```


#### Paso 4 (Confirmación)
![](https://gitlab.com/romulo.cordova1/doc_img/-/raw/main/Nuevo-DAP-Web/Nuevo-DAP-Web-DAaP/img/daap_paso4.png)

Cuando el socio ingrese a esta página, se debe ejecutar el siguiente script:

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/mis-productos/deposito-a-plazo/paso-4',
});
</script>
```

##### Confirmar
Cuando el socio haga click en **Confirmar** (bordeado en naranja), se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/mis-productos/deposito-a-plazo/paso-4',
            'eve-cat' : 'Click',
			'eve-lab' : 'Confirmar'
		}
	});
</script>
```

###### Aceptar Términos y Condiciones
![](https://gitlab.com/romulo.cordova1/doc_img/-/raw/main/Nuevo-DAP-Web/Nuevo-DAP-Web-DAaP/img/daap_paso4_tyc.PNG)

Una vez el socio haga click en **Confirmar** se desplegará este modal. Si el socio hace click en **Aceptar**, se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/mis-productos/deposito-a-plazo/paso-4',
            'eve-cat' : 'Click',
			'eve-lab' : 'Aceptar terminos y condiciones'
		}
	});
</script>
```



##### No por ahora

Cuando el socio haga click en **No por ahora** (bordeado en azul), se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/mis-productos/deposito-a-plazo/paso-4',
            'eve-cat' : 'Click',
			'eve-lab' : 'No por ahora'
		}
	});
</script>
```

###### [Caso borde] Volver a Simular - Sin Saldo Suficiente
![](https://gitlab.com/romulo.cordova1/doc_img/-/raw/main/Nuevo-DAP-Web/Nuevo-DAP-Web-DAaP/img/daap_paso4_saldo_insuficiente.PNG)

En caso de que el socio haya hecho click en **Confirmar** y no tenga saldo suficiente, se desplegará la huincha mostaza. Si esto sucede, se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/mis-productos/deposito-a-plazo/paso-4',
            'eve-cat' : 'Error',
			'eve-lab' : 'Saldo insuficiente'
		}
	});
</script>
```
Cuando el socio haga click en **Volver a Simular** (bordeado en verde), se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/mis-productos/deposito-a-plazo/paso-4',
            'eve-cat' : 'Click',
			'eve-lab' : 'Volver a Simular'
		}
	});
</script>
```
#### Paso 5 (Comprobante)
![](https://gitlab.com/romulo.cordova1/doc_img/-/raw/main/Nuevo-DAP-Web/Nuevo-DAP-Web-DAaP/img/daap_paso5.png)

Cuando el socio ingrese a esta página, se debe ejecutar el siguiente script. Donde ```%monto_invertido%``` hace referencia al **Monto Invertido** mostrado en la huincha azul en *Detalles de inversión*,  ```%tasa_base%``` a la tasa mostrada en la fila de la tabla con el mismo nombre y ```%producto%``` al Tipo de depósito mostrado en la tabla:

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/mis-productos/deposito-a-plazo/paso-5',
	'monto_simulado' : '%monto_invertido%',
	'tasa_interes_mensual' : '%tasa_base%',
	'producto' : '%tipo_deposito%'
});
</script>
```





## Liquidación

### Landing

![](https://gitlab.com/romulo.cordova1/doc_img/-/raw/main/Nuevo-DAP-Web/Nuevo-DAP-Web-DAaP/img/liquidar_landing.PNG)

Cuando el socio ingrese a esta sección de **Mis Productos**, llamada **Depósito a Plazo**, se debe ejecutar el siguiente script:

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/mis-productos/deposito-a-plazo',
});
</script>
```



##### Ver detalle

Cuando el socio haga click en el botón "**Ver detalle**" (bordeado en color naranja), se debe ejecutar el siguiente script.

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/mis-productos/deposito-a-plazo',
            'eve-cat' : 'Click',
			'eve-lab' : 'Ver detalle'
		}
	});
</script>
```



### Detalle

![](https://gitlab.com/romulo.cordova1/doc_img/-/raw/main/Nuevo-DAP-Web/Nuevo-DAP-Web-DAaP/img/liquidar_detalle.png)



Cuando el socio ingrese al detalle del **Depósito a Plazo** se debe ejecutar el siguiente script:

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/mis-productos/deposito-a-plazo/detalle',
});
</script>
```

#### Liquidar

Cuando el socio haga click en el botón **Liquidar** se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/mis-productos/deposito-a-plazo/detalle',
            'eve-cat' : 'Click',
			'eve-lab' : 'Liquidar'
		}
	});
</script>
```



### Paso 1 (Confirmación)

![](https://gitlab.com/romulo.cordova1/doc_img/-/raw/main/Nuevo-DAP-Web/Nuevo-DAP-Web-DAaP/img/liquidar_paso-1.png)



Cuando el socio ingrese al Paso 1 (Confirmación), se debe ejecutar el siguiente script:

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/mis-productos/deposito-a-plazo/liquidar/paso-1',
});
</script>
```

#### Confirmar

Si el socio hace click en **Confirmar** (bordeado en naranja), se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/mis-productos/deposito-a-plazo/liquidar/paso-1',
            'eve-cat' : 'Click',
			'eve-lab' : 'Confirmar'
		}
	});
</script>
```

#### Volver

Si el socio hace click en **Volver** (bordeado en azul), se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/mis-productos/deposito-a-plazo/liquidar/paso-1',
            'eve-cat' : 'Click',
			'eve-lab' : 'Volver'
		}
	});
</script>
```

### Paso 2 (Comprobante)
![](https://gitlab.com/romulo.cordova1/doc_img/-/raw/main/Nuevo-DAP-Web/Nuevo-DAP-Web-DAaP/img/liquidar_paso-2.png)

Cuando el socio ingrese en el Paso 2 (Comprobante), se debe ejecutar el siguiente script:

Cuando el socio ingrese a esta página, se debe ejecutar el siguiente script. Donde ```%monto_liquidar%``` hace referencia al **Monto a Liquidar** mostrado en la huincha azul en *Detalles de inversión*,  ```%tasa_base%``` a la tasa mostrada en la fila de la tabla con el mismo nombre y ```%producto%``` al Tipo de depósito mostrado en la tabla:

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/mis-productos/deposito-a-plazo/liquidar/paso-2',
	'monto_simulado' : '%monto_liquidar%',
	'tasa_interes_mensual' : '%tasa_base%',
	'producto' : '%tipo_deposito%'
});
</script>
```



## [Caso Borde] Liquidación sin Cuenta Vista

![](https://gitlab.com/romulo.cordova1/doc_img/-/raw/main/Nuevo-DAP-Web/Nuevo-DAP-Web-DAaP/img/liquidar_sin_cuenta_vista.png)

Cuando este modal aparezca (si el socio no posee una **Cuenta Vista Activa**, y hace click en **Liquidar**), se debe ejecutar el siguiente script:

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/mis-productos/deposito-a-plazo/liquidar/sin-cuenta-vista',
});
</script>
```
#### Solicitar Cuenta Vista

Cuando el socio haga click en **Solicitar Cuenta Vista** (bordeado en naranja), se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/mis-productos/deposito-a-plazo/liquidar/sin-cuenta-vista',
            'eve-cat' : 'Click',
			'eve-lab' : 'Solicitar Cuenta Vista'
		}
	});
</script>
```

Si hace click en **< No por ahora**, se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/mis-productos/deposito-a-plazo/liquidar/sin-cuenta-vista',
            'eve-cat' : 'Click',
			'eve-lab' : 'No por ahora'
		}
	});
</script>
```