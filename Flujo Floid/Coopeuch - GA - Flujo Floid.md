<img align="left" width="19%" height="19%" src="https://gitlab.com/d2b_chile/formato-documento-markdown/-/raw/master/Imagenes/D2B-GRIS-POSITIVO-CON BAJADA-01.png">

# **Documento de Implementación** para Flujo de Evaluación de TC a través de Flöid  (Google Analytics)

Abril 2021





## Sobre el Documento

El siguiente documento describe las variables a ser utilizadas en cada marca, se debe copiar el script tal cual como viene indicado en cada paso. Cuando aparezca un  **`%variable%`**, corresponde a una variable que se debe reemplazar según el dato que corresponda de acuerdo al paso o sección en que se encuentre.

______

































## Scripts

###  Sí Está en Base de Riesgo para Piloto

#### Solicitud TC



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Flujo Floid\img\flujo_floid-si_bdr_p1.png)

Cuando el socio ingrese a esta página, se debe ejecutar el siguiente script:

```html
<script>
dataLayer.push({
	'event': 'vpv',
	'page' : '/evaluacion-tc-floid/solicitud-tc/'
});
</script>
```

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Flujo Floid\img\flujo_floid-si_bdr_p1_comenzar.png)
Cuando el socio haga click en el botón **Comenzar** se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/evaluacion-tc-floid/solicitud-tc',
            'eve-cat' : 'Click',
			'eve-lab' : 'Comenzar'
		}
	});
</script>
```

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Flujo Floid\img\flujo_floid-si_bdr_p1_ver_documentos.png)

Si el socio hace click en **Ver Documentos** se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/evaluacion-tc-floid/solicitud-tc',
            'eve-cat' : 'Click',
			'eve-lab' : 'Ver Documentos'
		}
	});
</script>
```


Si el socio hace click en el enlace **Volver a Coopeuch**, se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/evaluacion-tc-floid/solicitud-tc',
            'eve-cat' : 'Click',
			'eve-lab' : 'Volver a Coopeuch'
		}
	});
</script>
```

#### Dónde retirar la tarjeta

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Flujo Floid\img\flujo_floid-si_bdr_p2.png)

Cuando el socio ingrese a esta página, se debe ejecutar el siguiente script:. 

```html
<script>
dataLayer.push({
	'event': 'vpv',
	'page' : '/evaluacion-tc-floid/donde-retirar-tc/'
});
</script>
```

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Flujo Floid\img\flujo_floid-si_bdr_p2_continuar.png)

Cuando el socio haga click en **Continuar**, se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/evaluacion-tc-floid/donde-retirar-tc/',
            'eve-cat' : 'Click',
			'eve-lab' : 'Continuar'
		}
	});
</script>
```



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Flujo Floid\img\flujo_floid-si_bdr_p2_volver.png)

Cuando el socio haga click en **Volver**, se debe ejecutar el siguiente script:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/evaluacion-tc-floid/donde-retirar-tc/',
            'eve-cat' : 'Click',
			'eve-lab' : 'Volver'
		}
	});
</script>
```



#### Confirmación Datos Personales



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Flujo Floid\img\flujo_floid-si_bdr_p3.png)



Cuando el socio ingrese a esta página, se debe ejecutar el siguiente script:



```html
<script>
dataLayer.push({
	'event': 'vpv',
	'page' : '/evaluacion-tc-floid/confirmacion-datos-personales/'
});
</script>
```



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Flujo Floid\img\flujo_floid-si_bdr_p3_continuar.png)



Si el socio hace click en el botón **Continuar**, se debe ejecutar el siguiente script:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/evaluacion-tc-floid/confirmacion-datos-personales/',
            'eve-cat' : 'Click',
			'eve-lab' : 'Continuar'
		}
	});
</script>
```


![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Flujo Floid\img\flujo_floid-si_bdr_p3_donde_encontrarlo.png)



Cuando el socio haga click en el enlace **¿Dónde encontrarlo?**, se debe ejecutar el siguiente script:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/evaluacion-tc-floid/confirmacion-datos-personales/',
            'eve-cat' : 'Click',
			'eve-lab' : 'Donde encontrarlo'
		}
	});
</script>
```



##### ¿Dónde está el número de serie?



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Flujo Floid\img\flujo_floid-si_bdr_p3_ns.png)



Cuando se cargue esta vista en el dispositivo del socio, se debe ejecutar el siguiente script. Donde ```%ubicacion%```  corresponde al lugar del flujo en el que se muestra el botón:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/evaluacion-tc-floid/%ubicacion%/',
            'eve-cat' : 'Ayuda',
			'eve-lab' : 'Numero de serie documento'
		}
	});
</script>
```



Cuando el socio haga click en el botón **Volver**, se debe ejecutar el script. Donde ```%ubicacion%```  corresponde al lugar del flujo en el que se muestra el botón:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/evaluacion-tc-floid/%ubicacion%/',
            'eve-cat' : 'Ayuda',
			'eve-lab' : 'Volver'
		}
	});
</script>
```



#### Autorización a Datos



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Flujo Floid\img\flujo_floid-si_bdr_p4.png)

Cuando el socio ingrese a esta página, debe ejecutarse el siguiente script:



```html
<script>
dataLayer.push({
	'event': 'vpv',
	'page' : '/evaluacion-tc-floid/firma-digital-autorizacion/'
});
</script>
```

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Flujo Floid\img\flujo_floid-si_bdr_p4_continuar.png)



Cuando el socio autorice, haciendo click en **Sí, autorizo**, y luego haga click en el botón **Continuar**, se debe ejecutar el siguiente script:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/evaluacion-tc-floid/firma-digital-autorizacion/',
            'eve-cat' : 'Click',
			'eve-lab' : 'Continuar'
		}
	});
</script>
```



##### Socio Cancela  "Autorización a Datos"



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Flujo Floid\img\flujo_floid-si_bdr_p4_cancelar.png)



Si el socio hace click en el botón **Cancelar** en esta página, se debe ejecutar el siguiente script:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/evaluacion-tc-floid/firma-digital-autorizacion/',
            'eve-cat' : 'Click',
			'eve-lab' : 'Cancelar'
		}
	});
</script>
```



Luego, se desplegaría la siguiente advertencia:



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Flujo Floid\img\flujo_floid-si_bdr_p4_cancelar_vista.png)



Cuando aparezca esta advertencia en el dispositivo del socio, se debe ejecutar el siguiente script:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/evaluacion-tc-floid/firma-digital-autorizacion/',
            'eve-cat' : 'Error',
			'eve-lab' : 'Cancelar'
		}
	});
</script>
```



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Flujo Floid\img\flujo_floid-si_bdr_p4_cancelar_vista_cancelar.png)



Si el cliente hace click en el botón **Si, Cancelar**, se debe ejecutar el siguiente script:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/evaluacion-tc-floid/firma-digital-autorizacion/',
            'eve-cat' : 'Click',
			'eve-lab' : 'Cancelar Firma Digital Autorizacion'
		}
	});
</script>
```



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Flujo Floid\img\flujo_floid-si_bdr_p4_cancelar_vista_volver.png)



Si el cliente hace click en el botón **Volver**, se debe ejecutar el siguiente script:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/evaluacion-tc-floid/firma-digital-autorizacion/',
            'eve-cat' : 'Click',
			'eve-lab' : 'Volver a Firma Digital Autorizacion'
		}
	});
</script>
```



#### FAQs e Introducción a Floid



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Flujo Floid\img\flujo_floid-si_bdr_p5.png)



Cuando el socio ingrese a esta página, se debe ejecutar el siguiente script:



```html
<script>
dataLayer.push({
	'event': 'vpv',
	'page' : '/evaluacion-tc-floid/faqs-introduccion-floid/'
});
</script>
```



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Flujo Floid\img\flujo_floid-si_bdr_p5_comenzar.png)



Cuando el socio haga click en el botón **Comenzar**, se debe ejecutar el siguiente script:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/evaluacion-tc-floid/faqs-introduccion-floid/',
            'eve-cat' : 'Click',
			'eve-lab' : 'Comenzar'
		}
	});
</script>
```



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Flujo Floid\img\flujo_floid-si_bdr_p5_volver.png)



Cuando el cliente haga click en **< Volver**, se debe ejecutar el siguiente script:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/evaluacion-tc-floid/faqs-introduccion-floid/',
            'eve-cat' : 'Click',
			'eve-lab' : 'Volver'
		}
	});
</script>
```



#### iFrame de Flöid



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Flujo Floid\img\flujo_floid-si_bdr_p6.png)



Al ejecutarse el iFrame de Flöid que permite la autorización digital de datos, y la conexión con el banco, podremos ver un  enlace **¿Por qué debo ingresar mis datos bancarios?**, en la parte inferior del iFrame en cada una de las pantallas del proceso.

Una vez el socio haga click en este enlace, se debe ejecutar el siguiente script:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/evaluacion-tc-floid/iframe-floid/',
            'eve-cat' : 'Click',
			'eve-lab' : 'Ayuda sobre datos bancarios'
		}
	});
</script>
```
