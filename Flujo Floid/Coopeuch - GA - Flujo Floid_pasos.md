<img align="left" width="19%" height="19%" src="https://gitlab.com/d2b_chile/formato-documento-markdown/-/raw/master/Imagenes/D2B-GRIS-POSITIVO-CON BAJADA-01.png">

# **Documento de Implementación** para Flujo de Evaluación de TC a través de Flöid  (Google Analytics)

Abril 2021





## Sobre el Documento

El siguiente documento describe las variables a ser utilizadas en cada marca, se debe copiar el script tal cual como viene indicado en cada paso. Cuando aparezca un  **`%variable%`**, corresponde a una variable que se debe reemplazar según el dato que corresponda de acuerdo al paso o sección en que se encuentre.

______

































## Scripts

#### Paso 1 - Solicitud TC



![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Floid/flujo_floid-si_bdr_p1.png)

Cuando el socio ingrese a esta página, se debe ejecutar el siguiente script:

```html
<script>
dataLayer.push({
	'event': 'vpv',
	'page' : '/evaluacion-tc-floid/paso-1/'
});
</script>
```

![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Floid/flujo_floid-si_bdr_p1_comenzar.png)
Cuando el socio haga click en el botón **Comenzar** se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/evaluacion-tc-floid/paso-1/',
            'eve-cat' : 'Click',
			'eve-lab' : 'Comenzar'
		}
	});
</script>
```

![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Floid/flujo_floid-si_bdr_p1_ver_documentos.png)

Si el socio hace click en **Ver Documentos** se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/evaluacion-tc-floid/paso-1/',
            'eve-cat' : 'Click',
			'eve-lab' : 'Ver Documentos'
		}
	});
</script>
```


Si el socio hace click en el enlace **Volver a Coopeuch**, se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/evaluacion-tc-floid/paso-1/',
            'eve-cat' : 'Click',
			'eve-lab' : 'Volver a Coopeuch'
		}
	});
</script>
```



#### Paso 2 - Dónde retirar la tarjeta



![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Floid/flujo_floid-si_bdr_p2.png)

Cuando el socio ingrese a esta página, se debe ejecutar el siguiente script:. 

```html
<script>
dataLayer.push({
	'event': 'vpv',
	'page' : '/evaluacion-tc-floid/paso-2/'
});
</script>
```

![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Floid/flujo_floid-si_bdr_p2_continuar.png)

Cuando el socio haga click en **Continuar**, se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/evaluacion-tc-floid/paso-2/',
            'eve-cat' : 'Click',
			'eve-lab' : 'Continuar'
		}
	});
</script>
```



![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Floid/flujo_floid-si_bdr_p2_volver.png)

Cuando el socio haga click en **Volver**, se debe ejecutar el siguiente script:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/evaluacion-tc-floid/paso-2/',
            'eve-cat' : 'Click',
			'eve-lab' : 'Volver'
		}
	});
</script>
```



#### Paso 3 - Confirmación Datos Personales



![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Floid/flujo_floid-si_bdr_p3.png)



Cuando el socio ingrese a esta página, se debe ejecutar el siguiente script:



```html
<script>
dataLayer.push({
	'event': 'vpv',
	'page' : '/evaluacion-tc-floid/paso-3/'
});
</script>
```



![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Floid/flujo_floid-si_bdr_p3_continuar.png)



Si el socio hace click en el botón **Continuar**, se debe ejecutar el siguiente script:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/evaluacion-tc-floid/paso-3/',
            'eve-cat' : 'Click',
			'eve-lab' : 'Continuar'
		}
	});
</script>
```


![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Floid/flujo_floid-si_bdr_p3_donde_encontrarlo.png)



Cuando el socio haga click en el enlace **¿Dónde encontrarlo?**, se debe ejecutar el siguiente script:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/evaluacion-tc-floid/paso-3/',
            'eve-cat' : 'Click',
			'eve-lab' : 'Donde encontrarlo'
		}
	});
</script>
```



##### ¿Dónde está el número de serie?



![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Floid/flujo_floid-si_bdr_p3_ns.png)



Cuando se cargue esta vista en el dispositivo del socio, se debe ejecutar el siguiente script. 

```html
<script>
dataLayer.push({
	'event': 'vpv',
	'page' : '/evaluacion-tc-floid/modal-numero-serie/'
});
</script>
```



Cuando el socio haga click en el botón **Volver**, se debe ejecutar el script. Donde ```%ubicacion%```  corresponde al lugar del flujo en el que se muestra el botón:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/evaluacion-tc-floid/modal-numero-serie/',
            'eve-cat' : 'Ayuda',
			'eve-lab' : 'Volver'
		}
	});
</script>
```



#### Paso 4 - Autorización a Datos



![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Floid/flujo_floid-si_bdr_p4.png)

Cuando el socio ingrese a esta página, debe ejecutarse el siguiente script:



```html
<script>
dataLayer.push({
	'event': 'vpv',
	'page' : '/evaluacion-tc-floid/paso-4/'
});
</script>
```

![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Floid/flujo_floid-si_bdr_p4_continuar.png)



Cuando el socio haga click en **Continuar**, se debe ejecutar el siguiente script:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/evaluacion-tc-floid/paso-4/',
            'eve-cat' : 'Click',
			'eve-lab' : 'Continuar'
		}
	});
</script>
```



##### Socio Cancela  "Autorización a Datos"



![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Floid/flujo_floid-si_bdr_p4_cancelar.png)



Si el socio hace click en el botón **Cancelar** en esta página, se debe ejecutar el siguiente script:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/evaluacion-tc-floid/paso-4/',
            'eve-cat' : 'Click',
			'eve-lab' : 'Cancelar'
		}
	});
</script>
```



Luego, se desplegaría la siguiente advertencia:



![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Floid/flujo_floid-si_bdr_p4_cancelar_vista.png)



Cuando aparezca este mensaje, se debe ejecutar el siguiente script:

```html
<script>
dataLayer.push({
	'event': 'vpv',
	'page' : '/evaluacion-tc-floid/paso-4-cancelar'
});
</script>
```



![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Floid/flujo_floid-si_bdr_p4_cancelar_vista_cancelar.png)



Si el cliente hace click en el botón **Si, Cancelar**, se debe ejecutar el siguiente script:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/evaluacion-tc-floid/paso-4-cancelar',
            'eve-cat' : 'Click',
			'eve-lab' : 'Cancelar Firma Digital Autorizacion'
		}
	});
</script>
```



![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Floid/flujo_floid-si_bdr_p4_cancelar_vista_volver.png)



Si el cliente hace click en el botón **Volver**, se debe ejecutar el siguiente script:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/evaluacion-tc-floid/paso-4-cancelar',
            'eve-cat' : 'Click',
			'eve-lab' : 'Volver a Firma Digital Autorizacion'
		}
	});
</script>
```



#### Paso 5 - FAQs e Introducción a Floid



![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Floid/flujo_floid-si_bdr_p5.png)

Cuando el socio ingrese a esta página, se debe ejecutar el siguiente script:



```html
<script>
dataLayer.push({
	'event': 'vpv',
	'page' : '/evaluacion-tc-floid/paso-5/'
});
</script>
```



![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Floid/flujo_floid-si_bdr_p5_comenzar.png)



Cuando el socio haga click en el botón **Comenzar**, se debe ejecutar el siguiente script:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/evaluacion-tc-floid/paso-5/',
            'eve-cat' : 'Click',
			'eve-lab' : 'Comenzar'
		}
	});
</script>
```



![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Floid/flujo_floid-si_bdr_p5_volver.png)



Cuando el cliente haga click en **< Volver**, se debe ejecutar el siguiente script:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/evaluacion-tc-floid/paso-5/',
            'eve-cat' : 'Click',
			'eve-lab' : 'Volver'
		}
	});
</script>
```



#### Paso 6 - iFrame de Flöid



![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Floid/flujo_floid-si_bdr_p6.png)



Al ejecutarse el iFrame de Flöid que permite la autorización digital de datos, y la conexión con el banco, podremos ver un  enlace **¿Por qué debo ingresar mis datos bancarios?**, en la parte inferior del iFrame en cada una de las pantallas del proceso.

Una vez el socio haga click en este enlace, se debe ejecutar el siguiente script:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/evaluacion-tc-floid/paso-6/',
            'eve-cat' : 'Click',
			'eve-lab' : 'Ayuda sobre datos bancarios'
		}
	});
</script>
```



#### Apéndice errores

##### No podemos continuar con solicitud

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Flujo Floid\img\flujo_floid-error-no_continuar.png)

Cuando este mensaje de error aparezca en el dispositivo del socio, se debe ejecutar el siguiente script. Donde ```%ubicacion%```  corresponde al lugar del flujo en el que se muestra el mismo:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/evaluacion-tc-floid/%ubicacion&/',
            'eve-cat' : 'Error',
			'eve-lab' : 'No podemos continuar con solicitud'
		}
	});
</script>
```



#####  Ya tienes una solicitud en curso



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Flujo Floid\img\flujo_floid-error-posee_tdc.png)

Cuando este mensaje de error aparezca en el dispositivo del socio, se debe ejecutar el siguiente script. Donde ```%ubicacion%```  corresponde al lugar del flujo en el que se muestra el mismo:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/evaluacion-tc-floid/%ubicacion&/',
            'eve-cat' : 'Error',
			'eve-lab' : 'Solicitud de tc en curso'
		}
	});
</script>
```



##### CRM no responde - "No podemos procesar solicitud"


![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Flujo Floid\img\flujo_floid-error-crm_no_responde.png)



Cuando este mensaje de error aparezca en el dispositivo del socio, se debe ejecutar el siguiente script. Donde ```%ubicacion%```  corresponde al lugar del flujo en el que se muestra el mismo:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/evaluacion-tc-floid/%ubicacion&/',
            'eve-cat' : 'Error',
			'eve-lab' : 'CRM no responde'
		}
	});
</script>
```



_Nota: Este error está etiquetado como "CRM no responde" dadas las instrucciones ofrecidas por Paulina Tapia. Se considera que la mejor alternativa para identificar este mensaje es la falla, más que el mensaje que aparece en el dispositivo del socio_



##### Error en el envío de datos



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Flujo Floid\img\flujo_floid-error-error_envio_datos.png)



Cuando este mensaje de error aparezca en el dispositivo del socio, se debe ejecutar el siguiente script. Donde ```%ubicacion%```  corresponde al lugar del flujo en el que se muestra el mismo:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/evaluacion-tc-floid/%ubicacion&/',
            'eve-cat' : 'Error',
			'eve-lab' : 'Clave no compatible con RUT'
		}
	});
</script>
```