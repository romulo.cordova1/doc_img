<img align="left" width="19%" height="19%" src="https://gitlab.com/d2b_chile/formato-documento-markdown/-/raw/master/Imagenes/D2B-GRIS-POSITIVO-CON BAJADA-01.png">

# **Documento de Implementación** Creación y Recuperación de clave  (Google Analytics)

Marzo 2021





## Sobre el Documento

El siguiente documento describe las variables a ser utilizadas en cada marca, se debe copiar el script tal cual como viene indicado en cada paso. Cuando aparezca un  **`%variable%`**, corresponde a una variable que se debe reemplazar según el dato que corresponda de acuerdo al paso o sección en que se encuentre.

______

































## Desktop Scripts - Creación de Clave

#### Página de ingreso

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_recuperacion_clave_inicio.png)

Cuando el socio ingrese en el landing de login, se debe ejecutar el siguiente script:

```html
<script>
dataLayer.push({
	'event': 'vpv',
	'page' : '/ingreso/sitio-privado'
});
</script>
```


#### Crea tu Clave



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-1_click-recuperar-clave.png)



Cuando el socio haga click en "**Crea tu Clave**" en el landing de login, se debe ejecutar el siguiente script:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/ingreso/sitio-privado',
            'eve-cat' : 'Click',
			'eve-lab' : 'Crea tu clave'
		}
	});
</script>
```



##### Errores en Página de ingreso



###### RUT Obligatorio



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_recuperacion_clave_rut-obligatorio.png)

Cuando aparezca este mensaje en el dispositivo del socio, se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/ingreso/sitio-privado',
            'eve-cat' : 'Error',
			'eve-lab' : 'El RUT es obligatorio'
		}
	});
</script>
```

###### RUT No válido
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_recuperacion_clave_rut-invalido.png)

Cuando aparezca este mensaje en el dispositivo del socio, se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/ingreso/sitio-privado',
            'eve-cat' : 'Error',
			'eve-lab' : 'El RUT no es válido'
		}
	});
</script>
```

###### Clave debe tener 6 y 8 caracteres
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_recuperacion_clave_6-8caracteres.png)

Cuando aparezca este mensaje en el dispositivo del socio, se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/ingreso/sitio-privado',
            'eve-cat' : 'Error',
			'eve-lab' : 'Clave debe tener entre 6 y 8 caracteres'
		}
	});
</script>
```

###### Necesitas crear una clave
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_recuperacion_clave_error-crear-una-clave.png)

Cuando aparezca este mensaje en el dispositivo del socio, se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/ingreso/sitio-privado',
            'eve-cat' : 'Error',
			'eve-lab' : 'Debes crear una nueva clave'
		}
	});
</script>
```

###### Clave Bloqueada
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_recuperacion_clave_clave-bloqueada.png)

Cuando aparezca este mensaje en el dispositivo del socio, se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/ingreso/sitio-privado',
            'eve-cat' : 'Error',
			'eve-lab' : 'Clave Bloqueada'
		}
	});
</script>
```

###### Datos incorrectos. Inténtalo nuevamente
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_recuperacion_clave_datos-incorrectos.png)

Cuando aparezca este mensaje en el dispositivo del socio, se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/ingreso/sitio-privado',
            'eve-cat' : 'Error',
			'eve-lab' : 'Datos ingresados son incorrectos'
		}
	});
</script>
```

###### RUT no pertenece a un registro asociado
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_recuperacion_clave_rut-no-esta-registrado.png)



Cuando aparezca este mensaje en el dispositivo del socio, se debe ejecutar el siguiente script:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/ingreso/sitio-privado',
            'eve-cat' : 'Error',
			'eve-lab' : 'Datos ingresados son incorrectos'
		}
	});
</script>
```


###### Sesión ha expirado por inactividad


![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_recuperacion_clave_sesion-expirada.png)



Cuando aparezca este mensaje en el dispositivo del socio, se debe ejecutar el siguiente script:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/ingreso/sitio-privado',
            'eve-cat' : 'Error',
			'eve-lab' : 'Sesión expirada por inactividad'
		}
	});
</script>
```



###### Nuestro servicio no se encuentra disponible



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_recuperacion_clave_servicio-no-disponible.png)



Cuando aparezca este mensaje en el dispositivo del socio, se debe ejecutar el siguiente script:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/ingreso/sitio-privado',
            'eve-cat' : 'Error',
			'eve-lab' : 'Servicio no disponible'
		}
	});
</script>
```


#### Paso 1 - Ingreso de RUT



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-1.png)



Cuando el socio ingrese en esta página, se debe ejecutar el siguiente script:



```html
<script>
dataLayer.push({
	'event': 'vpv',
	'page' : '/creacion-clave/paso1'
});
</script>
```


##### Errores en Paso 1 - Ingreso de RUT



###### Número de serie no corresponde



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-1_numero-de-serie-no-corresponde.png)



Cuando aparezca este mensaje en el dispositivo del socio, se debe ejecutar el siguiente script:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/creacion-clave/paso1',
            'eve-cat' : 'Error',
			'eve-lab' : 'Número de serie no corresponde'
		}
	});
</script>
```


###### Tooltip de número de serie



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-1_tooltip-no-serie.png)



Cuando el socio haga click en el tooltip, al lado de "**Número de serie**", se debe ejecutar el siguiente script:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/creacion-clave/paso1',
            'eve-cat' : 'Error',
			'eve-lab' : 'Número de serie no corresponde'
		}
	});
</script>
```


#### Paso 2 [Tarjeta de Coordenadas] - Método para crear clave



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-2.png)



Cuando el socio ingrese en esta página, se debe ejecutar el siguiente script:



```html
<script>
dataLayer.push({
	'event': 'vpv',
	'page' : '/creacion-clave/paso2'
});
</script>
```



##### Método: Tarjeta de Coordenadas



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-2_tarjeta-de-coordenadas.png)



Cuando el socio seleccione el método "**Tarjeta de Coordenadas**", y luego haga click en el botón "**Continuar**", se deb ejecutar el siguiente script. Donde ``%metodo-validacion%`` corresponde al método seleccionado.



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-validacion%',
		'event-config': {
			'eve-acc' : '/creacion-clave/paso2',
            'eve-cat' : 'Click',
			'eve-lab' : 'Continuar'
		}
	});
</script>
```



#### Paso 3 [Tarjeta de Coordenadas] - Validación



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-3.png)



Cuando el socio ingrese en esta página, se debe ejecutar el siguiente script. Donde ``%metodo-validacion%`` corresponde al método seleccionado en el paso 2:



```html
<script>
dataLayer.push({
	'producto':'%metodo-validacion%',
	'event': 'vpv',
	'page' : '/creacion-clave/paso3'
});
</script>
```



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-3-click_autorizar.png)



Cuando el socio haga click en el botón "**Autorizar**", se debe ejecutar el siguiente script. Donde ``%metodo-validacion%`` corresponde al método seleccionado en el paso 2.



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-validacion%',
		'event-config': {
			'eve-acc' : '/creacion-clave/paso3',
            'eve-cat' : 'Click',
			'eve-lab' : 'Validar con Tarjeta de Coordenadas'
		}
	});
</script>
```


Cuando el socio ingrese las coordenadas correctamente se habilitará el botón "**Autorizar**". Cuando el socio haga click en este botón, se debe ejecutar el siguiente script. Donde ``%metodo-validacion%`` corresponde al método seleccionado en el paso 2.



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-validacion%',,
		'event-config': {
			'eve-acc' : '/creacion-clave/paso3',
            'eve-cat' : 'Click',
			'eve-lab' : 'Autorizar Tarjeta de Coordenadas'
		}
	});
</script>
```


##### Errores en Paso 3 [Tarjeta de Coordenadas] - Validación


######  Tiempo agotado

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-3-error_tiempo_agotado.png)



Cuando se muestre este error en el dispositivo del socio, se debe ejecutar el siguiente script. Donde ``%metodo-validacion%`` corresponde al método seleccionado en el paso 2:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-validacion%',
		'event-config': {
			'eve-acc' : '/creacion-clave/paso3',
            'eve-cat' : 'Error',
			'eve-lab' : 'Se agoto el tiempo'
		}
	});
</script>
```



######  Coordenadas no corresponden

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-3-error_coordenadas_erroneas.png)



Cuando se muestre este error en el dispositivo del socio, se debe ejecutar el siguiente script. Donde ``%metodo-validacion%`` corresponde al método seleccionado en el paso 2:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-validacion%',
		'event-config': {
			'eve-acc' : '/creacion-clave/paso3',
            'eve-cat' : 'Error',
			'eve-lab' : 'Coordenadas no corresponden'
		}
	});
</script>
```



######  Operación rechazada

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-3-error_operacion-rechazada.png)



Cuando se muestre este error en el dispositivo del socio, se debe ejecutar el siguiente script. Donde ``%metodo-validacion%`` corresponde al método seleccionado en el paso 2:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-validacion%',
		'event-config': {
			'eve-acc' : '/creacion-clave/paso3',
            'eve-cat' : 'Error',
			'eve-lab' : 'Operacion rechazada'
		}
	});
</script>
```



######  Tarjeta de Coordenadas bloqueada

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-3-error_tarjeta-bloqueada.png)



Cuando se muestre este error en el dispositivo del socio, se debe ejecutar el siguiente script. Donde ``%metodo-validacion%`` corresponde al método seleccionado en el paso 2:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-validacion%',
		'event-config': {
			'eve-acc' : '/creacion-clave/paso3',
            'eve-cat' : 'Error',
			'eve-lab' : 'Tarjeta de Coordenadas bloqueada'
		}
	});
</script>
```



######  Operación no ha podido ser realizada

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-3-error_imposible-realizar-operacion.png)



Cuando se muestre este error en el dispositivo del socio, se debe ejecutar el siguiente script. Donde ``%metodo-validacion%`` corresponde al método seleccionado en el paso 2:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-validacion%',
		'event-config': {
			'eve-acc' : '/creacion-clave/paso3',
            'eve-cat' : 'Error',
			'eve-lab' : 'Operacion no ha podido ser realizada'
		}
	});
</script>
```



#### Paso 4 [Tarjeta de Coordenadas] - Creación de Clave

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-4.png)

Cuando el socio ingrese en esta página, se debe ejecutar el siguiente script. Donde ``%metodo-validacion%`` corresponde al método seleccionado en el paso 2:

```html
<script>
dataLayer.push({
	'producto':'%metodo-validacion%',
	'event': 'vpv',
	'page' : '/creacion-clave/paso4'
});
</script>
```



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-4-crear_clave.png)



Cuando el socio haya llenado los campos "**Nueva Clave**" y "**Confirmación**" sin errores, se habilite el botón "**Crear Clave**", y haga click en el mismo, se debe ejecutar el siguiente script. Donde ``%metodo-validacion%`` corresponde al método seleccionado en el paso 2:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-validacion%',
		'event-config': {
			'eve-acc' : '/creacion-clave/paso4',
            'eve-cat' : 'Click',
			'eve-lab' : 'Crear Clave'
		}
	});
</script>
```



##### Clave creada con éxito



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-4-creacion_clave_exitosa.png)



Cuando el socio ingrese en esta página, se debe ejecutar el siguiente script. Donde ``%metodo-validacion%`` corresponde al método seleccionado en el paso 2:



```html
<script>
dataLayer.push({
	'producto':'%metodo-validacion%',
	'event': 'vpv',
	'page' : '/creacion-clave/exito'
});
</script>
```



##### Errores en Paso 4 [Tarjeta de Coordenadas] - Creación de Clave
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-4-creacion_clave_errores.png)

Cuando cualquiera de los siguientes errores aparezcan en el dispositivo del socio:

- "**Tu clave no coincide**"
- "**Tu clave debe tener 6 a 8 caracteres**"
- "**Tu clave debe ser alfanumérica**"
- "**Tu clave debe tener 1 mayúscula**"
- "**Tu clave no tiene que tener espacios**"
- "**Tu clave tiene que tener 1 número**"
- "**La contraseña debe ser distinta a la anterior**"

se debe ejecutar el siguiente script. Donde ``%metodo-validacion%`` corresponde al método seleccionado en el paso 2 y ``%error-clave%``sería el mensaje arrojado debajo de los campos de textos como error.

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-validacion%',
		'event-config': {
			'eve-acc' : '/creacion-clave/paso4',
            'eve-cat' : 'Error',
			'eve-lab' : '%error-clave%'
		}
	});
</script>
```

#### Paso 2 [Clave SMS] - Método para crear clave


![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-2.png)



Cuando el socio ingrese en esta página, se debe ejecutar el siguiente script:



```html
<script>
dataLayer.push({
	'event': 'vpv',
	'page' : '/creacion-clave/paso2'
});
</script>
```


##### Método: Clave SMS



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-2_clave-sms.png)



Cuando el socio seleccione el método "**SMS**", y luego haga click en el botón "**Continuar**", se debe ejecutar el siguiente script. Donde ``%metodo-validacion%`` corresponde al método seleccionado.



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-validacion%',
		'event-config': {
			'eve-acc' : '/creacion-clave/paso2',
            'eve-cat' : 'Click',
			'eve-lab' : 'Continuar'
		}
	});
</script>
```



##### Errores en Método: Clave SMS

###### Sin teléfono asociado

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-2_clave-sms_sin-telefono-asociado.png)

Cuando aparezca este mensaje en el dispositivo del socio, se debe ejecutar el siguiente script. Donde ``%metodo-validacion%`` corresponde al método seleccionado en el paso 2.

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-validacion%',
		'event-config': {
			'eve-acc' : '/creacion-clave/paso2',
            'eve-cat' : 'Error',
			'eve-lab' : 'Sin telefono asociado'
		}
	});
</script>
```
###### Número bloqueado

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-2_clave-sms_numero-bloqueado.png)

Cuando aparezca este mensaje en el dispositivo del socio, se debe ejecutar el siguiente script. Donde ``%metodo-validacion%`` corresponde al método seleccionado en el paso 2.

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-validacion%',
		'event-config': {
			'eve-acc' : '/creacion-clave/paso2',
            'eve-cat' : 'Error',
			'eve-lab' : 'Numero de telefono bloqueado'
		}
	});
</script>
```


#### Paso 3 [Clave SMS] - Validación

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-3_sms.png)


Cuando el socio ingrese en esta página, se debe ejecutar el siguiente script. Donde ``%metodo-validacion%`` corresponde al método seleccionado en el paso 2:


```html
<script>
dataLayer.push({
	'producto':'%metodo-validacion%',
	'event': 'vpv',
	'page' : '/creacion-clave/paso3'
});
</script>
```


![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-3-sms_click_autorizar.png)



Cuando el socio haga click en el botón "**Validar**", se debe ejecutar el siguiente script. Donde ``%metodo-validacion%`` corresponde al método seleccionado en el paso 2.

_Nota: en el inVision no está la vista del flujo donde el botón de "**Validar**". Supuse que podría ser pinchado una vez el usuario ingresara el código de seguridad válido._



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-validacion%',
		'event-config': {
			'eve-acc' : '/creacion-clave/paso3',
            'eve-cat' : 'Click',
			'eve-lab' : 'Validar con codigo sms'
		}
	});
</script>
```



Si el socio hace click en "**Aún no recibo el código, enviar nuevamente**", se debe ejecutar el siguiente script. Donde ``%metodo-validacion%`` corresponde al método seleccionado en el paso 2.



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-validacion%',,
		'event-config': {
			'eve-acc' : '/creacion-clave/paso3',
            'eve-cat' : 'Click',
			'eve-lab' : 'Enviar codigo nuevamente'
		}
	});
</script>
```


#### Errores en Paso 3 [Clave SMS] - Validación

######  Código no válido

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-3-sms_codigo-no-valido.png)



Cuando se muestre este error en el dispositivo del socio, se debe ejecutar el siguiente script. Donde ``%metodo-validacion%`` corresponde al método seleccionado en el paso 2:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-validacion%',
		'event-config': {
			'eve-acc' : '/creacion-clave/paso3',
            'eve-cat' : 'Error',
			'eve-lab' : 'Codigo no valido'
		}
	});
</script>
```



######  Código caducado

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-3-sms_codigo-caducado.png)



Cuando se muestre este error en el dispositivo del socio, se debe ejecutar el siguiente script. Donde ``%metodo-validacion%`` corresponde al método seleccionado en el paso 2:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-validacion%',
		'event-config': {
			'eve-acc' : '/creacion-clave/paso3',
            'eve-cat' : 'Error',
			'eve-lab' : 'Codigo caducado'
		}
	});
</script>
```


###### Reintentar

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-3-sms_reintentar.png)

Cuando el socio haga click en "**Reintentar**", se debe ejecutar el siguiente script. Donde ``%metodo-validacion%`` corresponde al método seleccionado en el paso 2:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-validacion%',
		'event-config': {
			'eve-acc' : '/creacion-clave/paso3',
            'eve-cat' : 'Click',
			'eve-lab' : 'Reintentar'
		}
	});
</script>
```

###### Código SMS bloqueado

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-3-sms_codigo-sms-bloqueado.png)

Cuando se muestre este error en el dispositivo del socio, se debe ejecutar el siguiente script. Donde ``%metodo-validacion%`` corresponde al método seleccionado en el paso 2:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-validacion%',
		'event-config': {
			'eve-acc' : '/creacion-clave/paso3',
            'eve-cat' : 'Error',
			'eve-lab' : 'Codigo sms bloqueado'
		}
	});
</script>
```

#### Paso 4 [Clave SMS] - Creación de Clave

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-4.png)

Cuando el socio ingrese en esta página, se debe ejecutar el siguiente script. Donde ``%metodo-validacion%`` corresponde al método seleccionado en el paso 2:

```html
<script>
dataLayer.push({
	'producto':'%metodo-validacion%',
	'event': 'vpv',
	'page' : '/creacion-clave/paso4'
});
</script>
```



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-4-crear_clave.png)



Cuando el socio haya llenado los campos "**Nueva Clave**" y "**Confirmación**" sin errores, se habilite el botón "**Crear Clave**", y haga click en el mismo, se debe ejecutar el siguiente script. Donde ``%metodo-validacion%`` corresponde al método seleccionado en el paso 2:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-validacion%',
		'event-config': {
			'eve-acc' : '/creacion-clave/paso4',
            'eve-cat' : 'Click',
			'eve-lab' : 'Crear Clave'
		}
	});
</script>
```



###### Clave creada con éxito



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-4-creacion_clave_exitosa.png)



Cuando el socio ingrese en esta página, se debe ejecutar el siguiente script. Donde ``%metodo-validacion%`` corresponde al método seleccionado en el paso 2:



```html
<script>
dataLayer.push({
	'producto':'%metodo-validacion%',
	'event': 'vpv',
	'page' : '/creacion-clave/exito'
});
</script>
```



#### Errores en Paso 4 [Clave SMS] - Creación de Clave
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-4-creacion_clave_errores.png)

Cuando cualquiera de los siguientes errores aparezcan en el dispositivo del socio:

- "**Tu clave no coincide**"
- "**Tu clave debe tener 6 a 8 caracteres**"
- "**Tu clave debe ser alfanumérica**"
- "**Tu clave debe tener 1 mayúscula**"
- "**Tu clave no tiene que tener espacios**"
- "**Tu clave tiene que tener 1 número**"
- "**La contraseña debe ser distinta a la anterior**"

se debe ejecutar el siguiente script. Donde ``%metodo-validacion%`` corresponde al método seleccionado en el paso 2 y ``%error-clave%``sería el mensaje arrojado debajo de los campos de textos como error.

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-validacion%',
		'event-config': {
			'eve-acc' : '/creacion-clave/paso4',
            'eve-cat' : 'Error',
			'eve-lab' : '%error-clave%'
		}
	});
</script>
```



## Desktop Scripts - Recuperación de Clave

#### Página de ingreso

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_recuperacion_clave_inicio.png)

Cuando el socio ingrese en el landing de login, se debe ejecutar el siguiente script:

```html
<script>
dataLayer.push({
	'event': 'vpv',
	'page' : '/ingreso/sitio-privado'
});
</script>
```


#### Recupera tu Clave



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-1_click-recuperar-clave.png)



Cuando el socio haga click en "**Recupera tu clave**" en el landing de login, se debe ejecutar el siguiente script:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/ingreso/sitio-privado',
            'eve-cat' : 'Click',
			'eve-lab' : 'Recupera tu clave'
		}
	});
</script>
```



#### Errores en Página de ingreso

##### RUT Obligatorio
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_recuperacion_clave_rut-obligatorio.png)

Cuando aparezca este mensaje en el dispositivo del socio, se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/ingreso/sitio-privado',
            'eve-cat' : 'Error',
			'eve-lab' : 'El RUT es obligatorio'
		}
	});
</script>
```

##### RUT No válido
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_recuperacion_clave_rut-invalido.png)

Cuando aparezca este mensaje en el dispositivo del socio, se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/ingreso/sitio-privado',
            'eve-cat' : 'Error',
			'eve-lab' : 'El RUT no es válido'
		}
	});
</script>
```

##### Clave debe tener 6 y 8 caracteres
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_recuperacion_clave_6-8caracteres.png)

Cuando aparezca este mensaje en el dispositivo del socio, se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/ingreso/sitio-privado',
            'eve-cat' : 'Error',
			'eve-lab' : 'Clave debe tener entre 6 y 8 caracteres'
		}
	});
</script>
```

##### Necesitas crear una clave
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_recuperacion_clave_error-crear-una-clave.png)

Cuando aparezca este mensaje en el dispositivo del socio, se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/ingreso/sitio-privado',
            'eve-cat' : 'Error',
			'eve-lab' : 'Debes crear una nueva clave'
		}
	});
</script>
```

##### Clave Bloqueada
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_recuperacion_clave_clave-bloqueada.png)

Cuando aparezca este mensaje en el dispositivo del socio, se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/ingreso/sitio-privado',
            'eve-cat' : 'Error',
			'eve-lab' : 'Clave Bloqueada'
		}
	});
</script>
```

##### Datos incorrectos. Inténtalo nuevamente
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_recuperacion_clave_datos-incorrectos.png)

Cuando aparezca este mensaje en el dispositivo del socio, se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/ingreso/sitio-privado',
            'eve-cat' : 'Error',
			'eve-lab' : 'Datos ingresados son incorrectos'
		}
	});
</script>
```

##### RUT no pertenece a un registro asociado
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_recuperacion_clave_rut-no-esta-registrado.png)



Cuando aparezca este mensaje en el dispositivo del socio, se debe ejecutar el siguiente script:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/ingreso/sitio-privado',
            'eve-cat' : 'Error',
			'eve-lab' : 'Datos ingresados son incorrectos'
		}
	});
</script>
```



##### Sesión ha expirado por inactividad



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_recuperacion_clave_sesion-expirada.png)



Cuando aparezca este mensaje en el dispositivo del socio, se debe ejecutar el siguiente script:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/ingreso/sitio-privado',
            'eve-cat' : 'Error',
			'eve-lab' : 'Sesión expirada por inactividad'
		}
	});
</script>
```



##### Nuestro servicio no se encuentra disponible



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_recuperacion_clave_servicio-no-disponible.png)



Cuando aparezca este mensaje en el dispositivo del socio, se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/ingreso/sitio-privado',
            'eve-cat' : 'Error',
			'eve-lab' : 'Servicio no disponible'
		}
	});
</script>
```



#### Paso 1 - Ingreso de RUT



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-1.png)



Cuando el socio ingrese en esta página, se debe ejecutar el siguiente script:



```html
<script>
dataLayer.push({
	'event': 'vpv',
	'page' : '/creacion-clave/paso1'
});
</script>
```


##### Errores en Paso 1 - Ingreso de RUT



###### Número de serie no corresponde



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-1_numero-de-serie-no-corresponde.png)



Cuando aparezca este mensaje en el dispositivo del socio, se debe ejecutar el siguiente script:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/recuperar-clave/paso1',
            'eve-cat' : 'Error',
			'eve-lab' : 'Número de serie no corresponde'
		}
	});
</script>
```


###### Tooltip de número de serie



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-1_tooltip-no-serie.png)



Cuando el socio haga click en el tooltip, al lado de "**Número de serie**", se debe ejecutar el siguiente script:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/recuperar-clave/paso1',
            'eve-cat' : 'Error',
			'eve-lab' : 'Número de serie no corresponde'
		}
	});
</script>
```



#### Paso 2 [PassCoopeuch] - Método para recuperar clave

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-2_metodo-recuperacion-clave.png)


Cuando el socio ingrese en esta página, se debe ejecutar el siguiente script:


```html
<script>
dataLayer.push({
	'event': 'vpv',
	'page' : '/recuperar-clave/paso2'
});
</script>
```



##### Método: PassCoopeuch



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-2_metodo-recuperacion-clave_passcoopeuch.png)



Cuando el socio seleccione el método "**PassCoopeuch**", y luego haga click en el botón "**Continuar**", se debe ejecutar el siguiente script. Donde ``%metodo-recuperacion%`` corresponde al método seleccionado.



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-recuperacion%',
		'event-config': {
			'eve-acc' : '/recuperar-clave/paso2',
            'eve-cat' : 'Click',
			'eve-lab' : 'Continuar'
		}
	});
</script>
```

#### Paso 3 [PassCoopeuch] - Validación

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_recuperar_clave_paso-3_passcoopeuch.png)



Cuando el socio ingrese en esta página, se debe ejecutar el siguiente script:



```html
<script>
dataLayer.push({
	'event': 'vpv',
	'page' : '/recuperar-clave/paso3'
});
</script>
```



Cuando el socio haga click en "**Autorizar con PassCoopeuch**", se debe ejecutar el siguiente script.:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-recuperacion%',
		'event-config': {
			'eve-acc' : '/recuperar-clave/paso3',
            'eve-cat' : 'Click',
			'eve-lab' : 'Autorizar con PassCoopeuch'
		}
	});
</script>
```



##### Errores en Paso 3 [PassCoopeuch] - Validación

######  Autorización desde celular rechazada

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_recuperar_clave_paso-3-error_autorizacion-celular-rechazada.png)



Cuando se muestre este error en el dispositivo del socio, se debe ejecutar el siguiente script. Donde ``%metodo-validacion%`` corresponde al método seleccionado en el paso 2:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-recuperacion%',
		'event-config': {
			'eve-acc' : '/recuperar-clave/paso3',
            'eve-cat' : 'Error',
			'eve-lab' : 'Autorizacion celular rechazada'
		}
	});
</script>
```
######  Operación no ha podido ser realizada

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_recuperar_clave_paso-3-error_operacion-no-puede-realizar.png)



Cuando se muestre este error en el dispositivo del socio, se debe ejecutar el siguiente script. Donde ``%metodo-validacion%`` corresponde al método seleccionado en el paso 2:


```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-recuperacion%',
		'event-config': {
			'eve-acc' : '/recuperar-clave/paso3',
            'eve-cat' : 'Error',
			'eve-lab' : 'Operacion no pudo realizarse'
		}
	});
</script>
```

######  Tiempo agotado

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_recuperar-clave_paso-3-error_tiempo_agotado.png)



Cuando se muestre este error en el dispositivo del socio, se debe ejecutar el siguiente script. Donde ``%metodo-validacion%`` corresponde al método seleccionado en el paso 2:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-recuperacion%',
		'event-config': {
			'eve-acc' : '/recuperar-clave/paso3',
            'eve-cat' : 'Error',
			'eve-lab' : 'Se agoto el tiempo'
		}
	});
</script>
```



######  Operación rechazada

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_recuperar_clave_paso-3-error_operacion-rechazada.png)



Cuando se muestre este error en el dispositivo del socio, se debe ejecutar el siguiente script. Donde ``%metodo-validacion%`` corresponde al método seleccionado en el paso 2:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-recuperacion%',
		'event-config': {
			'eve-acc' : '/recuperar-clave/paso3',
            'eve-cat' : 'Error',
			'eve-lab' : 'Operacion rechazada'
		}
	});
</script>
```

#### Paso 4 [PassCoopeuch] - Creación de Clave

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-4.png)

Cuando el socio ingrese en esta página, se debe ejecutar el siguiente script. Donde ``%metodo-validacion%`` corresponde al método seleccionado en el paso 2:

```html
<script>
dataLayer.push({
	'producto':'%metodo-recuperacion%',
	'event': 'vpv',
	'page' : '/recuperar-clave/paso4'
});
</script>
```



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-4-crear_clave.png)



Cuando el socio haya llenado los campos "**Nueva Clave**" y "**Confirmación**" sin errores, se habilite el botón "**Crear Clave**", y haga click en el mismo, se debe ejecutar el siguiente script. Donde ``%metodo-validacion%`` corresponde al método seleccionado en el paso 2:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-recuperacion%',
		'event-config': {
			'eve-acc' : '/recuperar-clave/paso4',
            'eve-cat' : 'Click',
			'eve-lab' : 'Crear Clave'
		}
	});
</script>
```



##### Clave creada con éxito



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-4-creacion_clave_exitosa.png)



Cuando el socio ingrese en esta página, se debe ejecutar el siguiente script. Donde ``%metodo-validacion%`` corresponde al método seleccionado en el paso 2:



```html
<script>
dataLayer.push({
	'producto':'%metodo-recuperacion%',
	'event': 'vpv',
	'page' : '/recuperar-clave/exito'
});
</script>
```



##### Errores en Paso 4 [PassCoopeuch] - Creación de Clave
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-4-creacion_clave_errores.png)

Cuando cualquiera de los siguientes errores aparezcan en el dispositivo del socio:

- "**Tu clave no coincide**"
- "**Tu clave debe tener 6 a 8 caracteres**"
- "**Tu clave debe ser alfanumérica**"
- "**Tu clave debe tener 1 mayúscula**"
- "**Tu clave no tiene que tener espacios**"
- "**Tu clave tiene que tener 1 número**"
- "**La contraseña debe ser distinta a la anterior**"

se debe ejecutar el siguiente script. Donde ``%metodo-validacion%`` corresponde al método seleccionado en el paso 2 y ``%error-clave%``sería el mensaje arrojado debajo de los campos de textos como error.

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-recuperacion%',
		'event-config': {
			'eve-acc' : '/recuperar-clave/paso4',
            'eve-cat' : 'Error',
			'eve-lab' : '%error-clave%'
		}
	});
</script>
```

#### Paso 2 [Tarjeta de Coordenadas] - Método para recuperar clave

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-2_metodo-recuperacion-clave.png)


Cuando el socio ingrese en esta página, se debe ejecutar el siguiente script:


```html
<script>
dataLayer.push({
	'event': 'vpv',
	'page' : '/recuperar-clave/paso2'
});
</script>
```



##### Método: Tarjeta de Coordenadas



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_recuperar-clave_paso-2_tarjeta-coordenadas.png)



Cuando el socio seleccione el método "**Tarjeta de Coordenadas**", y luego haga click en el botón "**Continuar**", se debe ejecutar el siguiente script. Donde ``%metodo-recuperacion%`` corresponde al método seleccionado.



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-recuperacion%',
		'event-config': {
			'eve-acc' : '/recuperar-clave/paso2',
            'eve-cat' : 'Click',
			'eve-lab' : 'Continuar'
		}
	});
</script>
```

#### Paso 3 [Tarjeta de Coordenadas] - Validación

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_recuperar_clave_paso-3_tarjeta-coordenadas.png)



Cuando el socio ingrese en esta página, se debe ejecutar el siguiente script. Donde ``%metodo-recuperacion%`` corresponde al método seleccionado en el paso 2.



```html
<script>
dataLayer.push({
	'producto': '%metodo-recuperacion%',
    'event': 'vpv',
	'page' : '/recuperar-clave/paso3'
});
</script>
```



Cuando el socio haga click en "**Autorizar**", se debe ejecutar el siguiente script. Donde ``%metodo-recuperacion%`` corresponde al método seleccionado en el paso 2:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-recuperacion%',
		'event-config': {
			'eve-acc' : '/recuperar-clave/paso3',
            'eve-cat' : 'Click',
			'eve-lab' : 'Autorizar con Tarjeta Coordenadas'
		}
	});
</script>
```



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_recuperar_clave_paso-3_tarjeta-coordenadas_autorizar.png)



Cuando el socio haga click en "**Autorizar**", se debe ejecutar el siguiente script. Donde ``%metodo-recuperacion%`` corresponde al método seleccionado en el paso 2:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-recuperacion%',
		'event-config': {
			'eve-acc' : '/recuperar-clave/paso3',
            'eve-cat' : 'Click',
			'eve-lab' : 'Autorizar Coordenadas'
		}
	});
</script>
```

##### Errores en Paso 3 [Tarjeta de Coordenadas] - Validación

######  Tiempo agotado

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-3-error_tiempo_agotado.png)



Cuando se muestre este error en el dispositivo del socio, se debe ejecutar el siguiente script. Donde ``%metodo-recuperacion%`` corresponde al método seleccionado en el paso 2:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-recuperacion%',
		'event-config': {
			'eve-acc' : '/recuperar-clave/paso3',
            'eve-cat' : 'Error',
			'eve-lab' : 'Se agoto el tiempo'
		}
	});
</script>
```



######  Coordenadas no corresponden

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-3-error_coordenadas_erroneas.png)



Cuando se muestre este error en el dispositivo del socio, se debe ejecutar el siguiente script. Donde ``%metodo-recuperacion%`` corresponde al método seleccionado en el paso 2:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-recuperacion%',
		'event-config': {
			'eve-acc' : '/recuperar-clave/paso3',
            'eve-cat' : 'Error',
			'eve-lab' : 'Coordenadas no corresponden'
		}
	});
</script>
```



######  Operación rechazada

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-3-error_operacion-rechazada.png)



Cuando se muestre este error en el dispositivo del socio, se debe ejecutar el siguiente script. Donde ``%metodo-recuperacion%`` corresponde al método seleccionado en el paso 2:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-recuperacion%',
		'event-config': {
			'eve-acc' : '/recuperar-clave/paso3',
            'eve-cat' : 'Error',
			'eve-lab' : 'Operacion rechazada'
		}
	});
</script>
```



######  Tarjeta de Coordenadas bloqueada

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-3-error_tarjeta-bloqueada.png)



Cuando se muestre este error en el dispositivo del socio, se debe ejecutar el siguiente script. Donde ``%metodo-recuperacion%`` corresponde al método seleccionado en el paso 2:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-recuperacion%',
		'event-config': {
			'eve-acc' : '/recuperar-clave/paso3',
            'eve-cat' : 'Error',
			'eve-lab' : 'Tarjeta de Coordenadas bloqueada'
		}
	});
</script>
```



######  Operación no ha podido ser realizada

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-3-error_imposible-realizar-operacion.png)



Cuando se muestre este error en el dispositivo del socio, se debe ejecutar el siguiente script. Donde ``%metodo-recuperacion%`` corresponde al método seleccionado en el paso 2:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-recuperacion%',
		'event-config': {
			'eve-acc' : '/recuperar-clave/paso3',
            'eve-cat' : 'Error',
			'eve-lab' : 'Operacion no ha podido ser realizada'
		}
	});
</script>
```

#### Paso 4 [Tarjeta de Coordenadas] - Creación de Clave

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-4.png)

Cuando el socio ingrese en esta página, se debe ejecutar el siguiente script. Donde ``%metodo-recuperacion%`` corresponde al método seleccionado en el paso 2:

```html
<script>
dataLayer.push({
	'producto':'%metodo-validacion%',
	'event': 'vpv',
	'page' : '/recuperar-clave/paso4'
});
</script>
```



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-4-crear_clave.png)



Cuando el socio haya llenado los campos "**Nueva Clave**" y "**Confirmación**" sin errores, se habilite el botón "**Crear Clave**", y haga click en el mismo, se debe ejecutar el siguiente script. Donde ``%metodo-recuperacion%`` corresponde al método seleccionado en el paso 2:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-recuperacion%',
		'event-config': {
			'eve-acc' : '/recuperar-clave/paso4',
            'eve-cat' : 'Click',
			'eve-lab' : 'Crear Clave'
		}
	});
</script>
```



##### Clave creada con éxito



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-4-creacion_clave_exitosa.png)



Cuando el socio ingrese en esta página, se debe ejecutar el siguiente script. Donde ``%metodo-recuperacion%`` corresponde al método seleccionado en el paso 2:



```html
<script>
dataLayer.push({
	'producto':'%metodo-recuperacion%',
	'event': 'vpv',
	'page' : '/recuperar-clave/exito'
});
</script>
```



##### Errores en Paso 4 [Tarjeta de Coordenadas] - Creación de Clave
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-4-creacion_clave_errores.png)

Cuando cualquiera de los siguientes errores aparezcan en el dispositivo del socio:

- "**Tu clave no coincide**"
- "**Tu clave debe tener 6 a 8 caracteres**"
- "**Tu clave debe ser alfanumérica**"
- "**Tu clave debe tener 1 mayúscula**"
- "**Tu clave no tiene que tener espacios**"
- "**Tu clave tiene que tener 1 número**"
- "**La contraseña debe ser distinta a la anterior**"

se debe ejecutar el siguiente script. Donde ``%metodo-recuperacion%`` corresponde al método seleccionado en el paso 2 y ``%error-clave%``sería el mensaje arrojado debajo de los campos de textos como error.

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-recuperacion%',
		'event-config': {
			'eve-acc' : '/recuperar-clave/paso4',
            'eve-cat' : 'Error',
			'eve-lab' : '%error-clave%'
		}
	});
</script>
```



#### Paso 2 [Clave SMS] - Método para recuperar clave

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-2_metodo-recuperacion-clave.png)


Cuando el socio ingrese en esta página, se debe ejecutar el siguiente script:


```html
<script>
dataLayer.push({
	'event': 'vpv',
	'page' : '/recuperar-clave/paso2'
});
</script>
```



##### Método: SMS



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_recuerar-clave_paso-2_sms.png)



Cuando el socio seleccione el método "**SMS**", y luego haga click en el botón "**Continuar**", se debe ejecutar el siguiente script. Donde ``%metodo-recuperacion%`` corresponde al método seleccionado.



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-recuperacion%',
		'event-config': {
			'eve-acc' : '/recuperar-clave/paso2',
            'eve-cat' : 'Click',
			'eve-lab' : 'Continuar'
		}
	});
</script>
```

##### Errores en Método: Clave SMS

###### Sin teléfono asociado

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_recuerar-clave_paso-2_sms_error-telefono-no-asociado.png)

Cuando aparezca este mensaje en el dispositivo del socio, se debe ejecutar el siguiente script. Donde ``%metodo-recuperacion%`` corresponde al método seleccionado en el paso 2.

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-validacion%',
		'event-config': {
			'eve-acc' : '/recuperar-clave/paso2',
            'eve-cat' : 'Error',
			'eve-lab' : 'Sin telefono asociado'
		}
	});
</script>
```

###### Número bloqueado

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_recuperar-clave_paso-2_sms_error-telefono-bloqueado.png)

Cuando aparezca este mensaje en el dispositivo del socio, se debe ejecutar el siguiente script. Donde ``%metodo-recuperacion%`` corresponde al método seleccionado en el paso 2.

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-recuperacion%',
		'event-config': {
			'eve-acc' : '/recuperar-clave/paso2',
            'eve-cat' : 'Error',
			'eve-lab' : 'Numero de telefono bloqueado'
		}
	});
</script>
```


#### Paso 3 [Clave SMS] - Validación

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-3_sms.png)


Cuando el socio ingrese en esta página, se debe ejecutar el siguiente script. Donde ``%metodo-recuperacion%`` corresponde al método seleccionado en el paso 2:


```html
<script>
dataLayer.push({
	'producto':'%metodo-recuperacion%',
	'event': 'vpv',
	'page' : '/recuperacion-clave/paso3'
});
</script>
```


![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-3-sms_click_autorizar.png)



Cuando el socio haga click en el botón "**Validar**", se debe ejecutar el siguiente script. Donde ``%metodo-recuperacion%`` corresponde al método seleccionado en el paso 2.

_Nota: en el inVision no está la vista del flujo donde el botón de "**Validar**". Supuse que podría ser pinchado una vez el usuario ingresara el código de seguridad válido._



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-recuperacion%',
		'event-config': {
			'eve-acc' : '/recuperacion-clave/paso3',
            'eve-cat' : 'Click',
			'eve-lab' : 'Validar con codigo sms'
		}
	});
</script>
```



Si el socio hace click en "**Aún no recibo el código, enviar nuevamente**", se debe ejecutar el siguiente script. Donde ``%metodo-recuperacion%`` corresponde al método seleccionado en el paso 2.



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-recuperacion%',,
		'event-config': {
			'eve-acc' : '/recuperacion-clave/paso3',
            'eve-cat' : 'Click',
			'eve-lab' : 'Enviar codigo nuevamente'
		}
	});
</script>
```



##### Errores en Paso 3 [Clave SMS] - Validación

######  Código no válido

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-3-sms_codigo-no-valido.png)



Cuando se muestre este error en el dispositivo del socio, se debe ejecutar el siguiente script. Donde ``%metodo-recuperacion%`` corresponde al método seleccionado en el paso 2:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-recuperacion%',
		'event-config': {
			'eve-acc' : '/recuperacion-clave/paso3',
            'eve-cat' : 'Error',
			'eve-lab' : 'Codigo no valido'
		}
	});
</script>
```



######  Código caducado

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-3-sms_codigo-caducado.png)



Cuando se muestre este error en el dispositivo del socio, se debe ejecutar el siguiente script. Donde ``%metodo-recuperacion%`` corresponde al método seleccionado en el paso 2:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-recuperacion%',
		'event-config': {
			'eve-acc' : '/recuperacion-clave/paso3',
            'eve-cat' : 'Error',
			'eve-lab' : 'Codigo caducado'
		}
	});
</script>
```



###### Reintentar

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-3-sms_reintentar.png)

Cuando el socio haga click en "**Reintentar**", se debe ejecutar el siguiente script. Donde ``%metodo-recuperacion%`` corresponde al método seleccionado en el paso 2:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-recuperacion%',
		'event-config': {
			'eve-acc' : '/creacion-clave/paso3',
            'eve-cat' : 'Click',
			'eve-lab' : 'Reintentar'
		}
	});
</script>
```

###### Código SMS bloqueado

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-3-sms_codigo-sms-bloqueado.png)

Cuando se muestre este error en el dispositivo del socio, se debe ejecutar el siguiente script. Donde ``%metodo-recuperacion%`` corresponde al método seleccionado en el paso 2:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-recuperacion%',
		'event-config': {
			'eve-acc' : '/creacion-clave/paso3',
            'eve-cat' : 'Error',
			'eve-lab' : 'Codigo sms bloqueado'
		}
	});
</script>
```


#### Paso 4 [Clave SMS] - Creación de Clave



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-4.png)



Cuando el socio ingrese en esta página, se debe ejecutar el siguiente script. Donde ``%metodo-recuperacion%`` corresponde al método seleccionado en el paso 2:



```html
<script>
dataLayer.push({
	'producto':'%metodo-validacion%',
	'event': 'vpv',
	'page' : '/recuperar-clave/paso4'
});
</script>
```



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-4-crear_clave.png)



Cuando el socio haya llenado los campos "**Nueva Clave**" y "**Confirmación**" sin errores, se habilite el botón "**Crear Clave**", y haga click en el mismo, se debe ejecutar el siguiente script. Donde ``%metodo-recuperacion%`` corresponde al método seleccionado en el paso 2:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-recuperacion%',
		'event-config': {
			'eve-acc' : '/recuperar-clave/paso4',
            'eve-cat' : 'Click',
			'eve-lab' : 'Crear Clave'
		}
	});
</script>
```



##### Clave creada con éxito



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-4-creacion_clave_exitosa.png)



Cuando el socio ingrese en esta página, se debe ejecutar el siguiente script. Donde ``%metodo-recuperacion%`` corresponde al método seleccionado en el paso 2:



```html
<script>
dataLayer.push({
	'producto':'%metodo-recuperacion%',
	'event': 'vpv',
	'page' : '/recuperar-clave/exito'
});
</script>
```



##### Errores en Paso 4 - Creación de Clave
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\flujo creación y recuperación de clave\img\desktop_creacion_clave_paso-4-creacion_clave_errores.png)

Cuando cualquiera de los siguientes errores aparezcan en el dispositivo del socio:

- "**Tu clave no coincide**"
- "**Tu clave debe tener 6 a 8 caracteres**"
- "**Tu clave debe ser alfanumérica**"
- "**Tu clave debe tener 1 mayúscula**"
- "**Tu clave no tiene que tener espacios**"
- "**Tu clave tiene que tener 1 número**"
- "**La contraseña debe ser distinta a la anterior**"

se debe ejecutar el siguiente script. Donde ``%metodo-recuperacion%`` corresponde al método seleccionado en el paso 2 y ``%error-clave%``sería el mensaje arrojado debajo de los campos de textos como error.

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto': '%metodo-recuperacion%',
		'event-config': {
			'eve-acc' : '/recuperar-clave/paso4',
            'eve-cat' : 'Error',
			'eve-lab' : '%error-clave%'
		}
	});
</script>
```