<img align="left" width="19%" height="19%" src="https://gitlab.com/d2b_chile/formato-documento-markdown/-/raw/master/Imagenes/D2B-GRIS-POSITIVO-CON BAJADA-01.png">

# **Documento de Implementación** del Flujo de Cuenta de Ahorro (Google Analytics)

Mayo 2021





## Sobre el Documento

El siguiente documento describe las variables a ser utilizadas en cada marca, se debe copiar el script tal cual como viene indicado en cada paso. Cuando aparezca un  **`%variable%`**, corresponde a una variable que se debe reemplazar según el dato que corresponda de acuerdo al paso o sección en que se encuentre.

______

































## Desktop Scripts

### Cuenta Ahorro

#### Solicitud Cuenta de Ahorro

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Marcas Cuenta de Ahorro\img\cta_ahorro_landing.png)



Cuando el socio ingrese en la página de **Cuenta de Ahorro**, se debe ejecutar el siguiente script. 



```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/cuenta-ahorro/solicitud',
});
</script>
```


Además, si el socio hace click en el botón "**Solicita tu Cuenta**", se debe ejecutar el siguiente script.

```html
<script>
	dataLayer.push({
		'event': 'eve',
			'eve-acc' : '/tef/cuenta-ahorro/solicitud',
			'eve-cat' : 'Click',
			'eve-lab' : 'Solicita tu cuenta'
	});
</script>
```



#### Mis Ahorros

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Marcas Cuenta de Ahorro\img\cta_ahorro_mis-ahorros.png)


Cuando el socio  entre en "**Mis Ahorros**" se debe ejecutar el siguiente script, que es la primera página que aparece al hacer click en _Mis Productos > Cuenta de Ahorro_. Donde ``%monto-ahorrado%`` sería el monto total mostrado en la pantalla del socio (recuadro amarillo).

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/cuenta-ahorro/mis-ahorros',
	'monto' : '%monto-ahorrado%',
});
</script>
```

**OPCIONAL**: Se debe correr el siguiente script, para medir el mensaje que avisa al socio que no tiene saldo en su cuenta (recuadro azul):

```html
<script>
	dataLayer.push({
		'event': 'eve',
			'eve-acc' : '/tef/cuenta-ahorro/mis-ahorros',
			'eve-cat' : 'Aviso',
			'eve-lab' : 'Sin saldo en la cuenta'
	});
</script>
```

#### Movimientos

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Marcas Cuenta de Ahorro\img\cta_ahorro_movimientos.png)

Cuando el socio ingrese en la sección **Movimientos**, se debe ejecutar el siguiente script.

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/cuenta-ahorro/movimientos',
});
</script>
```

#### Errores

##### Sin Servicio

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Marcas Cuenta de Ahorro\img\cta_ahorro_movimientos-sin_servicio.png)

Cuando se despliegue este modal de error, indicando que el servicio no se encuentra disponible, se debe ejecutar el siguiente script. Donde ``%ubicacion%`` puede ser ``/tef/cuenta-ahorro/movimientos`` o ``/tef/cuenta-ahorro/mis-ahorros``, dependiendo de dónde aparezca. Si aparece en la sección "**Movimientos**", el primer _path_; si aparece en "**Mis Ahorros**", el segundo.

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '%ubicacion%/sin-servicio',
});
</script>
```