<img align="left" width="19%" height="19%" src="https://gitlab.com/d2b_chile/formato-documento-markdown/-/raw/master/Imagenes/D2B-GRIS-POSITIVO-CON BAJADA-01.png">

# **Documento de Implementación** del Flujo de Cuenta de Ahorro (Google Analytics)

Marzo 2021





## Sobre el Documento

El siguiente documento describe las variables a ser utilizadas en cada marca, se debe copiar el script tal cual como viene indicado en cada paso. Cuando aparezca un  **`%variable%`**, corresponde a una variable que se debe reemplazar según el dato que corresponda de acuerdo al paso o sección en que se encuentre.

______

































## Desktop Scripts

### Login - Sitio Público

#### Landing

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Marcas Cuenta de Ahorro\img\login-sp_landing.png)

Cuando el socio ingrese en el landing de login, se debe ejecutar el siguiente script:

```html
<script>
dataLayer.push({
	'event': 'vpv',
	'page' : '/home/'
});
</script>
```


#### Botón Ingresar



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Marcas Cuenta de Ahorro\img\login-sp_btn-ingresar.png)



Cuando el socio haga click en "**Ingresar**" en el landing, se debe ejecutar el siguiente script:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/home/login',
            'eve-cat' : 'Click',
			'eve-lab' : 'Ingresar'
		}
	});
</script>
```



##### Errores en Modal de Ingreso



###### RUT no es válido



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Marcas Cuenta de Ahorro\img\login-sp_btn-ingresar_rut-no-valido.png)

Cuando aparezca este mensaje en el dispositivo del socio, se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/home/login',
            'eve-cat' : 'Error',
			'eve-lab' : 'El RUT no es válido'
		}
	});
</script>
```


###### Clave debe tener 6 y 8 caracteres
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Marcas Cuenta de Ahorro\img\login-sp_btn-ingresar_clave-6-8-chr.png)

Cuando aparezca este mensaje en el dispositivo del socio, se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/home/login',
            'eve-cat' : 'Error',
			'eve-lab' : 'Clave debe tener entre 6 y 8 caracteres'
		}
	});
</script>
```



### Login - Sitio Privado

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Marcas Cuenta de Ahorro\img\login-spriv.png)

Cuando el socio ingrese a esta página, se debe ejecutar el siguiente script.

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/login'
});
</script>
```
#### Errores en Login - Sitio Privado

##### RUT Obligatorio

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Marcas Cuenta de Ahorro\img\login-spriv_rut-obligatorio.png)

Cuando aparezca este mensaje en el dispositivo del socio, se debe ejecutar el siguiente script.

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/login',
            'eve-cat' : 'Error',
			'eve-lab' : 'El RUT es obligatorio'
		}
	});
</script>
```

##### RUT No es válido

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Marcas Cuenta de Ahorro\img\login-spriv_rut-no_valido.png)

Cuando aparezca este mensaje en el dispositivo del socio, se debe ejecutar el siguiente script.

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/login',
            'eve-cat' : 'Error',
			'eve-lab' : 'El RUT no es valido'
		}
	});
</script>
```

##### Clave debe tener entre 6 y 8 caracteres

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Marcas Cuenta de Ahorro\img\login-spriv_clave-6-8-chr.png)

Cuando aparezca este mensaje en el dispositivo del socio, se debe ejecutar el siguiente script.

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/login',
            'eve-cat' : 'Error',
			'eve-lab' : 'Clave debe tener entre 6 y 8 caracteres'
		}
	});
</script>
```

##### Actualiza tu clave

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Marcas Cuenta de Ahorro\img\login-spriv_actualiza-clave.png)

Cuando aparezca este mensaje en el dispositivo del socio, se debe ejecutar el siguiente script.

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/login',
            'eve-cat' : 'Error',
			'eve-lab' : 'Actualiza tu clave'
		}
	});
</script>
```

##### Crear una clave

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Marcas Cuenta de Ahorro\img\login-spriv_crea-clave.png)

Cuando aparezca este mensaje en el dispositivo del socio, se debe ejecutar el siguiente script.

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/login',
            'eve-cat' : 'Error',
			'eve-lab' : 'Crea una clave'
		}
	});
</script>
```

##### Clave bloqueada

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Marcas Cuenta de Ahorro\img\login-spriv_clave-bloqueada.png)

Cuando aparezca este mensaje en el dispositivo del socio, se debe ejecutar el siguiente script.

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/login',
            'eve-cat' : 'Error',
			'eve-lab' : 'Clave bloqueada'
		}
	});
</script>
```

##### Datos incorrectos

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Marcas Cuenta de Ahorro\img\login-spriv_datos-incorrectos.png)

Cuando aparezca este mensaje en el dispositivo del socio, se debe ejecutar el siguiente script.

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/login',
            'eve-cat' : 'Error',
			'eve-lab' : 'Datos ingresados incorrectos'
		}
	});
</script>
```

##### RUT ingresado no pertenece a un registro asociado a COOPEUCH

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Marcas Cuenta de Ahorro\img\login-spriv_rut-ingresado-no-pertenece.png)

Cuando aparezca este mensaje en el dispositivo del socio, se debe ejecutar el siguiente script.

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/login',
            'eve-cat' : 'Error',
			'eve-lab' : 'RUT ingresado no pertenece a registro en coopeuch'
		}
	});
</script>
```

##### Su sesión ha expirado

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Marcas Cuenta de Ahorro\img\login-spriv_sesion-expirada.png)

Cuando aparezca este mensaje en el dispositivo del socio, se debe ejecutar el siguiente script.

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/login',
            'eve-cat' : 'Error',
			'eve-lab' : 'Tu sesion ha expirado'
		}
	});
</script>
```

##### Tu sesión ha finalizado por seguridad

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Marcas Cuenta de Ahorro\img\login-spriv_sesion-finalizada-seguridad.png)

Cuando aparezca este mensaje en el dispositivo del socio, se debe ejecutar el siguiente script.

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/login',
            'eve-cat' : 'Error',
			'eve-lab' : 'Sesion finalizo por seguridad'
		}
	});
</script>
```

##### Servicio no disponible

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Marcas Cuenta de Ahorro\img\login-spriv_servicio-no-disponible.png)

Cuando aparezca este mensaje en el dispositivo del socio, se debe ejecutar el siguiente script.

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/login',
            'eve-cat' : 'Error',
			'eve-lab' : 'Servicio no se encuentra disponible'
		}
	});
</script>
```

### Dashboard

#### Primera visita - Bienvenido

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Marcas Cuenta de Ahorro\img\dashboard_1visita-bienvenido.png)

Cuando el socio entre a esta página, se debe ejecutar el siguiente script. Donde ``%aportado%`` hace referencia al monto ahorrado por el socio hasta la fecha.

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/bienvenido',
    'monto' : '%aportado%'
});
</script>
```
#### Último Acceso

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Marcas Cuenta de Ahorro\img\dashboard_ultimo-acceso.png)

Cuando el socio entre a esta página, se debe ejecutar el siguiente script. Donde ``%aportado%`` hace referencia al monto ahorrado por el socio hasta la fecha.

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/ultimo-acceso',
    'monto' : '%aportado%'
});
</script>
```

#### Sin Productos

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Marcas Cuenta de Ahorro\img\dashboard_sin-productos.png)

Cuando el socio entre a esta página, se debe ejecutar el siguiente script. Donde ``%aportado%`` hace referencia al monto ahorrado por el socio hasta la fecha.

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/sin-productos',
    'monto' : '%aportado%'
});
</script>
```

#### Conoce lo que tenemos - Desplegado

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Marcas Cuenta de Ahorro\img\dashboard_conoce-lo-que-tenemos-desplegado.png)

Cuando el socio despliegue el _dropdown_ que dice **Conoce lo que tenemos hoy para ti**, debe ejecutarse el siguiente script. Donde ``%ubicacion%`` hace referencia a la ruta donde se ejecutó el evento.

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/%ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Conoce lo que tenemos hoy para ti'
		}
	});
</script>
```

#### Solo Remanente

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Marcas Cuenta de Ahorro\img\dashboard_solo-remanente.png)

Cuando el socio entre a esta página, se debe ejecutar el siguiente script.

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/remanente'
});
</script>
```

#### Remanente y Cuenta Vista Coopeuch

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Marcas Cuenta de Ahorro\img\dashboard_cuenta-vista_remanente.png)

Cuando el socio entre a esta página, se debe ejecutar el siguiente script.

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/remanente_cuenta-vista'
});
</script>
```

#### Avisos

##### Cierre por inactividad

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Marcas Cuenta de Ahorro\img\aviso_cierre-inactividad.png)

Cuando aparezca este modal de aviso, se debe ejecutar el siguiente script. Donde ``%ubicacion%`` hace referencia a la ruta donde aparecería el aviso.

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/%ubicacion%',
            'eve-cat' : 'Aviso',
			'eve-lab' : 'Aviso de cierre por inactividad'
		}
	});
</script>
```

### Solicita - Cuota Participación

#### Inicio - Cuando es socio

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Marcas Cuenta de Ahorro\img\cuota_participacion.png)

Cuando el cliente ingrese a esta página, se debe ejecutar el siguiente script. Donde ``%aportado%`` hace referencia al monto ahorrado por el socio hasta la fecha.

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/solicitar/cuota-participacion',
	'monto' : '%aportado%'
});
</script>
```
#### Botón de cuotas

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Marcas Cuenta de Ahorro\img\cuota_participacion-botones.png)

En la página de Cuota Participación, pueden aparecer tres opciones de botones debajo del Total Aportado hasta la Fecha por el Socio: "**Pagar Cuotas**", "**Ir a pago online**", o "**Ver detalle**". Cuando el socio hace click en cualquiera de estos botones, se debe ejecutar el siguiente script. Donde ``%texto-de-boton%`` hace referencia al texto que está en el botón al que haga click el socio (los mencionados anteriormente).

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/solicitar/cuota-participacion',
            'eve-cat' : 'Click',
			'eve-lab' : '%texto-de-boton%'
		}
	});
</script>
```



#### Inicio - Cuando no es socio

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Marcas Cuenta de Ahorro\img\cuota_participacion.png)

Cuando el cliente ingrese a esta página, se debe ejecutar el siguiente script.

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/solicitar/cuota-participacion/hazte-socio',
});
</script>
```

#### Botón "Hazte Socio"

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Marcas Cuenta de Ahorro\img\cuota_participacion-boton-hazte_socio.png)

Cuando el socio haga click en el botón "**Hazte Socio**" se debe ejecutar el siguiente script.

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/solicitar/cuota-participacion/hazte-socio',
            'eve-cat' : 'Click',
			'eve-lab' : 'Hazte Socio'
		}
	});
</script>
```

### Mis Productos - Cuota de Participación

#### Inicio
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Marcas Cuenta de Ahorro\img\mis-productos_cuota-de-participacion.png)

Cuando el socio ingrese a esta vista, debe ejecutarse el siguiente script. Donde ``%aportado%`` hace referencia al monto ahorrado por el socio hasta la fecha. Además, ``%pendiente/pagado%`` corresponde al estado del monto pendiente (información del recuadro azul); si hay monto pendiente, el valor debe ser **pendiente**, si se está al día con el pago (cuando se realice el pago, o aparezca el mensaje "**¡Muy bien! Estás al día en el pago de tus cuotas**"), el valor es **pagado**. Por último,  ``%mensaje%`` corresponde al texto indicado en el mensaje superior (recuadro rojo).

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/mis-productos/cuota-participacion',
    'producto' : '%pendiente/pagado%',
    'valoracion' : '%mensaje%'
	'monto' : '%aportado%'
});
</script>
```


#### Botón "Pagar Cuota"

Cuando el socio haga click en el botón "**Pagar Cuota**" (recuadro amarillo) se debe ejecutar el siguiente script.

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/mis-productos/cuota-participacion',
            'eve-cat' : 'Click',
			'eve-lab' : 'Pagar Cuota'
		}
	});
</script>
```



### Pago de cuota de participación

#### (Paso 1) Mis Productos - Cuota de Participación - Monto a Pagar
![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Priv/Cuota-participacion-nuevo-privado/flujo_participacion-pago_cuota_participacion.png)

Cuando el socio ingrese a la sección de "**Cuota de Participación**", a través del desplegable "**Mis Productos**", para Pagar la Cuota de Participación,  se debe ejecutar el siguiente script.

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/pago-cuota-participacion/paso-1',
});
</script>
```
![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Priv/Cuota-participacion-nuevo-privado/flujo_participacion-pago_cuota_participacion-continuar.png)

Cuando el socio haga click en el botón "**Continuar**"", se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'eve',
			'eve-acc' : '/tef/pago-cuota-participacion/paso-1',
			'eve-cat' : 'Click',
			'eve-lab' : 'Continuar'
	});
</script>
```

Si el socio hace click en "**Volver**", se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'eve',
			'eve-acc' : '/tef/pago-cuota-participacion/paso-1',
			'eve-cat' : 'Click',
			'eve-lab' : 'Volver'
	});
</script>
```
#### (Paso 2) Mis Productos - Cuota de Participación - Pago

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Marcas Cuenta de Ahorro\img\pago_cuota_participacion-paso2.png)

Cuando el socio ingrese a esta página,  se debe ejecutar el siguiente script. Donde ```%monto-a-pagar%``` hace referencia al **Monto a Pagar** reflejado en el "**Resumen de Pago**"  y ``%cuotas%`` hace referencia al número de cuotas a pagar del socio.

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/pago-cuota-participacion/paso-2',
	'monto' : '%monto-a-pagar%',
    'cuotas' : '%cuotas%'	
});
</script>
```

![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Priv/Cuota-participacion-nuevo-privado/flujo_participacion-pago_cuota_participacion-paso2-pagar.png)

Cuando el socio haga click en el botón "**Pagar**", se debe ejecutar el siguiente script. Donde ``%monto-a-pagar%`` hace referencia al Monto a Pagar reflejado en el "Resumen de Pago", y %cuotas% hacereferencia al número de cuotas a pagar del socio (vistas en la página anterior)

```html
<script>
	dataLayer.push({
		'event': 'eve',
			'eve-acc' : '/tef/pago-cuota-participacion/paso-2',
			'eve-cat' : 'Click',
			'eve-lab' : 'Pagar',
			'monto' : '%monto-a-pagar%',
			'cuotas' : '%cuotas%'
	});
</script>
```
Si el socio hace click en "**Volver**", se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'eve',
			'eve-acc' : '/tef/pago-cuota-participacion/paso-2',
			'eve-cat' : 'Click',
			'eve-lab' : 'Volver'
	});
</script>
```

#### (Paso 3) Mis Productos - Cuota de Participación - Comprobante

![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Priv/Cuota-participacion-nuevo-privado/flujo_participacion-pago_cuota_participacion-paso3.png)

Cuando el socio ingrese a esta página,  se debe ejecutar el siguiente script. Donde ```%monto-a-pagar%``` hace referencia al **Monto a Pagar** reflejado en el "**Resumen de Pago**", y ```%cuotas%``` hace referencia al número de cuotas a pagar del socio (vistas en la página anterior)

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/pago-cuota-participacion/paso-3',
	'monto' : '%monto-a-pagar%',
	'cuotas' : '%cuotas%'
	
});
</script>
```

![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Priv/Cuota-participacion-nuevo-privado/flujo_participacion-pago_cuota_participacion-paso3-ir_cuota_participacion.png)

Cuando el socio haga click en el botón "**Ir a Cuota de Participación**", se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'eve',
			'eve-acc' : '/tef/pago-cuota-participacion/paso-3',
			'eve-cat' : 'Click',
			'eve-lab' : 'Ir a Cuota de Partipación'
	});
</script>
```
Si el socio hace click e "**Volver**", se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'eve',
			'eve-acc' : '/tef/pago-cuota-participacion/paso-3',
			'eve-cat' : 'Click',
			'eve-lab' : 'Volver'
	});
</script>
```



##### Mensajes de error



###### En Paso 1

![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Priv/Cuota-participacion-nuevo-privado/flujo_partipacion-error-paso1.png)

Cuando el socio ingrese un máximo de cuotas superior al de "**cuotas pendientes**", se debe ejecutar el siguiente script. Donde **``%maximo de cuotas%``** es el texto completo del mensaje de error.

```html
<script>
	dataLayer.push({
		'event': 'eve',
			'eve-acc' : '/tef/pago-cuota-participacion/paso-1',
			'eve-cat' : 'Error',
			'eve-lab' : '%maximo de cuotas%'
	});
</script>
```



###### En Paso 2



![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Priv/Cuota-participacion-nuevo-privado/flujo_partipacion-error-paso2_a.png)

Cuando aparezca este error en el dispositivo del socio, se debe ejecutar el siguiente script. 


```html
<script>
	dataLayer.push({
		'event': 'eve',
			'eve-acc' : '/tef/pago-cuota-participacion/paso-2',
			'eve-cat' : 'Error',
			'eve-lab' : 'No tienes saldo suficiente'
	});
</script>
```



![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Priv/Cuota-participacion-nuevo-privado/flujo_partipacion-error-paso2_d.png)

Cuando aparezca este error en el dispositivo del socio, se debe ejecutar el siguiente script:


```html
<script>
	dataLayer.push({
		'event': 'eve',
			'eve-acc' : '/tef/pago-cuota-participacion/paso-2',
			'eve-cat' : 'Error',
			'eve-lab' : 'Contraseña bloqueada'
	});
</script>
```

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Marcas Cuenta de Ahorro\img\pcp-error-clave_incorrecta.png)

Cuando aparezca este error en el dispositivo del socio, se debe ejecutar el siguiente script:


```html
<script>
	dataLayer.push({
		'event': 'eve',
			'eve-acc' : '/tef/pago-cuota-participacion/paso-2',
			'eve-cat' : 'Error',
			'eve-lab' : 'Contraseña incorrecta'
	});
</script>
```

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Marcas Cuenta de Ahorro\img\pcp-error-clave_incorrecta_tercer_intento.png)

Cuando aparezca este error en el dispositivo del socio, se debe ejecutar el siguiente script:


```html
<script>
	dataLayer.push({
		'event': 'eve',
			'eve-acc' : '/tef/pago-cuota-participacion/paso-2',
			'eve-cat' : 'Error',
			'eve-lab' : 'Contraseña incorrecta casi bloqueada'
	});
</script>
```



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Marcas Cuenta de Ahorro\img\pcp-error-paso2_clave-caracteres.png)

Cuando aparezca este error en el dispositivo del socio, se debe ejecutar el siguiente script:


```html
<script>
	dataLayer.push({
		'event': 'eve',
			'eve-acc' : '/tef/pago-cuota-participacion/paso-2',
			'eve-cat' : 'Error',
			'eve-lab' : 'La clave debe tener 6 u 8 caracteres'
	});
</script>
```

###### Sin Servicio

![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Priv/Cuota-participacion-nuevo-privado/flujo_partipacion-error-servicio_no_disponible.png)

Cuando aparezca este error en el dispositivo del socio, se debe ejecutar el siguiente script. Donde ``%ubicacion%`` hace referencia a la URL donde se encuentre el socio.


```html
<script>
	dataLayer.push({
		'event': 'eve',
			'eve-acc' : '/tef/%ubicacion%',
			'eve-cat' : 'Error',
			'eve-lab' : 'Sin Servicio'
	});
</script>
```

###### Sin Servicio

![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Priv/Cuota-participacion-nuevo-privado/flujo_partipacion-error-servicio_no_disponible.png)

Cuando aparezca este error en el dispositivo del socio, se debe ejecutar el siguiente script. Donde ``%ubicacion%`` hace referencia a la URL donde se encuentre el socio.


```html
<script>
	dataLayer.push({
		'event': 'eve',
			'eve-acc' : '/tef/%ubicacion%',
			'eve-cat' : 'Error',
			'eve-lab' : 'Sin Servicio'
	});
</script>
```

###### No se pudo realizar el pago de tus Cuotas de Participación

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Marcas Cuenta de Ahorro\img\pcp-error-no_se_pudo_pagar.png)

Cuando aparezca este error en el dispositivo del socio, se debe ejecutar el siguiente script. Donde ``%ubicacion%`` hace referencia a la URL donde se encuentre el socio.


```html
<script>
	dataLayer.push({
		'event': 'eve',
			'eve-acc' : '/tef/%ubicacion%',
			'eve-cat' : 'Error',
			'eve-lab' : 'Sin Servicio'
	});
</script>
```
### Cuenta Ahorro

#### Solicitud Cuenta de Ahorro

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Marcas Cuenta de Ahorro\img\cta_ahorro_landing.png)



Cuando el socio ingrese en la página de **Cuenta de Ahorro**, se debe ejecutar el siguiente script. 



```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/cuenta-ahorro/solicitud',
});
</script>
```


Además, si el socio hace click en el botón "**Solicita tu Cuenta**", se debe ejecutar el siguiente script.

```html
<script>
	dataLayer.push({
		'event': 'eve',
			'eve-acc' : '/tef/cuenta-ahorro/solicitud',
			'eve-cat' : 'Click',
			'eve-lab' : 'Solicita tu cuenta'
	});
</script>
```



#### Mis Ahorros

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Marcas Cuenta de Ahorro\img\cta_ahorro_mis-ahorros.png)


Cuando el socio  entre en "**Mis Ahorros**" se debe ejecutar el siguiente script, que es la primera página que aparece al hacer click en _Mis Productos > Cuenta de Ahorro_. Donde ``%monto-ahorrado%`` sería el monto total mostrado en la pantalla del socio (recuadro amarillo).

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/cuenta-ahorro/mis-ahorros',
	'monto' : '%monto-ahorrado%',
});
</script>
```

**OPCIONAL**: Se debe correr el siguiente script, para medir el mensaje que avisa al socio que no tiene saldo en su cuenta (recuadro azul):

```html
<script>
	dataLayer.push({
		'event': 'eve',
			'eve-acc' : '/tef/cuenta-ahorro/mis-ahorros',
			'eve-cat' : 'Aviso',
			'eve-lab' : 'Sin saldo en la cuenta'
	});
</script>
```

#### Movimientos

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Marcas Cuenta de Ahorro\img\cta_ahorro_movimientos.png)

Cuando el socio ingrese en la sección **Movimientos**, se debe ejecutar el siguiente script.

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/cuenta-ahorro/movimientos',
});
</script>
```

#### Sin Servicio

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Marcas Cuenta de Ahorro\img\cta_ahorro_movimientos-sin_servicio.png)

Cuando se despliegue este modal de error, indicando que el servicio no se encuentra disponible, se debe ejecutar el siguiente script. Donde ``%ubicacion%`` puede ser ``/tef/cuenta-ahorro/movimientos`` o ``/tef/cuenta-ahorro/mis-ahorros``, dependiendo de dónde aparezca. Si aparece en la sección "**Movimientos**", el primer _path_; si aparece en "**Mis Ahorros**", el segundo.

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '%ubicacion%/sin-servicio',
});
</script>
```