<img align="left" width="19%" height="19%" src="https://gitlab.com/d2b_chile/formato-documento-markdown/-/raw/master/Imagenes/D2B-GRIS-POSITIVO-CON BAJADA-01.png">

# **Documento de Implementación** Enrolamiento Biometría  (Google Analytics)

Marzo 2021





## Sobre el Documento

El siguiente documento describe las variables a ser utilizadas en cada marca, se debe copiar el script tal cual como viene indicado en cada paso. Cuando aparezca un  **`%variable%`**, corresponde a una variable que se debe reemplazar según el dato que corresponda de acuerdo al paso o sección en que se encuentre.

______

































## Scripts

### Landing

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\v2\img\hs_lp.png)



Cuando el socio ingrese a esta página, se debe ejecutar el siguiente script:

```html
<script>
dataLayer.push({
	'event': 'vpv',
	'page' : '/hazte-socio-digital/landing'
});
</script>
```



### Hazte Socio + Cuenta Coopeuch

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\v2\img\hs_lp_1.png)

Cuando el socio haga click en el botón "**Hazte Socio + Cuenta Coopeuch**" de la esquina superior derecha de la página, se debe ejecutar el siguiente script. 

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/hazte-socio-digital/landing',
            'eve-cat' : 'Click',
			'eve-lab' : 'Hazte socio + cuenta coopeuch 1'
		}
	});
</script>
```



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\v2\img\hs_lp_2.png)

Cuando el socio haga click en el botón "**Hazte Socio + Cuenta Coopeuch**" del *banner* ,se debe ejecutar el siguiente script. 

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/hazte-socio-digital/landing',
            'eve-cat' : 'Click',
			'eve-lab' : 'Hazte socio + cuenta coopeuch 2'
		}
	});
</script>
```



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\v2\img\hs_lp_3.png)

Cuando el socio haga click en el botón "**Hazte Socio + Cuenta Coopeuch**" que se encuentra en la huincha a mitad de página, se debe ejecutar el siguiente script.

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/hazte-socio-digital/landing',
            'eve-cat' : 'Click',
			'eve-lab' : 'Hazte socio + cuenta coopeuch 3'
		}
	});
</script>
```

### Desplegables FAQs


![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\v2\img\hs_lp_desplegables_faq.png)

Cuando el socio haga click en alguno de los desplegables,, se debe ejecutar el siguiente script, en donde **``%Nombre%``** corresponde al texto del desplegable al que el cliente hizo click,, que puede ser **¿Qué es el remanente?**, **¿Por qué deberías invertir en cuotas de participación?**, **¿Cuáles son los costos asociados si no accedo al costo 0?** o **¿Las cuotas de participación se pueden considerar un costo de la Cuenta Coopeuch?**.

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/hazte-socio-digital/landing',
            'eve-cat' : 'Click',
			'eve-lab' : '%Nombre%'
		}
	});
</script>
```

### Datos Personales (Paso 1)



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\v2\img\hs_p1.png)



Cuando el socio ingrese a esta página, se debe ejecutar el siguiente script:

```html
<script>
dataLayer.push({
	'event': 'vpv',
	'page' : '/hazte-socio-digital/paso1/verificacion-cedula'
});
</script>
```



#### Verificar Cédula



![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\v2\img\hs_p1_verificar_cedula.png)



Cuando el socio haga click en el botón "**Verificar Cédula**", se debe ejecutar el siguiente script.

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/hazte-socio-digital/paso1/verificacion-cedula',
            'eve-cat' : 'Click',
			'eve-lab' : 'Verificar cédula'
		}
	});
</script>
```

##### Verificación Cédula - Exitosa

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\v2\img\hs_p1_verificar_cedula_exito.png)

Cuando se muestre el banner verde que dice "**Verificación de Cédula de Identidad - Exitosa"**, se debe ejecutar el siguiente script:

```html
<script>
dataLayer.push({
	'event': 'vpv',
	'page' : '/hazte-socio-digital/paso1/verificacion-cedula-exitosa'
});
</script>
```





##### Declaraciones

![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/hazte%20socio%20con%20coopeuch/img/hazte_socio_lp.png)

Cuando el socio ingrese a la página del formulario de enrolamiento, se debe ejecutar el siguiente script:

```html
<script>
dataLayer.push({
	'event': 'vpv',
	'page' : '/hazte-socio/paso1/datos-personales'
});
</script>
```



![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/hazte%20socio%20con%20coopeuch/img/desk-01-declaraciones.png)

Cuando el socio haga click en "**Continuar**", se debe ejecutar el siguiente script. Donde **``%declaracion%``** corresponde a las todas las opciones de declaración seleccionadas, separadas por guión (-) en caso de ser más de una. Las opciones pueden ser **ImpuestoChile**, **EEUU**, **PaisDistinto**.

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
        'producto': '%declaracion%',
		'event-config': {
			'eve-acc' : '/hazte-socio/paso1/datos-personales',
            'eve-cat' : 'Click',
			'eve-lab' : 'Continuar'
		}
	});
</script>
```

Si el usuario hace click en "**Cancelar**", se debe ejecutar el siguiente script.

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/enrolamiento/paso1/datos-personales',
            'eve-cat' : 'Click',
			'eve-lab' : 'Cancelar'
		}
	});
</script>
```




### Preferencias (Paso 2)

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\v2\img\hs_p2.png)

Cuando el socio ingrese a la página principal de preferencias de enrolamiento, se debe ejecutar el siguiente script:

```html
<script>
dataLayer.push({
	'event': 'vpv',
	'page' : '/hazte-socio-digital/paso2/preferencias'
});
</script>
```

#### Autorizar cobro 1 cuota de participación

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\v2\img\hs_p2_cobrar_cuota_participacion.png)

Una vez que el socio haya registrado la cantidad de cuotas por mes, se muestre el monto total de inversión mensual y haga click en "**Sí, autorizo a la empresa de despacho a cobrar 1 uota de participación**", se debe ejecutar el siguiente script. Donde ``%inversion%`` es el monto total mostrado en "** inversión mensual**"

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'monto': '%inversion%',
		'event-config': {
			'eve-acc' : '/hazte-socio-digital/paso2/preferencias',
			'eve-cat' : 'Click',
			'eve-lab' : 'Autorizar cobro 1 cuota de participacion'
		}
});
</script>
```
#### Despacho de las tarjetas Cuenta Vista Coopeuch

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\v2\img\hs_p2_despacho_domicilio_tarjetas.png)

Cuando el socio haga click en "**Continuar**", se debe ejecutar el siguiente script, en donde ``%envio%`` corresponde a la opción seleccionada de envío, que puede ser **domicilio** o **nuevaDireccion**.

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'producto' : '%envio%',
		'event-config': {
			'eve-acc' : '/hazte-socio-digital/paso2/preferencias',
			'eve-cat' : 'Click',
			'eve-lab' : 'Continuar'
		}
});
</script>
```

Si el usuario hace click en "**Cancelar**", se debe ejecutar el siguiente script.

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/hazte-socio-digital/paso2/preferencias',
			'eve-cat' : 'Click',
			'eve-lab' : 'Cancelar'
		}
});
</script>
```

### Identidad (Paso 3)

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\v2\img\hs_p3.png)

Cuando el socio ingrese a esta página, se debe ejecutar el siguiente script.

```html
<script>
dataLayer.push({
	'event': 'vpv',
	'page' : '/hazte-socio-digital/paso3/identidad'
});
</script>
```

#### Validar con ClaveÚnica (botón)

Cuando el socio haga click en el botón "**Validar con ClaveÚnica**", se debe ejecutar el siguiente script

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/hazte-socio-digital/paso3/identidad',
			'eve-cat' : 'Click',
			'eve-lab' : 'Validar con ClaveUnica'
		}
});
</script>
```
Si el socio hace click en el enlace "**Cancelar**", se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/hazte-socio-digital/paso3/identidad',
			'eve-cat' : 'Click',
			'eve-lab' : 'Cancelar'
		}
});
</script>
```

#### Preguntas de seguridad

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\v2\img\hs_p3_preguntas_seguridad.png)

Cuando el socio ingrese a esta página, se debe ejecutar el siguiente script:

```html
<script>
dataLayer.push({
	'event': 'vpv',
	'page' : '/hazte-socio-digital/paso3/preguntas_seguridad'
});
</script>
```
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\v2\img\hs_p3_autorizacion_certificado.png)

Cuando se habilite el botón "**Continuar**" y el socio haga click en el mismo, se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/hazte-socio-digital/paso3/preguntas_seguridad',
			'eve-cat' : 'Click',
			'eve-lab' : 'Continuar'
		}
});
</script>
```

Si el socio hace click en el enlace "**Cancelar**", se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/hazte-socio-digital/paso3/preguntas_seguridad',
			'eve-cat' : 'Click',
			'eve-lab' : 'Cancelar'
		}
});
</script>
```

### Contratación (Paso 4)

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\v2\img\hs_p4.png)Cuando el socio ingrese en esta página, se debe ejecutar el siguiente script:

```html
<script>
dataLayer.push({
	'event': 'vpv',
	'page' : '/hazte-socio-digital/paso4/contratacion'
});
</script>
```

Cuando el socio haga click en "**Descargar set completo**" se debe ejecutar el siguiente script

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/hazte-socio-digital/paso4/contratacion',
            'eve-cat' : 'Click',
			'eve-lab' : 'Descargar set completo'
		}
	});
</script>
```

Cuando el socio haga click en "**Descargar**" se debe ejecutar el siguiente script, en donde ``%nombre-documento%`` corresponde al documento sobre el que se hizo click.

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/hazte-socio-digital/paso4/contratacion',
            'eve-cat' : 'Click Contratos',
			'eve-lab' : '%nombre-documento%'
		}
	});
</script>
```

#### Firma Electrónica

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\v2\img\hs_p4_firma_electronica.png)

Cuando el socio haga click en "**Enviar clave por SMS**", se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/hazte-socio-digital/paso4/contratacion',
            'eve-cat' : 'Click',
			'eve-lab' : 'Enviar clave por SMS'
		}
	});
</script>
```
Cuando el socio haga click en el botón "**Contratar**", se debe ejecutar el siguiente script.:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/hazte-socio-digital/paso4/contratacion',
            'eve-cat' : 'Click',
			'eve-lab' : 'Contratar'
		}
	});
</script>
```

Si el usuario hace click en "**Cancelar**", se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/hazte-socio-digital/paso4/contratacion',
            'eve-cat' : 'Click',
			'eve-lab' : 'Cancelar'
		}
	});
</script>
```

### Comprobante (Paso 5)

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\v2\img\hs_p5.png)

Cuando el socio ingrese a esta página, se debe ejecutar el siguiente script:

```html
<script>
dataLayer.push({
	'event': 'vpv',
	'page' : '/hazte-socio-digital/paso5/comprobante'
});
</script>
```

#### Descargas

Cuando el socio haga click en el enlace "**Descargar**", se debe ejecutar el siguiente script, en donde ``%Nombre%`` corresponde a la sección en que se encuentra el enlace de descarga, que puede ser **Comprobante**, **Detalle** o **Próximo pasos**.

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : 'hazte-socio-digital/paso5/comprobante',
            'eve-cat' : 'Click',
			'eve-lab' : 'Descargar %Nombre%'
		}
	});
</script>
```

Cuando el socio haga click en el enlace "**Volver a Coopeuch**", se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : 'hazte-socio-digital/paso5/comprobante',
            'eve-cat' : 'Click',
			'eve-lab' : 'Volver a Coopeuch'
		}
	});
</script>
```

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\v2\img\hs_p5_danos_opinion.png)

Cuando el socio haga click en el enlace "**Danos tu opinión**", se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : 'hazte-socio-digital/paso5/comprobante',
            'eve-cat' : 'Click',
			'eve-lab' : 'Danos tu opinion'
		}
	});
</script>
```

#### Ver Detalles

Cuando el socio haga click en el enlace "**Ver etalle**", se debe ejecutar el siguiente script, en donde ``%Nombre%`` corresponde a la sección en que se encuentra el enlace de detalles, que puede ser **Tu contratación**, o **Tus contratos firmados**.

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : 'hazte-socio-digital/paso5/comprobante',
            'eve-cat' : 'Click',
			'eve-lab' : 'Ver detalles %Nombre%'
		}
	});
</script>
```

#### ¿Por qué te hiciste socio?

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\v2\img\hs_p5_razones_socio.png)

Cuando el socio elija una de las opciones de este módulo, y haga click en "**Enviar mi respuesta**", se debe ejecutar el siguiente script. Donde ``%motivo%`` indica la opción escogida por el socio en la encuesta:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : 'hazte-socio-digital/paso5/comprobante',
            'eve-cat' : 'Click',
			'eve-lab' : '%motivo%'
		}
	});
</script>
```



### Encuestas

#### Encuesta Abandono

![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/hazte%20socio%20con%20coopeuch/img/ea-00.png)

Cuando aparezca este modal de encuesta en la pantalla del socio, se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event_visto',
		'event-config': {
			'eve-acc' : '/enrolamiento/encuesta-abandono',
            'eve-cat' : 'Encuesta',
			'eve-lab' : 'Encuesta de abandono'
		}
	});
</script>
```



![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/hazte%20socio%20con%20coopeuch/img/ea-otro-motivo.png)

Cuando el socio haga click en "**Enviar respuesta**" se debe ejecutar el siguiente script, en donde **``%motivo%``** corresponde a la opción marcada por el socio:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/enrolamiento/encuesta-abandono',
            'eve-cat' : 'Click',
			'eve-lab' : '%motivo%'
		}
	});
</script>
```

Cuando el socio haga click en "**Retomar contratación**" se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/enrolamiento/encuesta-abandono',
            'eve-cat' : 'Click',
			'eve-lab' : '%motivo%'
		}
	});
</script>
```

#### Encuesta Satisfacción

![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/hazte%20socio%20con%20coopeuch/img/es-02.png)








Cuando el usuario haga click en "**Enviar y continuar**", se debe ejecutar el siguiente script, en donde **``%nivel%``** corresponde a la cantidad de estrellas seleccionadas.

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/enrolamiento/encuesta-satisfaccion',
            'eve-cat' : 'Click',
			'eve-lab' : 'Enviar y continuar %nivel%'
		}
	});
</script>
```



#### Encuesta Usabilidad

![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/Enrol_10.png)

Cuando el usuario haga click en "**Enviar y terminar**", se debe ejecutar el siguiente script, en donde **``%nivel%``** corresponde a la cantidad de facilidad seleccionado, de 1 a 7; siendo 1 "**Muy difícil**" y 7 "**Muy fácil**".

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/enrolamiento/encuesta-satisfaccion',
            'eve-cat' : 'Click',
			'eve-lab' : 'Enviar y terminar %nivel%'
		}
	});
</script>
```


### Casos Bordes y errores

#### Modal error: Ya eres Socio Coopeuch

![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/casos%20borde%20hazte%20socio%20con%20coopeuch/img/vis_error_datos_personales.png)

Cuando el aparezca el modal de error "**¡Importante! Ya eres Socio Coopeuch**" en la pantalla del dispositivo del socio, se debe ejecutar el siguiente script. 

```html
<script>
	dataLayer.push({
		'event': 'trigger_event_visto',
		'event-config': {
			'eve-acc' : '/enrolamiento/ya-eres-socio',
            'eve-cat' : 'Error',
			'eve-lab' : 'Ya eres socio Coopeuch'
		}
	});
</script>
```



En caso donde el socio haga click en "**Ir a mi Sitio Privado**" debe ejecutarse el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/enrolamiento/ya-eres-socio',
            'eve-cat' : 'Click',
			'eve-lab' : 'Ir a mi Sitio Privado'
		}
	});
</script>
```



Cuando el socio haga click en **Solicitar Cuenta Coopeuch**, debe ejecutarse el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/enrolamiento/ya-eres-socio',
            'eve-cat' : 'Click',
			'eve-lab' : 'Solicitar Cuenta Coopeuch'
		}
	});
</script>
```

#### Modal error: Número ya registrado

![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/casos%20borde%20hazte%20socio%20con%20coopeuch/img/vis_error_numero_ya_registrado.png)

Cuando el aparezca el modal de error "**¡Lo sentimos! Número ya registrado**" en la pantalla del dispositivo del socio, se debe ejecutar el siguiente script. 

```html
<script>
	dataLayer.push({
		'event': 'trigger_event_visto',
		'event-config': {
			'eve-acc' : '/enrolamiento/error',
            'eve-cat' : 'Error',
			'eve-lab' : 'Numero ya registrado'
		}
	});
</script>
```

En caso donde el socio haga click en "**Entendido**" debe ejecutarse el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/enrolamiento/error',
            'eve-cat' : 'Click',
			'eve-lab' : 'Entendido Numero ya registrado'
		}
	});
</script>
```



#### Modal error: Declaraciones

![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/casos%20borde%20hazte%20socio%20con%20coopeuch/img/vis_error_declaraciones.png)

Cuando el aparezca el modal de error "**¡Lo sentimos! No es posible continuar con el proceso**" en la pantalla del dispositivo del socio, se debe ejecutar el siguiente script. Donde `%ubicacion%` corresponde al lugar del flujo en el que ocurre el error:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event_visto',
		'event-config': {
			'eve-acc' : 'Enrolamiento %ubicacion%',
            'eve-cat' : 'Error',
			'eve-lab' : 'Imposible continuar proceso'
		}
	});
</script>
```

En caso donde el socio haga click en "**Entendido**" debe ejecutarse el siguiente script, donde `%ubicacion%` corresponde al lugar del flujo en el que ocurre el error:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : 'Enrolamiento %ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Entendido Imposible continuar proceso'
		}
	});
</script>
```

#### Modal error: Dirección sin cobertura

![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/casos%20borde%20hazte%20socio%20con%20coopeuch/img/vis_error_direccion_sin_cobertura.png)

Cuando el aparezca el modal de error "**¡Lo sentimos! La dirección ingresada no tiene cobertura**" en la pantalla del dispositivo del socio, se debe ejecutar el siguiente script. 

```html
<script>
	dataLayer.push({
		'event': 'trigger_event_visto',
		'event-config': {
			'eve-acc' : '/enrolamiento/preferencias',
            'eve-cat' : 'Error',
			'eve-lab' : 'Direccion sin cobertura'
		}
	});
</script>
```

En caso donde el socio haga click en "**Entendido**" debe ejecutarse el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/enrolamiento/preferencias',
            'eve-cat' : 'Click',
			'eve-lab' : 'Entendido Direccion sin cobertura'
		}
	});
</script>
```



#### Modal error: Validación de identidad

![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/casos%20borde%20hazte%20socio%20con%20coopeuch/img/vis_error_id_validation_error.png)

Cuando el aparezca el modal de error "**¡Lo sentimos! No ha sido posible validar tu identidad**" en la pantalla del dispositivo del socio, se debe ejecutar el siguiente script. 

```html
<script>
	dataLayer.push({
		'event': 'trigger_event_visto',
		'event-config': {
			'eve-acc' : '/enrolamiento-validacion/paso2',
            'eve-cat' : 'Error',
			'eve-lab' : 'Imposible validar identidad'
		}
	});
</script>
```

En caso donde el socio haga click en "**Entendido**" debe ejecutarse el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/enrolamiento-validacion/paso2',
            'eve-cat' : 'Click',
			'eve-lab' : 'Entendido Imposible validar identidad'
		}
	});
</script>
```





#### Clave dinámica SMS: Tiempo agotado

![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/casos%20borde%20hazte%20socio%20con%20coopeuch/img/vis_error_sms_tiempo_agotado.png)

Cuando el contador del tiempo para el ingreso de la clave dinámica llegue a 00:00 y se muestre en la pantalla del socio, se debe ejecutar el siguiente script. 

```html
<script>
	dataLayer.push({
		'event': 'trigger_event_visto',
		'event-config': {
			'eve-acc' : '/enrolamiento/contratacion',
            'eve-cat' : 'Error',
			'eve-lab' : 'Clave dinámica SMS Tiempo agotado'
		}
	});
</script>
```


#### Clave dinámica SMS: Clave incorrecta

![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/casos%20borde%20hazte%20socio%20con%20coopeuch/img/vis_error_sms_clave_incorrecta.png)

Cuando se muestre el mensaje "**La clave dinámica ingresada es incorrecta. Reintentar** en la pantalla del socio, se debe ejecutar el siguiente script.



```html
<script>
	dataLayer.push({
		'event': 'trigger_event_visto',
		'event-config': {
			'eve-acc' : '/enrolamiento/contratacion',
            'eve-cat' : 'Error',
			'eve-lab' : 'Clave incorrecta'
		}
	});
</script>
```



#### Clave dinámica SMS: Método bloqueado

![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/casos%20borde%20hazte%20socio%20con%20coopeuch/img/vis_error_sms_bloqueado.png)

Cuando la huincha que advierte con el mensaje "**Has excedido el límite de intentos**" se muestre en la pantalla del socio, se debe ejecutar el siguiente script. 

```html
<script>
	dataLayer.push({
		'event': 'trigger_event_visto',
		'event-config': {
			'eve-acc' : '/enrolamiento/contratacion',
            'eve-cat' : 'Error',
			'eve-lab' : 'Clave dinámica SMS Bloqueada'
		}
	});
</script>
```

