<img align="left" width="19%" height="19%" src="https://gitlab.com/d2b_chile/formato-documento-markdown/-/raw/master/Imagenes/D2B-GRIS-POSITIVO-CON BAJADA-01.png">

# **Documento de Implementación** Enrolamiento Biometría  (Google Analytics)

Marzo 2021





## Sobre el Documento

El siguiente documento describe las variables a ser utilizadas en cada marca, se debe copiar el script tal cual como viene indicado en cada paso. Cuando aparezca un  **`%variable%`**, corresponde a una variable que se debe reemplazar según el dato que corresponda de acuerdo al paso o sección en que se encuentre.

______

































## Scripts

### Landing

![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/Enrol_01.png)



Cuando el socio ingrese a esta página, se debe ejecutar el siguiente script:

```html
<script>
dataLayer.push({
	'event': 'vpv',
	'page' : '/hazte-socio-digital/landing'
});
</script>
```



### Hazte Socio + Cuenta Coopeuch

![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/hazte%20socio%20con%20coopeuch/img/hazte_socio_boton1.png)

Cuando el socio haga click en el botón "**Hazte Socio + Cuenta Coopeuch**" de la esquina superior derecha de la página, se debe ejecutar el siguiente script. 

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/hazte-socio-digital/landing',
            'eve-cat' : 'Click',
			'eve-lab' : 'Hazte socio + cuenta coopeuch 1'
		}
	});
</script>
```



![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/hazte%20socio%20con%20coopeuch/img/hazte_socio_boton2.png)

Cuando el socio haga click en el botón "**Hazte Socio + Cuenta Coopeuch**" del *banner* ,se debe ejecutar el siguiente script. 

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/hazte-socio-digital/landing',
            'eve-cat' : 'Click',
			'eve-lab' : 'Hazte socio + cuenta coopeuch 2'
		}
	});
</script>
```



![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/Enrol_02.png)

Cuando el socio haga click en el botón "**Hazte Socio + Cuenta Coopeuch**" que se encuentra en la huincha a mitad de página, se debe ejecutar el siguiente script.

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/hazte-socio-digital/landing',
            'eve-cat' : 'Click',
			'eve-lab' : 'Hazte socio + cuenta coopeuch 3'
		}
	});
</script>
```



### Quiero que me contacten

![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/hazte%20socio%20con%20coopeuch/img/quiero_que_me_contacten.png)



Cuando el socio haga click en el botón "**Quiero que me contacten**" que se encuentra en la huincha a al final de la página, se debe ejecutar el siguiente script.

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/hazte-socio-digital/landing',
            'eve-cat' : 'Click',
			'eve-lab' : 'Quiero que me contacten'
		}
	});
</script>
```



### Validación Identidad



![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/Enrol_03.png)



Cuando el socio ingrese a esta página, se debe ejecutar el siguiente script:

```html
<script>
dataLayer.push({
	'event': 'vpv',
	'page' : '/hazte-socio-digital/validacion-identidad'
});
</script>
```



![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/hazte%20socio%20con%20coopeuch/img/responder_preguntas.png)



Cuando el socio haga click en el botón "**Responder preguntas**" que se encuentra en el módulo titulado "**¿Tienes problemas para escanear el código?**", se debe ejecutar el siguiente script. 

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/hazte-socio-digital/validacion-identidad',
            'eve-cat' : 'Click',
			'eve-lab' : 'Responder preguntas'
		}
	});
</script>
```























### Validación Preguntas

#### Paso 1![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/hazte%20socio%20con%20coopeuch/img/vp_lp.png)

Cuando el socio ingrese a esta página, se debe ejecutar el siguiente script:

```html
<script>
dataLayer.push({
	'event': 'vpv',
	'page' : '/enrolamiento-validacion/paso1'
});
</script>
```

Cuando el socio haga click en "**Continuar**", se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/enrolamiento-validacion/paso1',
            'eve-cat' : 'Click',
			'eve-lab' : 'Continuar'
		}
	});
</script>
```



Si el usuario hace click en "**Cancelar**", se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/enrolamiento-validacion/paso1',
            'eve-cat' : 'Click',
			'eve-lab' : 'Cancelar'
		}
	});
</script>
```



#### Paso 2

![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/hazte%20socio%20con%20coopeuch/img/vp_autorizacion.png)Cuando el socio ingrese a esta página, se debe ejecutar el siguiente script:

```html
<script>
dataLayer.push({
	'event': 'vpv',
	'page' : '/enrolamiento-validacion/paso2'
});
</script>
```



Cuando el socio haga click en "**Continuar**", se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/enrolamiento-validacion/paso2',
            'eve-cat' : 'Click',
			'eve-lab' : 'Continuar'
		}
	});
</script>
```



Si el usuario hace click en "**Cancelar**", se debe ejecutar el siguiente script. Donde ``%ubicacion`` corresponde al lugar del flujo en el que se muestra el botón:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/Enrolamiento %ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Cancelar'
		}
	});
</script>
```



#### Paso 3![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/hazte%20socio%20con%20coopeuch/img/vp_validacion_identidad_1.png)

Cuando el socio ingrese a esta página, se debe ejecutar el siguiente script:

```html
<script>
dataLayer.push({
'event': 'vpv',
'page' : '/enrolamiento-validacion/paso3'
});
</script>
```



Cuando el socio haga click en "**Continuar**", se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/enrolamiento-validacion/paso3',
            'eve-cat' : 'Click',
			'eve-lab' : 'Continuar'
		}
	});
</script>
```



#### Paso 4 - Comprobante

![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/hazte%20socio%20con%20coopeuch/img/vp_validacion_exitosa.png)

Cuando aparezca este modal en la pantalla del usuario, se debe ejecutar el siguiente script:

```html
<script>
dataLayer.push({
'event': 'vpv',
'page' : '/enrolamiento-validacion/paso4'
});
</script>
```
Si el usuario hace click al botón "**Continuar contratación**" en el mismo modal, se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/enrolamiento-validacion/paso4',
            'eve-cat' : 'Click',
			'eve-lab' : 'Continuar contratacion'
		}
	});
</script>
```

### Flujo enrolamiento
#### Datos Personales - Paso 1![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/hazte%20socio%20con%20coopeuch/img/hazte_socio_lp.png)

Cuando el socio ingrese a la página del formulario de enrolamiento, se debe ejecutar el siguiente script:

```html
<script>
dataLayer.push({
	'event': 'vpv',
	'page' : '/enrolamiento/datos-personales'
});
</script>
```



![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/hazte%20socio%20con%20coopeuch/img/desk-01-declaraciones.png)

Cuando el socio haga click en "**Continuar**", se debe ejecutar el siguiente script. Donde **``%declaracion%``** corresponde a las todas las opciones de declaración seleccionadas, separadas por guión (-) en caso de ser más de una. Las opciones pueden ser **ImpuestoChile**, **EEUU**, **PaisDistinto**.

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
        'producto': '%declaracion%',
		'event-config': {
			'eve-acc' : '/enrolamiento/datos-personales',
            'eve-cat' : 'Click',
			'eve-lab' : 'Continuar'
		}
	});
</script>
```

Si el usuario hace click en "**Cancelar**", se debe ejecutar el siguiente script.

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/enrolamiento/datos-personales',
            'eve-cat' : 'Click',
			'eve-lab' : 'Cancelar'
		}
	});
</script>
```



#### Preferencias - Paso 2


![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/hazte%20socio%20con%20coopeuch/img/hazte_socio_preferencias.png)Cuando el socio ingrese a la página principal de preferencias de enrolamiento, se debe ejecutar el siguiente script:

```html
<script>
dataLayer.push({
	'event': 'vpv',
	'page' : '/enrolamiento/preferencias'
});
</script>
```



![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/hazte%20socio%20con%20coopeuch/img/hazte_socio_preferencias_mandato_pago_primera_cuota_aceptar.png)

Una vez que el socio haya registrado la cantidad de cuotas por mes, se muestre el monto total de inversión mensual y haga click en "**Sí, autorizo a la empresa de despacho a cobrar mi primera cuota de participación**", se debe ejecutar el siguiente script. Donde **``%inversion``** es el monto total mostrado en "**Tu inversión total**"

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'monto': '%inversion%',
		'event-config': {
			'eve-acc' : '/enrolamiento/preferencias',
            'eve-cat' : 'Click',
			'eve-lab' : 'Autorizar cobro primera cuota de participacion'
		}
	});
</script>
```

![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/Enrol_05.png)

Cuando el socio haga click en **Continuar**, se debe ejecutar el siguiente script, en donde **```%envio%```** corresponde a la opción seleccionada de envío, que puede ser **domicilio** o **nuevaDireccion**.

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
        'producto' : '%envio%',
		'event-config': {
			'eve-acc' : '/enrolamiento/preferencias',
            'eve-cat' : 'Click',
			'eve-lab' : '%Continuar%'
		}
	});
</script>
```



Si el usuario hace click en "**Cancelar**", se debe ejecutar el siguiente script. Donde ``%ubicacion`` corresponde al lugar del flujo en el que se muestra el botón:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/enrolamiento/preferencias',
            'eve-cat' : 'Click',
			'eve-lab' : 'Cancelar'
		}
	});
</script>
```





#### Contratación - Paso 3

![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/hazte%20socio%20con%20coopeuch/img/hazte_socio_contratacion_lp.png)

Cuando el socio ingrese a la página de la contratación del enrolamiento, se debe ejecutar el siguiente script:

```html
<script>
dataLayer.push({
	'event': 'vpv',
	'page' : '/enrolamiento/contratacion'
});
</script>
```

![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/hazte%20socio%20con%20coopeuch/img/hazte_socio_contratacion_descargar_contratos.png)

Cuando el socio haga click en "**Descargar Contratos**" se debe ejecutar el siguiente script

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/enrolamiento/contratacion',
            'eve-cat' : 'Click',
			'eve-lab' : 'Descargar Contratos'
		}
	});
</script>
```

Cuando el socio haga click en "**Descargar**" se debe ejecutar el siguiente script, en donde **``%nombre-documento%``** corresponde al documento sobre el que se hizo click.

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/enrolamiento/contratacion',
            'eve-cat' : 'Click Contratos',
			'eve-lab' : '%nombre-documento%'
		}
	});
</script>
```



![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/hazte%20socio%20con%20coopeuch/img/hazte_socio_contratacion_terminos_condiciones_aceptar.png)

Cuando el socio haga click en la caja de "**Sí, acepto los términos y condiciones**", se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/enrolamiento/contratacion',
            'eve-cat' : 'Click',
			'eve-lab' : 'Acepto terminos y condiciones'
		}
	});
</script>
```

![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/hazte%20socio%20con%20coopeuch/img/hazte_socio_contratacion_enviar_clave_sms.png)

Cuando el socio haga click en el botón "**Enviar clave SMS**", se debe ejecutar el siguiente script.:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/enrolamiento/contratacion',
            'eve-cat' : 'Click',
			'eve-lab' : 'Enviar clave SMS'
		}
	});
</script>
```
![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/hazte%20socio%20con%20coopeuch/img/hazte_socio_boton_contratar.png)

Cuando el socio haga click en el botón "**Contratar**", se debe ejecutar el siguiente script.:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/enrolamiento/contratacion',
            'eve-cat' : 'Click',
			'eve-lab' : 'Contratar'
		}
	});
</script>
```

Si el usuario hace click en "**Cancelar**", se debe ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra el botón:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/enrolamiento/contratacion',
            'eve-cat' : 'Click',
			'eve-lab' : 'Cancelar'
		}
	});
</script>
```



#### Comprobante - Paso 4

![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/Enrol_06.png)

Cuando el socio ingrese a la página de comprobante del enrolamiento, se debe ejecutar el siguiente script:

```html
<script>
dataLayer.push({
	'event': 'vpv',
	'page' : '/enrolamiento/comprobante'
});
</script>
```

Cuando el socio haga click en el enlace "**Descargar**", se debe ejecutar el siguiente script, en donde **``%Nombre%``** corresponde a la sección en que se encuentra el enlace de descarga, que puede ser **Comprobante**, **Detalle** o **Próximo pasos a seguir**.

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/enrolamiento/comprobante',
            'eve-cat' : 'Click',
			'eve-lab' : '%Nombre%'
		}
	});
</script>
```



![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/Enrol_07.png)

Cuando el socio haga click en el enlace "**¿Qué te pareció contratar este producto?**", se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/enrolamiento/comprobante',
            'eve-cat' : 'Click',
			'eve-lab' : 'Encuesta'
		}
	});
</script>
```

Cuando el socio haga click en el enlace "**Volver a Coopeuch**", se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/enrolamiento/comprobante',
            'eve-cat' : 'Click',
			'eve-lab' : 'Volver a Coopeuch'
		}
	});
</script>
```




### Encuestas Hazte Socio

#### Encuesta Abandono

![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/hazte%20socio%20con%20coopeuch/img/ea-00.png)

Cuando aparezca este modal de encuesta en la pantalla del socio, se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event_visto',
		'event-config': {
			'eve-acc' : '/enrolamiento/encuesta-abandono',
            'eve-cat' : 'Encuesta',
			'eve-lab' : 'Encuesta de abandono'
		}
	});
</script>
```



![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/hazte%20socio%20con%20coopeuch/img/ea-otro-motivo.png)

Cuando el socio haga click en "**Enviar respuesta**" se debe ejecutar el siguiente script, en donde **``%motivo%``** corresponde a la opción marcada por el socio:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/enrolamiento/encuesta-abandono',
            'eve-cat' : 'Click',
			'eve-lab' : '%motivo%'
		}
	});
</script>
```

Cuando el socio haga click en "**Retomar contratación**" se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/enrolamiento/encuesta-abandono',
            'eve-cat' : 'Click',
			'eve-lab' : '%motivo%'
		}
	});
</script>
```

#### Encuesta Satisfacción

![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/hazte%20socio%20con%20coopeuch/img/es-02.png)








Cuando el usuario haga click en "**Enviar y continuar**", se debe ejecutar el siguiente script, en donde **``%nivel%``** corresponde a la cantidad de estrellas seleccionadas.

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/enrolamiento/encuesta-satisfaccion',
            'eve-cat' : 'Click',
			'eve-lab' : 'Enviar y continuar %nivel%'
		}
	});
</script>
```



#### Encuesta Usabilidad

![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/Enrol_10.png)

Cuando el usuario haga click en "**Enviar y terminar**", se debe ejecutar el siguiente script, en donde **``%nivel%``** corresponde a la cantidad de facilidad seleccionado, de 1 a 7; siendo 1 "**Muy difícil**" y 7 "**Muy fácil**".

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/enrolamiento/encuesta-satisfaccion',
            'eve-cat' : 'Click',
			'eve-lab' : 'Enviar y terminar %nivel%'
		}
	});
</script>
```



### Casos Bordes y errores

#### Modal error: Ya eres Socio Coopeuch

![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/casos%20borde%20hazte%20socio%20con%20coopeuch/img/vis_error_datos_personales.png)

Cuando el aparezca el modal de error "**¡Importante! Ya eres Socio Coopeuch**" en la pantalla del dispositivo del socio, se debe ejecutar el siguiente script. 

```html
<script>
	dataLayer.push({
		'event': 'trigger_event_visto',
		'event-config': {
			'eve-acc' : '/enrolamiento/ya-eres-socio',
            'eve-cat' : 'Error',
			'eve-lab' : 'Ya eres socio Coopeuch'
		}
	});
</script>
```



En caso donde el socio haga click en "**Ir a mi Sitio Privado**" debe ejecutarse el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/enrolamiento/ya-eres-socio',
            'eve-cat' : 'Click',
			'eve-lab' : 'Ir a mi Sitio Privado'
		}
	});
</script>
```



Cuando el socio haga click en **Solicitar Cuenta Coopeuch**, debe ejecutarse el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/enrolamiento/ya-eres-socio',
            'eve-cat' : 'Click',
			'eve-lab' : 'Solicitar Cuenta Coopeuch'
		}
	});
</script>
```

#### Modal error: Número ya registrado

![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/casos%20borde%20hazte%20socio%20con%20coopeuch/img/vis_error_numero_ya_registrado.png)

Cuando el aparezca el modal de error "**¡Lo sentimos! Número ya registrado**" en la pantalla del dispositivo del socio, se debe ejecutar el siguiente script. 

```html
<script>
	dataLayer.push({
		'event': 'trigger_event_visto',
		'event-config': {
			'eve-acc' : '/enrolamiento/error',
            'eve-cat' : 'Error',
			'eve-lab' : 'Numero ya registrado'
		}
	});
</script>
```

En caso donde el socio haga click en "**Entendido**" debe ejecutarse el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/enrolamiento/error',
            'eve-cat' : 'Click',
			'eve-lab' : 'Entendido Numero ya registrado'
		}
	});
</script>
```



#### Modal error: Declaraciones

![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/casos%20borde%20hazte%20socio%20con%20coopeuch/img/vis_error_declaraciones.png)

Cuando el aparezca el modal de error "**¡Lo sentimos! No es posible continuar con el proceso**" en la pantalla del dispositivo del socio, se debe ejecutar el siguiente script. Donde `%ubicacion%` corresponde al lugar del flujo en el que ocurre el error:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event_visto',
		'event-config': {
			'eve-acc' : 'Enrolamiento %ubicacion%',
            'eve-cat' : 'Error',
			'eve-lab' : 'Imposible continuar proceso'
		}
	});
</script>
```

En caso donde el socio haga click en "**Entendido**" debe ejecutarse el siguiente script, donde `%ubicacion%` corresponde al lugar del flujo en el que ocurre el error:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : 'Enrolamiento %ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Entendido Imposible continuar proceso'
		}
	});
</script>
```

#### Modal error: Dirección sin cobertura

![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/casos%20borde%20hazte%20socio%20con%20coopeuch/img/vis_error_direccion_sin_cobertura.png)

Cuando el aparezca el modal de error "**¡Lo sentimos! La dirección ingresada no tiene cobertura**" en la pantalla del dispositivo del socio, se debe ejecutar el siguiente script. 

```html
<script>
	dataLayer.push({
		'event': 'trigger_event_visto',
		'event-config': {
			'eve-acc' : '/enrolamiento/preferencias',
            'eve-cat' : 'Error',
			'eve-lab' : 'Direccion sin cobertura'
		}
	});
</script>
```

En caso donde el socio haga click en "**Entendido**" debe ejecutarse el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/enrolamiento/preferencias',
            'eve-cat' : 'Click',
			'eve-lab' : 'Entendido Direccion sin cobertura'
		}
	});
</script>
```



#### Modal error: Validación de identidad

![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/casos%20borde%20hazte%20socio%20con%20coopeuch/img/vis_error_id_validation_error.png)

Cuando el aparezca el modal de error "**¡Lo sentimos! No ha sido posible validar tu identidad**" en la pantalla del dispositivo del socio, se debe ejecutar el siguiente script. 

```html
<script>
	dataLayer.push({
		'event': 'trigger_event_visto',
		'event-config': {
			'eve-acc' : '/enrolamiento-validacion/paso2',
            'eve-cat' : 'Error',
			'eve-lab' : 'Imposible validar identidad'
		}
	});
</script>
```

En caso donde el socio haga click en "**Entendido**" debe ejecutarse el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/enrolamiento-validacion/paso2',
            'eve-cat' : 'Click',
			'eve-lab' : 'Entendido Imposible validar identidad'
		}
	});
</script>
```





#### Clave dinámica SMS: Tiempo agotado

![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/casos%20borde%20hazte%20socio%20con%20coopeuch/img/vis_error_sms_tiempo_agotado.png)

Cuando el contador del tiempo para el ingreso de la clave dinámica llegue a 00:00 y se muestre en la pantalla del socio, se debe ejecutar el siguiente script. 

```html
<script>
	dataLayer.push({
		'event': 'trigger_event_visto',
		'event-config': {
			'eve-acc' : '/enrolamiento/contratacion',
            'eve-cat' : 'Error',
			'eve-lab' : 'Clave dinámica SMS Tiempo agotado'
		}
	});
</script>
```


#### Clave dinámica SMS: Clave incorrecta

![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/casos%20borde%20hazte%20socio%20con%20coopeuch/img/vis_error_sms_clave_incorrecta.png)

Cuando se muestre el mensaje "**La clave dinámica ingresada es incorrecta. Reintentar** en la pantalla del socio, se debe ejecutar el siguiente script.



```html
<script>
	dataLayer.push({
		'event': 'trigger_event_visto',
		'event-config': {
			'eve-acc' : '/enrolamiento/contratacion',
            'eve-cat' : 'Error',
			'eve-lab' : 'Clave incorrecta'
		}
	});
</script>
```



#### Clave dinámica SMS: Método bloqueado

![](https://gitlab.com/rroblesd2b/img_md/-/raw/master/Cpch/Pub/Biometria-enrolamiento/casos%20borde%20hazte%20socio%20con%20coopeuch/img/vis_error_sms_bloqueado.png)

Cuando la huincha que advierte con el mensaje "**Has excedido el límite de intentos**" se muestre en la pantalla del socio, se debe ejecutar el siguiente script. 

```html
<script>
	dataLayer.push({
		'event': 'trigger_event_visto',
		'event-config': {
			'eve-acc' : '/enrolamiento/contratacion',
            'eve-cat' : 'Error',
			'eve-lab' : 'Clave dinámica SMS Bloqueada'
		}
	});
</script>
```

