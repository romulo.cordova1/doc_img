<img align="left" width="19%" height="19%" src="https://gitlab.com/d2b_chile/formato-documento-markdown/-/raw/master/Imagenes/D2B-GRIS-POSITIVO-CON BAJADA-01.png">

# **Documento de Implementación** Marcas en Cuota de Participación  (Google Analytics)

Marzo 2021





## Sobre el Documento

El siguiente documento describe las variables a ser utilizadas en cada marca, se debe copiar el script tal cual como viene indicado en cada paso. Cuando aparezca un  **`%variable%`**, corresponde a una variable que se debe reemplazar según el dato que corresponda de acuerdo al paso o sección en que se encuentre.

______

































## Scripts
### Landing
#### Solicita - Cuota de Participación
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Documento Flujo Participación\img\flujo_participacion-landing.png)

Cuando el socio ingrese a la sección de "**Cuota de Participación**", se debe ejecutar el siguiente script. Donde ``%aportado%`` hace referencia al monto aportado por el socio hasta la fecha:

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/solicita/cuota-participacion',
    'monto' : '%aportado%'
});
</script>
```


![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Documento Flujo Participación\img\flujo_participacion-pagar_cuotas.png)

Si el socio hace click en el botón "**Pagar Cuotas**", se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'eve',
			'eve-acc' : '/tef/solicita/cuota-participacion',
			'eve-cat' : 'Click',
			'eve-lab' : 'Pagar Cuotas'
	});
</script>
```

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Documento Flujo Participación\img\flujo_participacion-pago_online.png)


Si el socio hace click en el botón "**Ir a pago online**", se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'eve',
			'eve-acc' : '/tef/solicita/cuota-participacion',
			'eve-cat' : 'Click',
			'eve-lab' : 'Ir a pago online'
	});
</script>
```

#### Mis Productos - Cuota de Participación
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Documento Flujo Participación\img\flujo_participacion-misproductos_cuota.png)

Cuando el socio ingrese a la sección de "**Cuota de Participación**", a través del desplegable "**Mis Productos**" se debe ejecutar el siguiente script. Donde ``%aportado%`` hace referencia al monto ahorrado por el socio hasta la fecha:

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/mis-productos/cuota-participacion',
    'monto' : '%aportado%'
});
</script>
```

Si el socio hace click en el botón "**Pagar Cuota**", se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'eve',
			'eve-acc' : '/tef/mis-productos/cuota-participacion',
			'eve-cat' : 'Click',
			'eve-lab' : 'Pagar Cuota'
	});
</script>
```

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Documento Flujo Participación\img\flujo_participacion-sin_monto_pendiente.png)

Cuando aparezca el banner que dice "**¡Muy bien! Estás al día en el pago de tus cuotas**", se debe ejecutar el siguiente script:

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/mis-productos/pago-cuota-exitoso',
    'monto' : '%aportado%'
});
</script>
```
##### Invitación a pago WebPay

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Documento Flujo Participación\img\flujo_participacion-invitacion_pago_webpay.png)

Cuando aparezca el mensaje invitando al socio a realizar el pago de sus cuotas a través de "**WebPay**", se debe ejecutar el siguiente script:

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/mis-productos/cuota-participacion/pago-webpay',
    'monto' : '%aportado%'
});
</script>
```

Si el cliente hace click en el botón "**Ir a pago online**", se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'eve',
			'eve-acc' : '/tef/mis-productos/cuota-participacion/pago-webpay',
			'eve-cat' : 'Click',
			'eve-lab' : 'Ir a pago online'
	});
</script>
```

##### Estás suscrito por planilla

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Documento Flujo Participación\img\flujo_participacion-suscrito_por_planilla.png)

Cuando aparezca el mensaje del banner azul, se debe ejecutar el siguiente script:

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/mis-productos/cuota-participacion/suscrito-por-planilla',
    'monto' : '%aportado%'
});
</script>
```

##### Estás suscrito por Pago Automático de Cuentas

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Documento Flujo Participación\img\flujo_participacion-suscrito_pac.png)

Cuando aparezca el mensaje del banner azul, se debe ejecutar el siguiente script:

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/mis-productos/cuota-participacion/suscrito-pac',
    'monto' : '%aportado%'
});
</script>
```
### Pago de cuota de participación
#### (Paso 1) Mis Productos - Cuota de Participación - Monto a Pagar
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Documento Flujo Participación\img\flujo_participacion-pago_cuota_participacion.png)

Cuando el socio ingrese a la sección de "**Cuota de Participación**", a través del desplegable "**Mis Productos**", para Pagar la Cyota de Participación,  se debe ejecutar el siguiente script.

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/pago-cuota-participacion/paso-1',
});
</script>
```
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Documento Flujo Participación\img\flujo_participacion-pago_cuota_participacion-continuar.png)

Cuando el socio haga click en el botón "**Continuar**"", se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'eve',
			'eve-acc' : '/tef/pago-cuota-participacion/paso-1',
			'eve-cat' : 'Click',
			'eve-lab' : 'Continuar'
	});
</script>
```

Si el socio hace click en "**Volver**", se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'eve',
			'eve-acc' : '/tef/pago-cuota-participacion/paso-1',
			'eve-cat' : 'Click',
			'eve-lab' : 'Volver'
	});
</script>
```

#### (Paso 2) Mis Productos - Cuota de Participación - Pago

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Documento Flujo Participación\img\flujo_participacion-pago_cuota_participacion.png)

Cuando el socio ingrese a esta página,  se debe ejecutar el siguiente script. Donde ```%monto-a-pagar%``` hace referencia al **Monto a Pagar** reflejado en el "**Resumen de Pago**"

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/pago-cuota-participacion/paso-2',
	'monto' : '%monto-a-pagar%',
	
});
</script>
```

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Documento Flujo Participación\img\flujo_participacion-pago_cuota_participacion-paso2-pagar.png)

Cuando el socio haga click en el botón "**Pagar**", se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'eve',
			'eve-acc' : '/tef/pago-cuota-participacion/paso-2',
			'eve-cat' : 'Click',
			'eve-lab' : 'Pagar',
			'monto' : '%monto-a-pagar%',
			'cuotas' : '%cuotas%'
	});
</script>
```
Si el socio hace click e "**Volver**", se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'eve',
			'eve-acc' : '/tef/pago-cuota-participacion/paso-2',
			'eve-cat' : 'Click',
			'eve-lab' : 'Volver'
	});
</script>
```

#### (Paso 3) Mis Productos - Cuota de Participación - Comprobante

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Documento Flujo Participación\img\flujo_participacion-pago_cuota_participacion-paso3.png)

Cuando el socio ingrese a esta página,  se debe ejecutar el siguiente script. Donde ```%monto-a-pagar%``` hace referencia al **Monto a Pagar** reflejado en el "**Resumen de Pago**", y ```%cuotas%``` hace referencia al número de cuotas a pagar del socio (vistas en la página anterior)

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/pago-cuota-participacion/paso-3',
	'monto' : '%monto-a-pagar%',
	'cuotas' : '%cuotas%'
	
});
</script>
```

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Documento Flujo Participación\img\flujo_participacion-pago_cuota_participacion-paso3-ir_cuota_participacion.png)

Cuando el socio haga click en el botón "**Ir a Cuota de Participación**", se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'eve',
			'eve-acc' : '/tef/pago-cuota-participacion/paso-3',
			'eve-cat' : 'Click',
			'eve-lab' : 'Ir a Cuota de Partipación'
	});
</script>
```
Si el socio hace click e "**Volver**", se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'eve',
			'eve-acc' : '/tef/pago-cuota-participacion/paso-3',
			'eve-cat' : 'Click',
			'eve-lab' : 'Volver'
	});
</script>
```

### Mensajes de error

#### En Paso 1

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Documento Flujo Participación\img\flujo_partipacion-error-paso1.png)

Cuando el socio ingrese un máximo de cuotas superior al de "**cuotas pendientes**", se debe ejecutar el siguiente script. Donde ``%maximo de cuotas%``es el número máximo de cuotas pendientes del socio..

```html
<script>
	dataLayer.push({
		'event': 'eve',
			'eve-acc' : '/tef/pago-cuota-participacion/paso-1',
			'eve-cat' : 'Error',
			'eve-lab' : 'Excede máximo de %maximo-cuotas% cuotas'
	});
</script>
```
#### En Paso 2

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Documento Flujo Participación\img\flujo_partipacion-error-paso2_a.png)

Cuando aparezca este error en el dispositivo del socio, se debe ejecutar el siguiente script. 


```html
<script>
	dataLayer.push({
		'event': 'eve',
			'eve-acc' : '/tef/pago-cuota-participacion/paso-2',
			'eve-cat' : 'Error',
			'eve-lab' : 'No tienes saldo suficiente'
	});
</script>
```

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Documento Flujo Participación\img\flujo_partipacion-error-paso2_b.png)

Cuando aparezca este error en el dispositivo del socio, se debe ejecutar el siguiente script. _Nota: Esta marca podría ser innecesaria, ya que podría generar muchos hits, y no ofrece insights de valor para Coopeuch_ 


```html
<script>
	dataLayer.push({
		'event': 'eve',
			'eve-acc' : '/tef/pago-cuota-participacion/paso-2',
			'eve-cat' : 'Error',
			'eve-lab' : 'Clave debe tener 6 y 8 caracteres'
	});
</script>
```

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Documento Flujo Participación\img\flujo_partipacion-error-paso2_c.png)

Cuando aparezca este error en el dispositivo del socio, se debe ejecutar el siguiente script. 


```html
<script>
	dataLayer.push({
		'event': 'eve',
			'eve-acc' : '/tef/pago-cuota-participacion/paso-2',
			'eve-cat' : 'Error',
			'eve-lab' : 'Contraseña incorrecta'
	});
</script>
```
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Documento Flujo Participación\img\flujo_partipacion-error-paso2_d.png)

Cuando aparezca este error en el dispositivo del socio, se debe ejecutar el siguiente script:


```html
<script>
	dataLayer.push({
		'event': 'eve',
			'eve-acc' : '/tef/pago-cuota-participacion/paso-2',
			'eve-cat' : 'Error',
			'eve-lab' : 'Contraseña bloqueada'
	});
</script>
```

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Documento Flujo Participación\img\flujo_partipacion-error-servicio_no_disponible.png)

Cuando aparezca este error en el dispositivo del socio, se debe ejecutar el siguiente script. Donde ``%ubicacion%`` hace referencia a la URL donde se encuentre el socio.


```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/%ubicacion%/',
});
</script>
```