<img align="left" width="19%" height="19%" src="https://gitlab.com/d2b_chile/formato-documento-markdown/-/raw/master/Imagenes/D2B-GRIS-POSITIVO-CON BAJADA-01.png">

# **Documento de Implementación** de Crédito de Consumo (Google Analytics)

Marzo 2021





## Sobre el Documento

El siguiente documento describe las variables a ser utilizadas en cada marca, se debe copiar el script tal cual como viene indicado en cada paso. Cuando aparezca un  **`%variable%`**, corresponde a una variable que se debe reemplazar según el dato que corresponda de acuerdo al paso o sección en que se encuentre.

______

































## Scripts
### Landing
#### Crédito de Consumo
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Crédito de Consumo\img\lp.png)

Cuando el socio ingrese en la página de **Crédito de Consumo**, debe ejecutarse el siguiente script:

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/%ubicacion%'
});
</script>
```

#### Botón SIMULAR
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Crédito de Consumo\img\lp_simular.png)

Cuando el socio haga click en el botón "**Simular**", se debe ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/%ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Simular'
		}
	});
</script>
```

#### Landing - Crédito Consumo
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Crédito de Consumo\img\lp-2.png)



Cuando el socio llegue a este página se debe ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página:


```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/%ubicacion%'
});
</script>
```

#### Botón - Pagar Cuota

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Crédito de Consumo\img\lp-2_pagar-cuota.png)


Cuando el socio haga click en "**Pagar Cuota**", se debe ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/%ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Pagar Cuota'
		}
	});
</script>
```

##### Huinchas / Advertencias Azules

###### Más info sobre crédito

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Crédito de Consumo\img\lp-2_advertencia_info-credito.png)

Cuando la advertencia que dice "**Para más información sobre este crédito...**" se muestre en la pantalla del dispositivo del socio, se debe ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/%ubicacion%',
            'eve-cat' : 'Advertencia',
			'eve-lab' : 'Mas info sobre este credito'
		}
	});
</script>
```

###### Suscrito a pago por planilla

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Crédito de Consumo\img\lp-2_advertencia_suscrito-a-pago.png)

Cuando la advertencia que dice "**Estás suscrito a pago por planilla...**" se muestre en la pantalla del dispositivo del socio, se debe ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/%ubicacion%',
            'eve-cat' : 'Advertencia',
			'eve-lab' : 'Estas suscrito a pago por planilla'
		}
	});
</script>
```

###### Invitación a pago crédito a través de Webpay

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Crédito de Consumo\img\lp-2_advertencia_suscrito-a-pago.png)

Cuando la advertencia que dice "**Estás suscrito a pago por planilla...**" se muestre en la pantalla del dispositivo del socio, se debe ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/%ubicacion%',
            'eve-cat' : 'Advertencia',
			'eve-lab' : 'Estas suscrito a pago por planilla'
		}
	});
</script>
```
#### Botón - Pagar Cuota

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Crédito de Consumo\img\lp-2_pagar-cuota.png)


Cuando el socio haga click en "**Ir a Pago Online**", se debe ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/%ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Ir a Pago Online'
		}
	});
</script>
```





### Pago de la cuota

#### Paso 1 - Pago
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\crédito listo flujo\img\pc_paso1.png)

Cuando el socio ingrese a esta página, ejecuar el siguiente script.

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/pago-cuota/paso1'
});
</script>
```
#### Paso 2 - Comprobante
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\crédito listo flujo\img\pc_paso2.png)

Cuando el socio ingrese a esta página, se debe ejecuar el siguiente script.

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/pago-cuota/paso2'
});
</script>
```
##### Pago exitoso

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Crédito de Consumo\img\pc_paso2_pago-exitoso.png)

Cuando aparezca el banner verde con el mensaje "**Pago exitoso**", se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/%ubicacion%',
            'eve-cat' : 'Visto',
			'eve-lab' : 'Pago exitoso'
		}
	});
</script>
```


### Mensajería
#### Sin saldo suficiente

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\crédito listo flujo\img\mensajeria_sin-saldo.png)

Cuando aparezca la advertencia amarilla que dice "**No tiene saldo suficiente. Te invitamos a abonar saldo a tu cuenta.**", se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/pago-cuota/paso1',
            'eve-cat' : 'Visto',
			'eve-lab' : 'Sin saldo suficiente'
		}
	});
</script>
```

#### La clave debe tener entre 6 y 8 caracteres

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\crédito listo flujo\img\mensajeria_clave-caracteres.png)

Cuando aparezca la advertencia amarilla que dice "**La clave debe tener entre 6 y 8 caract.**", se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/pago-cuota/paso1',
            'eve-cat' : 'Visto',
			'eve-lab' : 'Clave con caracteres insuficientes'
		}
	});
</script>
```

#### Clave incorrecta

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\crédito listo flujo\img\mensajeria_clave-incorrecta.png)

Cuando aparezca la advertencia amarilla que dice "**La contraseña ingresada es incorrecta. Por favor vuelve a intentar.**", se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/pago-cuota/paso1',
            'eve-cat' : 'Error',
			'eve-lab' : 'Clave incorrecta'
		}
	});
</script>
```

#### Clave incorrecta - Vuelve a intentar

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\crédito listo flujo\img\mensajeria_clave-incorrecta_vuelve-a-intentar.png)

Cuando aparezca la advertencia amarilla que dice "**La contraseña ingresada es incorrecta. Por favor vuelve a intentar. Recuerda que al tercer intento erróneo, tu clave sera bloqueada**", se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/pago-cuota/paso1',
            'eve-cat' : 'Error',
			'eve-lab' : 'Clave incorrecta'
		}
	});
</script>
```

#### Clave Bloqueada

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\crédito listo flujo\img\mensajeria_clave-bloqueada.png)

Cuando aparezca la advertencia amarilla que dice "**Clave Bloqueada**", se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/pago-cuota/paso1',
            'eve-cat' : 'Error',
			'eve-lab' : 'Clave Bloqueada'
		}
	});
</script>
```

### Sin Servicio
#### Servicio No Disponible

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\crédito listo flujo\img\mensajeria_clave-bloqueada.png)

Cuando aparezca la advertencia amarilla que dice "**En este momento nuestro servicio no se encuentra disponible.**", se debe ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/%ubicacion%/',
            'eve-cat' : 'Error',
			'eve-lab' : 'Servicio no disponible'
		}
	});
</script>
```
#### No se ha podido realizar el pago de tu Cuota del Crédito de Consumo
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\crédito listo flujo\img\mensajeria_clave-bloqueada.png)

Cuando aparezca la advertencia amarilla que dice "**No se ha podido realizar el pago de tu Cuota del Crédito de Consumo.**", se debe ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/%ubicacion%/',
            'eve-cat' : 'Error',
			'eve-lab' : 'No se pudo pagar el la cuota credito consumo'
		}
	});
</script>
```