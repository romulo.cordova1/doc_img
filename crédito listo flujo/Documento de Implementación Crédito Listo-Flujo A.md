<img align="left" width="19%" height="19%" src="https://gitlab.com/d2b_chile/formato-documento-markdown/-/raw/master/Imagenes/D2B-GRIS-POSITIVO-CON BAJADA-01.png">

# **Documento de Implementación** Enrolamiento Biometría  (Google Analytics)

Marzo 2021





## Sobre el Documento

El siguiente documento describe las variables a ser utilizadas en cada marca, se debe copiar el script tal cual como viene indicado en cada paso. Cuando aparezca un  **`%variable%`**, corresponde a una variable que se debe reemplazar según el dato que corresponda de acuerdo al paso o sección en que se encuentre.

______

































## Scripts
### Formas de Ingreso
#### Forma A
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\crédito listo flujo\img\ingreso_a.png)

Cuando el socio haga click en el botón "**Evalúa y contrata**", se debe ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/%ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Evalua y contrata-1'
		}
	});
</script>
```

#### Forma B
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\crédito listo flujo\img\ingreso_b.png)

Cuando el socio haga click en el botón "**Evalúa y contrata**", se debe ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/%ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Evalua y contrata-2'
		}
	});
</script>
```

#### Forma C
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\crédito listo flujo\img\ingreso_c.png)

Cuando el socio haga click en el botón "**Evalúa y contrata**", se debe ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/%ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Evalua y contrata-3'
		}
	});
</script>
```

### Paso 1
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\crédito listo flujo\img\paso_1.png)

Cuando el socio ingrese a esta página, ejecuar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página:

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/%ubicacion%'
});
</script>
```


![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\crédito listo flujo\img\paso_1_enviar_autorizacion.png)
Cuando se habilite el botón "**Enviar Autorización**" y el socio haga click en el mismo, se debe ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/%ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Enviar autorizacion'
		}
	});
</script>
```


![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\crédito listo flujo\img\paso_1_cancelar.png)
Si el socio hace click en el enlace "**Cancelar**", se debe ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página. 

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/%ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Cancelar'
		}
	});
</script>
```


### Paso 2
#### No podemos generar oferta - Caso A
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\crédito listo flujo\img\paso_2_no_oferta_a.png)

Si al socio no se le pudo generar una oferta, y se le muestra esta opción, se debe ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página:

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/%ubicacion%'
});
</script>
```
Si no se muestra el banner azul, con las "**ofertas que podrían interesar**", se debe ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'producto': 'Ofertas',
		'event-config': {
			'eve-acc' : '/tef/%ubicacion%',
            'eve-cat' : 'Formulario',
			'eve-lab' : 'Sin recomendacion de oferta'
		}
	});
</script>
```

Si el socio llenó los campos "**Email**", "**Teléfono celular**", e hizo click en "**Enviar y salir**", se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'producto': 'Ofertas',
		'event-config': {
			'eve-acc' : '/tef/%ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Enviar y salir'
		}
	});
</script>
```

#### No podemos generar oferta - Caso B
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\crédito listo flujo\img\paso_2_no_oferta_b.png)

Si al socio no se le pudo generar una oferta, y se le muestra esta opción, se debe ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página:

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/%ubicacion%'
});
</script>
```
Si se muestra el banner azul, con las "**ofertas que podrían interesar**", se debe ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'producto': 'Ofertas',
		'event-config': {
			'eve-acc' : '/tef/%ubicacion%',
            'eve-cat' : 'Formulario',
			'eve-lab' : 'Con recomendacion de oferta'
		}
	});
</script>
```


Si el socio llenó los campos "**Email**", "**Teléfono celular**", e hizo click en "**Enviar y salir**", se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'producto': 'Ofertas',
		'event-config': {
			'eve-acc' : '/tef/%ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Enviar y salir'
		}
	});
</script>
```
#### Salir sin enviar
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\crédito listo flujo\img\paso_2_no_oferta_salir.png)

Si el socio hace click en "**Salir sin enviar**", se debe ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/%ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Salir sin enviar'
		}
	});
</script>
```



#### Crédito aprobado

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\crédito listo flujo\img\paso_2_credito_aprobado.png)

Cuando el socio ingrese a la página de "**Crédito aprobado**", se debe ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página:

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/%ubicacion%'
});
</script>
```

#### Aceptar oferta
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\crédito listo flujo\img\paso_2_credito_aprobado_aceptar_oferta.png)

Cuando el socio haga click en "**Aceptar oferta**", se debe ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/%ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Aceptar oferta'
		}
	});
</script>
```

#### Continuar
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\crédito listo flujo\img\paso_2_credito_aprobado_continuar.png)

Cuando el socio haga click en el botón "**Continuar**", habilitado una vez haya seleccionado "**Desgravamen**" o "**Cesantía**" y escogido alguna "**Enfermedad pre-existente**", se debe ejecutar el siguiente sript. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página, ``%seguro%`` al seguro que el socio decidió contratar y ``%enfermedad%`` a la enfermedad pre-existente declarada en el menú dropdown:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'producto': '%seguro%',
		'preexistencias': '%enfermedad%',
		'event-config': {
			'eve-acc' : '/tef/%ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Continuar'
		}
	});
</script>
```

#### Cancelar
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\crédito listo flujo\img\paso_2_credito_aprobado_cancelar.png)

Cuando el socio haga click en "**Cancelar**", se debe ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/%ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Cancelar'
		}
	});
</script>
```

### Modales

#### Encuesta "No, prefiero seguir"
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\crédito listo flujo\img\modal_rechazo.png)

Cuando aparezca el modal de la encuesta para saber por qué el socio seleccionó la opción "**No, prefiero no seguir**" en la pantalla de su dispositivo, se debe ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'producto': 'Encuesta no seguir',
		'event-config': {
			'eve-acc' : '/tef/%ubicacion%',
            'eve-cat' : 'Error',
			'eve-lab' : 'Prefiero no seguir'
		}
	});
</script>
```

Cuando el socio seleccione alguna de las opciones ofrecidas en los botones radio y haga click en "**Enviar y abandonar**", se debe ejecutar el siguiente script. Donde ``%motivo%``corresponde a la opción marcada por el socio y ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'producto': 'Encuesta no seguir',
		'event-config': {
			'eve-acc' : '/tef/%ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : '%motivo%'
		}
	});
</script>
```
Si el socio hace click en "**Abandonar sin enviar**", se debe ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'producto': 'Encuesta no seguir',
		'event-config': {
			'eve-acc' : '/tef/%ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Abandonar sin enviar'
		}
	});
</script>
```
Si el socio hace click en "**Cerrar**", se debe ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'producto': 'Encuesta no seguir',
		'event-config': {
			'eve-acc' : '/tef/%ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Cerrar'
		}
	});
</script>
```
#### Seguro desgravamen
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\crédito listo flujo\img\seguro_desgravamen.png)

Cuando aparezca el modal de "**Seguro desgravamen* ***" en la pantalla del dispositivo del socio, se debe ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página:

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/%ubicacion%'
});
</script>
```

Cuando el socio haga click en "**Aceptar**" en el modal de "**Seguro desgravamen* ***", se debe ejecutar el siguiente script. Donde``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página: ¿

**nota de Rómulo: ¿No es indiferente si el socio hace click en "Aceptar" o en "Cerrar"? No creo que sea útil medir los clicks en este modal.**

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/%ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Seguro desgravamen aceptar'
		}
	});
</script>
```

#### Valida tu identidad
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\crédito listo flujo\img\validacion_identidad.png)

Cuando el módulo "**Valida tu identidad**" aparezca en el dispositivo del socio, se debe ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página:

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/%ubicacion%'
});
</script>
```

Cuando el socio ingrese su "**Nº de serie / Nº de documento**", y haga click en el botón "**Validar**", se debe ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/%ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Validar identidad'
		}
	});
</script>
```

Si el socio hace click en "**Cerrar**", se debe ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/%ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Cerrar'
		}
	});
</script>
```