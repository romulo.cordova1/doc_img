<img align="left" width="19%" height="19%" src="https://gitlab.com/d2b_chile/formato-documento-markdown/-/raw/master/Imagenes/D2B-GRIS-POSITIVO-CON BAJADA-01.png">

# **Documento de Implementación** Reprogramación crédito hipotecario FOGAPE (Google Analytics)

Marzo 2021





## Sobre el Documento

El siguiente documento describe las variables a ser utilizadas en cada marca, se debe copiar el script tal cual como viene indicado en cada paso. Cuando aparezca un  **`%variable%`**, corresponde a una variable que se debe reemplazar según el dato que corresponda de acuerdo al paso o sección en que se encuentre.

______

































## Scripts
### Home Page
##### Página vista de sitio privado

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Reprogramación CH FOGAPE\img\home_sitio_privado.PNG)

Cuando el socio ingrese a esta página, se debe ejecutar el siguiente script:

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/home'
});
</script>
```
#### Banner reprogramación de créditos

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Reprogramación CH FOGAPE\img\banner_boton_azul.PNG)

Cuando el socio haga click el botón "**Continuar**" dentro del banner "**Reprogramación cuotas de créditos**", se debe ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/home',
            'eve-cat' : 'Click',
			'eve-lab' : 'Continuar'
		}
	});
</script>
```

### Configuración reprogramación de créditos
#### Contrato de Canales

##### Visita de Contrato de Canales

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Reprogramación CH FOGAPE\img\contrato_canales.PNG)

Cuando el socio ingrese a esta página, se debe ejecutar el siguiente script. Donde ``%ubicacion``. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página.

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/%ubicacion%'
});
</script>
```

##### Descargar Contrato de canales

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Reprogramación CH FOGAPE\img\descargar_contrato_canles.PNG)

Cuando el socio haga click en "**Descargar Contratos**" se debe ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página.

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/%ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Descargar Contrato'
		}
	});
</script>
```

##### Check Condiciones y Continuar Contrato de Canales

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Reprogramación CH FOGAPE\img\continuar_contrato_canales.PNG)

Cuando el socio haga click en el check de "**Sí, acepto**", y luego haga click en "**Continuar**" se debe ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página.

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/%ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Continuar'
		}
	});
</script>
```

##### Cancelar proceso
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Reprogramación CH FOGAPE\img\contrato_canales_cancelar.PNG)

Si el socio hace click en el enlace "**Cancelar**", se debe ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página. 

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/%ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Cancelar'
		}
	});
</script>
```



### Crédito Hipotecario

##### Página vista de sitio privado

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Reprogramación CH FOGAPE\img\credito_hipotecario_pv.PNG)

Cuando el socio ingrese a esta página, se debe ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página.

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/%ubicacion%'
});
</script>
```

##### Ver y reprogramar

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Reprogramación CH FOGAPE\img\credito_hipotecario_ver_reprogramar.PNG)

Cuando el socio haga click en el link "**Ver y reprogramar**", se debe ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página.

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/%ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Ver y reprogramar'
		}
	});
</script>
```

### Declaración Jurada

#### Firmar y continuar
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Reprogramación CH FOGAPE\img\declaracion_jurada_firmar_continuar.PNG)

Cuando el socio haga click en alguno de los dos botones radio, y luego haga click en "**Firmar y Continuar**" se debe ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página y ``%si_no%`` a la opción seleccionada en el botón radio.

```html
<script>
	dataLayer.push({
		'event': 'eve',
        'producto': '%si_no%'
		'event-config': {
			'eve-acc' : '/tef/%ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Continuar'
		}
	});
</script>
```

#### Pago gastos operacionales - Reprogramar 6 dvidendos
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Reprogramación CH FOGAPE\img\reprogramar_dividendos.PNG)

Cuando el socio haga click en el check de "**Estoy de acuerdo y me comprometo a realizar el pago de $75.000 para efectuar la reprogramación al finalizar esa solicitud.**", seleccione el email al cual se le enviará la "**Copia digital de sus contratos**" (en los botones radio), haga click en el check de "**Sí, acepto los términos y condiciones.**",  ingrese su "**Nº de serie o de documento cédula de identidad**", y finalmente haga click en el botón "**Reprogramar 6 dividendos**", ejecutar el siuiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página y ``%monto%`` al Valor Total del pago de gastos operacionales:

```html
<script>
	dataLayer.push({
		'event': 'eve',
        'monto': '%valor-total%'
		'event-config': {
			'eve-acc' : '/tef/%ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Reprogramar 6 dividendos'
		}
	});
</script>
```

#### Pago gastos operacionales - Descargar contrato
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Reprogramación CH FOGAPE\img\reprogramar_dividendos_descarga_contrato.PNG)

Cuando el socio haga click en el enlace "**Descargar**" en el banner "**Solicitud de financiamiento de dividendos Crédito de Hipotecario y mandatos**", ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página: 

```html
<script>
	dataLayer.push({
		'event': 'eve',
        'monto': '%valor-total%'
		'event-config': {
			'eve-acc' : '/tef/%ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Descargar contrato'
		}
	});
</script>
```
#### Pago gastos operacionales - No por ahora
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Reprogramación CH FOGAPE\img\reprogramar_dividendos_no_por_ahora.PNG)

Cuando el socio haga click en el enlace "**No por ahora**" en el banner "**Firma electrónica**", ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página: 

```html
<script>
	dataLayer.push({
		'event': 'eve',
        'monto': '%valor-total%'
		'event-config': {
			'eve-acc' : '/tef/%ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'No por ahora'
		}
	});
</script>
```
### Comprobante
#### Solicitud de Reprogramación Exitosa
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Reprogramación CH FOGAPE\img\credito_hipotecario_exitoso.PNG)

Cuando el socio llegue a la página del "**Comprobante**", ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página: 

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/%ubicacion%'
});
</script>
```

### Modales

####  No es posible continuar con tu solicitud
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Reprogramación CH FOGAPE\img\modal_error_imposible_continuar.PNG)

Cuando el socio vea en su dispositivo el modal "**¡Importante! No puedes continuar con tu soliciud**", ejecutar el siguiente script. Donde `%ubicacion%` corresponde al lugar del flujo en el que ocurre el error:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/%ubicacion%',
            'eve-cat' : 'Error',
			'eve-lab' : 'Imposible continuar con solicitud'
		}
	});
</script>
```

####   No es posible realizar tu reprogramación con Aval Fogape
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Reprogramación CH FOGAPE\img\modal_error_imposible_reprogramacion_aval.PNG)

Cuando el socio vea en su dispositivo el modal "**¡Lo sentimos! No es posible realizar tu reprogramación con Aval Fogape**", ejecutar el siguiente script. Donde `%ubicacion%` corresponde al lugar del flujo en el que ocurre el error:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/%ubicacion%',
            'eve-cat' : 'Error',
			'eve-lab' : 'No cumple requisitos reprogramacion Aval Fogape'
		}
	});
</script>
```