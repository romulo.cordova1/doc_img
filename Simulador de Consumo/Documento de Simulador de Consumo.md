<img align="left" width="19%" height="19%" src="https://gitlab.com/d2b_chile/formato-documento-markdown/-/raw/master/Imagenes/D2B-GRIS-POSITIVO-CON BAJADA-01.png">

# **Documento de Implementación** de Simulador de Consumo (Google Analytics)

Marzo 2021





## Sobre el Documento

El siguiente documento describe las variables a ser utilizadas en cada marca, se debe copiar el script tal cual como viene indicado en cada paso. Cuando aparezca un  **`%variable%`**, corresponde a una variable que se debe reemplazar según el dato que corresponda de acuerdo al paso o sección en que se encuentre.

______

































## Scripts
### Solicita - Simulador de Consumo
#### Paso 1
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Simulador de Consumo\img\Paso1-vpv.png)

Cuando el socio seleccione **Simulador de Consumo** en el menú de la izquierda, que lo lleva al _Paso 1_ de la Simulación, debe ejecutarse el siguiente script::

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/solicitar/simulador-consumo/paso1',
});
</script>
```

##### Simular
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Simulador de Consumo\img\Paso1-botones.png)

Cuando el socio haga click en el botón "**Simular**" (bordeado en color naranja), se debe ejecutar el siguiente script.

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/solicitar/simulador-consumo/paso1',
            'eve-cat' : 'Click',
			'eve-lab' : 'Simular'
		}
	});
</script>
```
##### Volver
Además, si el cliente hace click en **Volver** (bordeado en azul), se debe ejecutar el siguiente script.

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/solicitar/simulador-consumo/paso1',
            'eve-cat' : 'Click',
			'eve-lab' : 'Volver'
		}
	});
</script>
```

#### Paso 2
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Simulador de Consumo\img\Paso2-vpv.png)

Cuando el socio llegue a este página se debe ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página:


```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/solicitar/simulador-consumo/paso2',
});
</script>
```

##### Lo Quiero

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Simulador de Consumo\img\Paso2-botones.png)

Cuando el socio haga click en "**Lo quiero**" (bordeado en naranja), se debe ejecutar el siguiente script. Donde ```%monto-liquido%``` corresponde al **Monto líquido del crédito**, y ```%tasa-mensual%``` corresponde a la **Tasa de interés mensual**.

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/solicitar/simulador-consumo/paso2',
            'eve-cat' : 'Click',
			'eve-lab' : 'Lo quiero',
			'monto' : '%monto-liquido%',
			'tasa_interes_mensual': '%tasa-mensual%'
		}
	});
</script>
```
##### Volver a Simular

Además, si el cliente hace click en **Volver a Simular** (bordeado en azul), se debe ejecutar el siguiente script.

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/solicitar/simulador-consumo/paso2',
            'eve-cat' : 'Click',
			'eve-lab' : 'Volver a simular'
		}
	});
</script>
```

#### Paso 3
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Simulador de Consumo\img\Paso3-vpv.png)

Cuando el socio llegue a este página se debe ejecutar el siguiente script. 

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/solicitar/simulador-consumo/paso3',
});
</script>
```

##### Solicitar

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Simulador de Consumo\img\Paso3-botones.png)

Cuando el socio haga click en "**Solicitar**" (bordeado en naranja), se debe ejecutar el siguiente script.

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/solicitar/simulador-consumo/paso3',
            'eve-cat' : 'Click',
			'eve-lab' : 'Solicitar',
		}
	});
</script>
```

##### Ver simulación

Además, si el cliente hace click en **Ver simulación** (bordeado en azul), se debe ejecutar el siguiente script.

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/solicitar/simulador-consumo/paso3',
            'eve-cat' : 'Click',
			'eve-lab' : 'Ver simulación'
		}
	});
</script>
```

#### Paso 4
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Simulador de Consumo\img\Paso4-vpv.png)

Cuando el socio llegue a este página se debe ejecutar los siguientes scripts. Donde ```%monto-liquido``` y ```%tasa-mensual``` serían los mismos valores obtenidos en el script del botón **Lo quiero**, el _Paso 2_ de este documento

```html
<script>
dataLayer.push({
	'event': 'vpv-privado',
	'page' : '/tef/solicitar/simulador-consumo/paso4',
	'monto' : '%monto-liquido%',
	'tasa_interes_mensual': '%tasa-mensual%',
});
</script>
```

##### Volver al home

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Simulador de Consumo\img\Paso4-botones.png)

Cuando el socio haga click en "**Volver al home**" (bordeado en naranja), se debe ejecutar el siguiente script.

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/solicitar/simulador-consumo/paso4',
            'eve-cat' : 'Click',
			'eve-lab' : 'Volver al home',
		}
	});
</script>
```

##### Volver a simular

Además, si el cliente hace click en **Volver a simular** (bordeado en azul), se debe ejecutar el siguiente script.

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/solicitar/simulador-consumo/paso4',
            'eve-cat' : 'Click',
			'eve-lab' : 'Volver a simular'
		}
	});
</script>
```

### Sin Servicio
#### Servicio No Disponible

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Simulador de Consumo\img\servicio_no_disponible.png)

Cuando aparezca la advertencia amarilla que dice "**En este momento nuestro servicio no se encuentra disponible.**", se debe ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/%ubicacion%/',
            'eve-cat' : 'Error',
			'eve-lab' : 'Servicio no disponible'
		}
	});
</script>
```
#### No se ha podido realizar la Simulación del Crédito de Consumo
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\Simulador de Consumo\img\simulacion_credito_imposible.png)

Cuando aparezca la advertencia amarilla que dice "**No se ha podido realizar la Simulación del Crédito de Consumo.**", se debe ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la página:

```html
<script>
	dataLayer.push({
		'event': 'eve',
		'event-config': {
			'eve-acc' : '/tef/%ubicacion%/',
            'eve-cat' : 'Error',
			'eve-lab' : 'No se pudo realizar simulacion credito consumo'
		}
	});
</script>
```