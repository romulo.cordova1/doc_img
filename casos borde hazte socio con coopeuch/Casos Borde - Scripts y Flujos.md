## Enrolamieno - Scripts y Flujos
### Hazte Socio + Cuenta Coopeuch  (Botón 1)

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\img\hazte_socio_boton1.png)

Cuando el visitante haga click en el botón "**Hazte Socio + Cuenta Coopeuch**" de la esquina superior derecha de la página, ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra el botón:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '%ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Hazte socio + cuenta coopeuch 1'
		}
	});
</script>
```

### Hazte Socio + Cuenta Coopeuch  (Botón 2)

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\img\hazte_socio_boton2.png)

Cuando el visitante haga click en el botón "**Hazte Socio + Cuenta Coopeuch**" del _hero banner_, ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra el botón:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '%ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Hazte socio + cuenta coopeuch 2'
		}
	});
</script>
```

### Hazte Socio + Cuenta Coopeuch  (Botón 3)

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\img\hazte_socio_boton3.png)

Cuando el visitante haga click en el botón "**Hazte Socio + Cuenta Coopeuch**" que se encuentra en la huincha a mitad de página, ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra el botón:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '%ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Hazte socio + cuenta coopeuch 3'
		}
	});
</script>
```

### Quiero que me contacten

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\img\quiero_que_me_contacten.png)



Cuando el visitante haga click en el botón "**Quiero que me contacten**" que se encuentra en la huincha a al final de la página, ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra el botón:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '%ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Quiero que me contacten'
		}
	});
</script>
```


### ClaveÚnica Checkmiid

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\img\checkmiid_continuar.png)



Cuando el visitante haga click en el botón "**Continuar**" que se encuentra el módulo de "**ClaveÚnica**", ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra el botón:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '%ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Continuar'
		}
	});
</script>
```





### Responder preguntas

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\img\responder_preguntas.png)



Cuando el visitante haga click en el botón "**Responder preguntas**" que se encuentra en el módulo titulado "**¿Tienes problemas para escanear el código?**", ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra el botón:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '%ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Responder preguntas'
		}
	});
</script>
```





### Validación de Preguntas

_**Página de Clave Única del Gobierno de Chile asumo que no puede ser trackeada por coopeuch**_



### VP-02 Validación de Preguntas - Verificación

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\img\vp_lp.png)

Cuando el visitante visite esta página, ejecutar el siguiente script:

```html
<script>
dataLayer.push({
'event': 'vpv',
'page' : '/hazte-socio-con-cuenta-coopeuch/enrolamiento/verificacion'
});
</script>
```



Cuando el visitante haga click en "**Continuar**", ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra el botón.

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/Enrolamiento %ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Continuar'
		}
	});
</script>
```



Si el usuario hace click en "**Cancelar**", ejecutar el siguiente script. Donde ``%ubicacion`` corresponde al lugar del flujo en el que se muestra el botón:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/Enrolamiento %ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Cancelar'
		}
	});
</script>
```



### VP-07 Validación de Preguntas - Autorización

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\img\vp_autorizacion.png)

Cuando el visitante visite esta página, ejecutar el siguiente script:

```html
<script>
dataLayer.push({
'event': 'vpv',
'page' : '/hazte-socio-con-cuenta-coopeuch/enrolamiento/autorizacion'
});
</script>
```



Cuando el visitante haga click en "**Continuar**", ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra el botón.



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/Enrolamiento %ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Continuar'
		}
	});
</script>
```



Si el usuario hace click en "**Cancelar**", ejecutar el siguiente script. Donde ``%ubicacion`` corresponde al lugar del flujo en el que se muestra el botón:



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/Enrolamiento %ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Cancelar'
		}
	});
</script>
```



### VP-06 Validación de Preguntas - Validación de Identidad

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\img\vp_validacion_identidad_1.png)

Cuando el visitante visite esta página, ejecutar el siguiente script:

```html
<script>
dataLayer.push({
'event': 'vpv',
'page' : '/hazte-socio-con-cuenta-coopeuch/enrolamiento/validacion-identidad'
});
</script>
```

### VP-09 Validación de Preguntas - Validación Exitosa

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\img\vp_validacion_exitosa.png)

Cuando aparezca este modal en la pantalla del usuario, ejecutar el siguiente scriptt:

```html
<script>
dataLayer.push({
'event': 'vpv',
'page' : '/hazte-socio-con-cuenta-coopeuch/enrolamiento/validacion-exitosa'
});
</script>
```
Si el usuario hace click al botón "**Continuar contratación**" en el mismo modal, ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/hazte-socio-con-cuenta-coopeuch/enrolamiento/validacion-exitosa',
            'eve-cat' : 'Click',
			'eve-lab' : 'Continuar contratacion'
		}
	});
</script>
```



### Datos Personales
#### DESK-01
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\img\hazte_socio_lp.png)Cuando el socio ingrese a la página del formulario de enrolamiento, ejecutar el siguiente script:

```html
<script>
dataLayer.push({
'event': 'vpv',
'page' : '/hazte-socio-con-cuenta-coopeuch/enrolamiento/datos personales'
});
</script>
```

#### Declaraciones
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\img\desk-01-declaraciones.png)

Cuando el usuario haga click en alguna de las opciones del módulo "**Declaraciones**", haya llenado exitosamente todos los datos personales, y haga click en el botón "**Continuar**", ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se selecciona las opciones y ``%declaracion%`` el texto de la opción de declaración que seleccionó el usuario:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'declaraciones': '%declaracion%'
		'event-config': {
			'eve-acc' : '/Enrolamiento %ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : '%declaracion%'
		}
	});
</script>
```

Cuando el visitante haga click en "**Continuar**", ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra el botón.

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/Enrolamiento %ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Continuar'
		}
	});
</script>
```

Si el usuario hace click en "**Cancelar**", ejecutar el siguiente script. Donde ``%ubicacion`` corresponde al lugar del flujo en el que se muestra el botón:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/Enrolamiento %ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Cancelar'
		}
	});
</script>
```



### Preferencias

#### DESK-02

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\img\hazte_socio_preferencias.png)Cuando el socio ingrese a la página principal de preferencias de enrolamiento, ejecutar el siguiente script:

```html
<script>
dataLayer.push({
'event': 'vpv',
'page' : '/hazte-socio-con-cuenta-coopeuch/preferencias'
});
</script>
```



#### DESK-04 - Mandato de pago primera Cuota de Participación

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\img\hazte_socio_preferencias_mandato_pago_primera_cuota_aceptar.png)

Una vez que el socio haya registrado la cantidad de cuotas por mes, se muestre el monto total de inversión mensual y haga click en "**Sí, autorizo a la empresa de despacho a cobrar mi primera cuota de participación**", correr el siguiente script. Donde ``%inversion`` es el monto total mostrado en "**Tu inversión total**"

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'monto': '%inversion%',
		'event-config': {
			'eve-acc' : '/hazte-socio-con-cuenta-coopeuch/preferencias',
            'eve-cat' : 'Click',
			'eve-lab' : 'Autorizar cobro primera cuota de participacion'
		}
	});
</script>
```



#### DESK-04 - Sucursal de preferencia (Sucursal Santiago Centro)

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\img\hazte_socio_preferencias_sucursal_santiago_centro.png)

Si el socio hace click en  "**Sucursal Santiago Centro (San Antonio Nº347, Local 1, Santiago)**", correr el siguiente script. Donde ```%sucursal%``` es el nombre de la sucursal escogida.

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'sucursal': '%sucursal%',
		'event-config': {
			'eve-acc' : '/hazte-socio-con-cuenta-coopeuch/preferencias',
            'eve-cat' : 'Click',
			'eve-lab' : '%sucursal%'
		}
	});
</script>
```


#### DESK-04 - Sucursal de preferencia (En otra sucursal)

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\img\hazte_socio_preferencias_sucursal_otra.png)

Si el socio hace click en  "**En otra sucursal**", correr el siguiente script. Donde ```%sucursal%``` es el nombre de la sucursal escogida.

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/hazte-socio-con-cuenta-coopeuch/preferencias',
            'eve-cat' : 'Click',
			'eve-lab' : 'En otra sucursal'
		}
	});
</script>
```



#### DESK-04 - Sucursal de preferencia (Ubicación de otra sucursal)

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\img\hazte_socio_preferencias_sucursal_otra_ubicacion.png)



Una vez el socio seleccione "**Región**", "**Comuna**", y "**Sucursal**" de los 3 menúes que se muestran después de hacer click en "**En otra sucursal**", ejecutar el siguiente script. Donde ```%sucursal%``` es la ubicación de la sucursal mostrada en negrita, dentro del aviso azul.



```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'sucursal': '%sucursal%',
		'event-config': {
			'eve-acc' : '/hazte-socio-con-cuenta-coopeuch/preferencias',
            'eve-cat' : 'Click',
			'eve-lab' : '%sucursal%'
		}
	});
</script>
```



Cuando el visitante haga click en "**Continuar**", ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra el botón.

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/Enrolamiento %ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Continuar'
		}
	});
</script>
```



Si el usuario hace click en "**Cancelar**", ejecutar el siguiente script. Donde ``%ubicacion`` corresponde al lugar del flujo en el que se muestra el botón:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/Enrolamiento %ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Cancelar'
		}
	});
</script>
```



#### DESK-04 - Cuenta Coopeuch: Despacho de tu Kit (En mi domicilio)

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\img\hazte_socio_despacho_kit_mi_domicilio.png)

Si el socio hace click en "**En mi domicilio**", ejecutar el siguiente script. Donde ``%kit_domicilio%`` es la dirección de domicilio que se muestra dentro de los paréntesis en el botón seleccionado.

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'despacho-kit': '%kit_domicilio%',
		'event-config': {
			'eve-acc' : '/hazte-socio-con-cuenta-coopeuch/preferencias',
            'eve-cat' : 'Click',
			'eve-lab' : '%kit_domicilio%'
		}
	});
</script>
```

### Contratación
#### DESK-05 - Conoce y lee tus contratos

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\img\hazte_socio_contratacion_lp.png)

Cuando el socio ingrese a la página de la contratación del enrolamiento, ejecutar el siguiente script:

```html
<script>
dataLayer.push({
'event': 'vpv',
'page' : '/hazte-socio-con-cuenta-coopeuch/enrolamiento/contratacion'
});
</script>
```

#### DESK-05 - Descargar contratos

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\img\hazte_socio_contratacion_descargar_contratos.png)

Cuando el socio haga click en "**Descargar Contratos**" de la esquina superior derecha, ejecutar el siguiente script.:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/hazte-socio-con-cuenta-coopeuch/contratacion',
            'eve-cat' : 'Click',
			'eve-lab' : 'Descargar contratos%'
		}
	});
</script>
```

#### DESK-05 - Descargar Solicitud ingreso socio y mandatos

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\img\hazte_socio_contratacion_descargar_contratos_1.png)

Cuando el socio haga click en "**Descargar**" en la fila titulada "**Solicitud ingreso socio y mandatos**", ejecutar el siguiente script.:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/hazte-socio-con-cuenta-coopeuch/contratacion',
            'eve-cat' : 'Click',
			'eve-lab' : 'Descargar solicitud ingreso socio y mandatos'
		}
	});
</script>
```

#### DESK-05 - Descargar Hoja de Resumen Tradicional Nominal

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\img\hazte_socio_contratacion_descargar_contratos_2.png)

Cuando el socio haga click en "**Descargar**" en la fila titulada "**Hoja de Resumen Tradicional Nominal**", ejecutar el siguiente script.:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/hazte-socio-con-cuenta-coopeuch/contratacion',
            'eve-cat' : 'Click',
			'eve-lab' : 'Descargar hoja de resumen tradicional nominal'
		}
	});
</script>
```

#### DESK-05 - Descargar Contrato de canales

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\img\hazte_socio_contratacion_descargar_contratos_3.png)

Cuando el socio haga click en "**Descargar**" en la fila titulada "**Contrato de canales**", ejecutar el siguiente script.:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/hazte-socio-con-cuenta-coopeuch/contratacion',
            'eve-cat' : 'Click',
			'eve-lab' : 'Descargar contrato de canales'
		}
	});
</script>
```

#### DESK-05 - Descargar Contrato de apertura de Cuenta Vista y afiliación al sistema y uso de Tarjeta de Débito

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\img\hazte_socio_contratacion_descargar_contratos_4.png)

Cuando el socio haga click en "**Descargar**" en la fila titulada "**Contrato de apertura de Cuenta Vista y afiliación al sistema y uso de Tarjeta de Débito**", ejecutar el siguiente script.:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/hazte-socio-con-cuenta-coopeuch/contratacion',
            'eve-cat' : 'Click',
			'eve-lab' : 'Descargar contrato de apertura de cuenta vista y afiliación al sistema y uso de tarjeta de debito'
		}
	});
</script>
```

#### DESK-05 - Descargar Poder especial

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\img\hazte_socio_contratacion_descargar_contratos_5.png)

Cuando el socio haga click en "**Descargar**" en la fila titulada "**Poder especial**", ejecutar el siguiente script.:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/hazte-socio-con-cuenta-coopeuch/contratacion',
            'eve-cat' : 'Click',
			'eve-lab' : 'Descargar poder especial'
		}
	});
</script>
```

#### DESK-05 - Térmios y condiciones de los contratos

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\img\hazte_socio_contratacion_terminos_condiciones_aceptar.png)

Cuando el socio haga click en la caja de "**Sí, acepto los términos y condiciones**", ejecutar el siguiente script:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/hazte-socio-con-cuenta-coopeuch/contratacion',
            'eve-cat' : 'Click',
			'eve-lab' : 'Acepto terminos y condiciones'
		}
	});
</script>
```

#### DESK-05 - Firma electrónica

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\img\hazte_socio_contratacion_enviar_clave_sms.png)

Cuando el socio haga click en el botón "**Enviar clave SMS**", ejecutar el siguiente script.:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/hazte-socio-con-cuenta-coopeuch/contratacion',
            'eve-cat' : 'Click',
			'eve-lab' : 'Enviar clave SMS'
		}
	});
</script>
```
#### DESK-05 - Contratar

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\img\hazte_socio_boton_contratar.png)

Cuando el socio haga click en el botón "**Contratar**", ejecutar el siguiente script.:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/hazte-socio-con-cuenta-coopeuch/contratacion',
            'eve-cat' : 'Click',
			'eve-lab' : 'Contratar'
		}
	});
</script>
```



Si el usuario hace click en "**Cancelar**", ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra el botón:

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/Enrolamiento %ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Cancelar'
		}
	});
</script>
```






## Casos Borde - Scripts y Flujos

### Modal error: Ya eres Socio Coopeuch

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\casos borde hazte socio con coopeuch\img\vis_error_datos_personales.png)

Cuando el aparezca el modal de error "**¡Importante! Ya eres Socio Coopeuch**" en la pantalla del dispositivo del socio, ejecutar el siguiente script. 

```html
<script>
  dataLayer.push({
	'event': 'trigger_event',
	'eve-acc' : 'hazte-socio-con-cuenta-coopeuch/enrolamiento',
	'eve-cat' : 'Error',
	'eve-lab' : 'Ya eres socio Coopeuch'
});
</script>
```

En caso donde el socio haga click en "**Ir a mi Sitio Privado**" debe ejecutarse el siguiente script:

```html
<script>
  dataLayer.push({
	'event': 'eve',
	'eve-acc' : 'Enrolamiento %ubicacion%',
	'eve-cat' : 'Click',
	'eve-lab' : 'Ir a mi Sitio Privado'
});
</script>
```

Cuando el socio haga click en **Solicitar Cuenta Coopeuch**, debe ejecutarse el siguiente script:

```html
<script>
  dataLayer.push({
	'event': 'eve',
	'eve-acc' : 'Enrolamiento %ubicacion%',
	'eve-cat' : 'Click',
	'eve-lab' : 'Solicitar Cuenta Coopeuch'
});
</script>
```


### Modal error: Número ya registrado

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\casos borde hazte socio con coopeuch\img\vis_error_numero_ya_registrado.png)

Cuando el aparezca el modal de error "**¡Lo sentimos! Número ya registrado**" en la pantalla del dispositivo del socio, ejecutar el siguiente script. Donde `%ubicacion%` corresponde al lugar del flujo en el que ocurre el error:

```html
<script>
  dataLayer.push({
	'event': 'evevisto',
	'eve-acc' : 'Enrolamiento %ubicacion%',
	'eve-cat' : 'Error',
	'eve-lab' : 'Numero ya registrado'
});
</script>
```

En caso donde el socio haga click en "**Entendido**" debe ejecutarse el siguiente script:

```html
<script>
  dataLayer.push({
	'event': 'eve',
	'eve-acc' : 'Enrolamiento %ubicacion%',
	'eve-cat' : 'Click',
	'eve-lab' : 'Entendido'
});
</script>
```

### Modal error: Declaraciones

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\casos borde hazte socio con coopeuch\img\vis_error_declaraciones.png)

Cuando el aparezca el modal de error "**¡Lo sentimos! No es posible continuar con el proceso**" en la pantalla del dispositivo del socio, ejecutar el siguiente script. Donde `%ubicacion%` corresponde al lugar del flujo en el que ocurre el error:

```html
<script>
  dataLayer.push({
	'event': 'evevisto',
	'eve-acc' : 'Enrolamiento %ubicacion%',
	'eve-cat' : 'Error',
	'eve-lab' : 'Declaraciones - Imposible continuar proceso'
});
</script>
```

En caso donde el socio haga click en "**Entendido**" debe ejecutarse el siguiente script:
```html
<script>
  dataLayer.push({
	'event': 'eve',
	'eve-acc' : 'Enrolamiento %ubicacion%',
	'eve-cat' : 'Click',
	'eve-lab' : 'Entendido'
});
</script>
```
### Modal error: Declaraciones

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\casos borde hazte socio con coopeuch\img\vis_error_declaraciones.png)

Cuando el aparezca el modal de error "**¡Lo sentimos! No es posible continuar con el proceso**" en la pantalla del dispositivo del socio, ejecutar el siguiente script. Donde `%ubicacion%` corresponde al lugar del flujo en el que ocurre el error:

```html
<script>
  dataLayer.push({
	'event': 'evevisto',
	'eve-acc' : 'Enrolamiento %ubicacion%',
	'eve-cat' : 'Error',
	'eve-lab' : 'Declaraciones - Imposible continuar proceso'
});
</script>
```

En caso donde el socio haga click en "**Entendido**" debe ejecutarse el siguiente script:
```html
<script>
  dataLayer.push({
	'event': 'eve',
	'eve-acc' : 'Enrolamiento %ubicacion%',
	'eve-cat' : 'Click',
	'eve-lab' : 'Entendido'
});
</script>
```

### Modal error: Dirección sin cobertura

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\casos borde hazte socio con coopeuch\img\vis_error_direccion_sin_cobertura.png)

Cuando el aparezca el modal de error "**¡Lo sentimos! La dirección ingresada no tiene cobertura**" en la pantalla del dispositivo del socio, ejecutar el siguiente script. Donde `%ubicacion%` corresponde al lugar del flujo en el que ocurre el error:

```html
<script>
  dataLayer.push({
	'event': 'evevisto',
	'eve-acc' : 'Enrolamiento %ubicacion%',
	'eve-cat' : 'Error',
	'eve-lab' : 'Direccion sin cobertura'
});
</script>
```

En caso donde el socio haga click en "**Entendido**" debe ejecutarse el siguiente script:
```html
<script>
  dataLayer.push({
	'event': 'eve',
	'eve-acc' : 'Enrolamiento %ubicacion%',
	'eve-cat' : 'Click',
	'eve-lab' : 'Entendido'
});
</script>
```

### Modal error: Validación de identidad

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\casos borde hazte socio con coopeuch\img\vis_error_id_validation_error.png)

Cuando el aparezca el modal de error "**¡Lo sentimos! No ha sido posible validar tu idetidad**" en la pantalla del dispositivo del socio, ejecutar el siguiente script. Donde `%ubicacion%` corresponde al lugar del flujo en el que ocurre el error:

```html
<script>
  dataLayer.push({
	'event': 'evevisto',
	'eve-acc' : 'Enrolamiento %ubicacion%',
	'eve-cat' : 'Error',
	'eve-lab' : 'Imposible validar identidad'
});
</script>
```

En caso donde el socio haga click en "**Entendido**" debe ejecutarse el siguiente script:
```html
<script>
  dataLayer.push({
	'event': 'eve',
	'eve-acc' : 'Enrolamiento %ubicacion%',
	'eve-cat' : 'Click',
	'eve-lab' : 'Entendido'
});
</script>
```

### Clave dinámica SMS: Tiempo agotado

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\casos borde hazte socio con coopeuch\img\vis_error_sms_tiempo_agotado.png)

Cuando el contador del tiempo para el ingreso de la clave dinámica llegue a 00:00 y se muestre en la pantalla del socio, ejecutar el siguiente script. Donde `%ubicacion%` corresponde al lugar del flujo en el que ocurre el error:

```html
<script>
  dataLayer.push({
	'event': 'evevisto',
	'eve-acc' : 'Enrolamiento %ubicacion%',
	'eve-cat' : 'Error',
	'eve-lab' : 'Clave dinámica SMS: Tiempo agotado'
});
</script>
```

### Clave dinámica SMS: Clave incorrecta

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\casos borde hazte socio con coopeuch\img\vis_error_sms_clave_incorrecta.png)

Cuando se muestre el mensaje "**La clave dinámica ingresada es incorrecta. Reintentar** en la pantalla del socio, ejecutar el siguiente script. Donde `%ubicacion%` corresponde al lugar del flujo en el que ocurre el error:

```html
<script>
  dataLayer.push({
	'event': 'evevisto',
	'eve-acc' : 'Enrolamiento %ubicacion%',
	'eve-cat' : 'Error',
	'eve-lab' : 'Clave incorrecta'
});
</script>
```

### Clave dinámica SMS: Método bloqueado

![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\casos borde hazte socio con coopeuch\img\vis_error_sms_bloqueado.png)

Cuando la huincha que advierte con el mensaje "**Has excedido el límite de intentos**" se muestre en la pantalla del socio, ejecutar el siguiente script. Donde `%ubicacion%` corresponde al lugar del flujo en el que ocurre el error:

```html
<script>
  dataLayer.push({
	'event': 'evevisto',
	'eve-acc' : 'Enrolamiento %ubicacion%',
	'eve-cat' : 'Error',
	'eve-lab' : 'Clave dinámica SMS: Bloqueada'
});
</script>
```



## Encuestas Hazte Socio con Cuenta Coopeuch - Scripts y Flujos

### EA-00 - Encuesta de abandono
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\img\ea-00.png)

Cuando aparezca este modal de encuesta en la pantalla del usuario, ejecutar el siguiente script. Donde ``%ubicacion%``

```html
<script>
  dataLayer.push({
	'event': 'evevisto',
	'eve-acc' : 'Enrolamiento %ubicacion%',
	'eve-cat' : 'Encuesta',
	'eve-lab' : 'Encuesta de abandono'
});
</script>
```

### EA-01 - Encuesta de abandono (Opción: otro motivo)
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\img\ea-otro-motivo.png)

Cuando el usuario haga click en "**Otro motivo**", llene el campo de texto y haga click en el botón "**Enviar respuesta**", ejecutar el siguiente script. Donde ``%motivo`` es el texto que el usuario ingresó dentro del campo de texto habilitado y ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la encuesta.

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'encuesta': 'abandono',
		'motivo-abandono': '%motivo%',
		'comentario': '%comentario%',
		'event-config': {
			'eve-acc' : '/Enrolamiento %ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : '%motivo%'
		}
	});
</script>
```

### EA-02 - Encuesta de abandono (Opción: distinta a "Otro motivo")
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\img\ea-motivo-explicito.png)

Cuando el usuario seleccione alguna opción del cuestionario, y haga click en el botón "**Enviar respuesta**", ejecutar el siguiente script. Donde ``%motivo%`` es la opción que el usuario seleccionó al hacer click, ``%comentario%`` es el texto que  ingresó dentro del campo de texto habilitado y ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la encuesta.

_Nota: en el caso de "**Otro motivo**" es obligatorio que el usuario llene el campo de texto, por lo tanto, el script se debe ejecutar cuando esto pase, si el usuario no llena el campo de texto de las otras opciones, ejecutar el script igualmente._

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'encuesta': 'abandono',
		'motivo-abandono': '%motivo%',
		'comentario': '%comentario%',
		'event-config': {
			'eve-acc' : '/Enrolamiento %ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : '%motivo%'
		}
	});
</script>
```
Cuando el usuario haga click en "**Enviar respuesta**", ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la encuesta.

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/Enrolamiento %ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Enviar respuesta'
		}
	});
</script>
```

Cuando el usuario haga click en "**Retomar contratación**", ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la encuesta.

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'event-config': {
			'eve-acc' : '/Enrolamiento %ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Retomar contratación'
		}
	});
</script>
```

### ES-02 - Encuesta de Satisfacción
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\img\es-02.png)

Cuando el usuario haga click en "**Enviar y continuar**", ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la encuesta.

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'encuesta': 'satisfaccion'
		'event-config': {
			'eve-acc' : '/Enrolamiento %ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Enviar y continuar'
		}
	});
</script>
```

### EU-02 - Encuesta de Usabilidad
![](C:\Users\Rom\Documents\D2B\COOPEUCH\docs\hazte socio con coopeuch\img\es-02.png)

Cuando el usuario haga click en "**Enviar y terminar**", ejecutar el siguiente script. Donde ``%ubicacion%`` corresponde al lugar del flujo en el que se muestra la encuesta.

```html
<script>
	dataLayer.push({
		'event': 'trigger_event',
		'encuesta': 'satisfaccion',
		'event-config': {
			'eve-acc' : '/Enrolamiento %ubicacion%',
            'eve-cat' : 'Click',
			'eve-lab' : 'Enviar y terminar'
		}
	});
</script>
```


